﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GalaxyGates_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GateAlpha_Radio = New System.Windows.Forms.RadioButton()
        Me.GateBeta_Radio = New System.Windows.Forms.RadioButton()
        Me.GateGamma_Radio = New System.Windows.Forms.RadioButton()
        Me.GateDelta_Radio = New System.Windows.Forms.RadioButton()
        Me.GalaxyGates_Label = New System.Windows.Forms.Label()
        Me.Close_Button = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'GateAlpha_Radio
        '
        Me.GateAlpha_Radio.AutoSize = True
        Me.GateAlpha_Radio.Location = New System.Drawing.Point(32, 22)
        Me.GateAlpha_Radio.Name = "GateAlpha_Radio"
        Me.GateAlpha_Radio.Size = New System.Drawing.Size(78, 17)
        Me.GateAlpha_Radio.TabIndex = 0
        Me.GateAlpha_Radio.TabStop = True
        Me.GateAlpha_Radio.Text = "Gate Alpha"
        Me.GateAlpha_Radio.UseVisualStyleBackColor = True
        '
        'GateBeta_Radio
        '
        Me.GateBeta_Radio.AutoSize = True
        Me.GateBeta_Radio.Location = New System.Drawing.Point(32, 59)
        Me.GateBeta_Radio.Name = "GateBeta_Radio"
        Me.GateBeta_Radio.Size = New System.Drawing.Size(73, 17)
        Me.GateBeta_Radio.TabIndex = 1
        Me.GateBeta_Radio.TabStop = True
        Me.GateBeta_Radio.Text = "Gate Beta"
        Me.GateBeta_Radio.UseVisualStyleBackColor = True
        '
        'GateGamma_Radio
        '
        Me.GateGamma_Radio.AutoSize = True
        Me.GateGamma_Radio.Location = New System.Drawing.Point(32, 97)
        Me.GateGamma_Radio.Name = "GateGamma_Radio"
        Me.GateGamma_Radio.Size = New System.Drawing.Size(87, 17)
        Me.GateGamma_Radio.TabIndex = 2
        Me.GateGamma_Radio.TabStop = True
        Me.GateGamma_Radio.Text = "Gate Gamma"
        Me.GateGamma_Radio.UseVisualStyleBackColor = True
        '
        'GateDelta_Radio
        '
        Me.GateDelta_Radio.AutoSize = True
        Me.GateDelta_Radio.Location = New System.Drawing.Point(32, 133)
        Me.GateDelta_Radio.Name = "GateDelta_Radio"
        Me.GateDelta_Radio.Size = New System.Drawing.Size(76, 17)
        Me.GateDelta_Radio.TabIndex = 3
        Me.GateDelta_Radio.TabStop = True
        Me.GateDelta_Radio.Text = "Gate Delta"
        Me.GateDelta_Radio.UseVisualStyleBackColor = True
        '
        'GalaxyGates_Label
        '
        Me.GalaxyGates_Label.AutoSize = True
        Me.GalaxyGates_Label.Location = New System.Drawing.Point(153, 24)
        Me.GalaxyGates_Label.Name = "GalaxyGates_Label"
        Me.GalaxyGates_Label.Size = New System.Drawing.Size(42, 13)
        Me.GalaxyGates_Label.TabIndex = 5
        Me.GalaxyGates_Label.Text = "Testing"
        '
        'Close_Button
        '
        Me.Close_Button.Location = New System.Drawing.Point(32, 179)
        Me.Close_Button.Name = "Close_Button"
        Me.Close_Button.Size = New System.Drawing.Size(437, 43)
        Me.Close_Button.TabIndex = 6
        Me.Close_Button.Text = "Close"
        Me.Close_Button.UseVisualStyleBackColor = True
        '
        'GalaxyGates_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(501, 244)
        Me.ControlBox = False
        Me.Controls.Add(Me.Close_Button)
        Me.Controls.Add(Me.GalaxyGates_Label)
        Me.Controls.Add(Me.GateDelta_Radio)
        Me.Controls.Add(Me.GateGamma_Radio)
        Me.Controls.Add(Me.GateBeta_Radio)
        Me.Controls.Add(Me.GateAlpha_Radio)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "GalaxyGates_Form"
        Me.RightToLeftLayout = True
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DOHelper - Galaxy Gates"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GateAlpha_Radio As System.Windows.Forms.RadioButton
    Friend WithEvents GateBeta_Radio As System.Windows.Forms.RadioButton
    Friend WithEvents GateGamma_Radio As System.Windows.Forms.RadioButton
    Friend WithEvents GateDelta_Radio As System.Windows.Forms.RadioButton
    Friend WithEvents GalaxyGates_Label As System.Windows.Forms.Label
    Friend WithEvents Close_Button As System.Windows.Forms.Button
End Class
