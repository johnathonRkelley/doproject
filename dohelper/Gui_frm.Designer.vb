﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Gui_frm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Gui_frm))
        Me.pan_header = New System.Windows.Forms.Panel()
        Me.lblBack = New System.Windows.Forms.Label()
        Me.min_pan = New System.Windows.Forms.Panel()
        Me.close_pan = New System.Windows.Forms.Panel()
        Me.title_lbl = New System.Windows.Forms.Label()
        Me.widget_pan = New System.Windows.Forms.Panel()
        Me.doPanel = New System.Windows.Forms.Panel()
        Me.panBug = New System.Windows.Forms.Panel()
        Me.pan_about = New System.Windows.Forms.Panel()
        Me.lblInformation = New System.Windows.Forms.Label()
        Me.pan_help = New System.Windows.Forms.Panel()
        Me.pan_helplbl = New System.Windows.Forms.Label()
        Me.calc_pan = New System.Windows.Forms.Panel()
        Me.info_pan = New System.Windows.Forms.Panel()
        Me.tools_pan = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.pan_two = New System.Windows.Forms.Panel()
        Me.pan_twoimg = New System.Windows.Forms.Panel()
        Me.pan_twolbl = New System.Windows.Forms.Label()
        Me.pan_9 = New System.Windows.Forms.Panel()
        Me.pan_nineimg = New System.Windows.Forms.Panel()
        Me.pan_ninelbl = New System.Windows.Forms.Label()
        Me.pan_eight = New System.Windows.Forms.Panel()
        Me.pan_eightimg = New System.Windows.Forms.Panel()
        Me.pan_eightlbl = New System.Windows.Forms.Label()
        Me.pan_seven = New System.Windows.Forms.Panel()
        Me.pan_sevenimg = New System.Windows.Forms.Panel()
        Me.pan_sevenlbl = New System.Windows.Forms.Label()
        Me.pan_five = New System.Windows.Forms.Panel()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.pan_fiveimg = New System.Windows.Forms.Panel()
        Me.pan_fivelbl = New System.Windows.Forms.Label()
        Me.pan_six = New System.Windows.Forms.Panel()
        Me.pan_siximg = New System.Windows.Forms.Panel()
        Me.pan_sixlbl = New System.Windows.Forms.Label()
        Me.pan_three = New System.Windows.Forms.Panel()
        Me.pan_threeimg = New System.Windows.Forms.Panel()
        Me.pan_threelbl = New System.Windows.Forms.Label()
        Me.pan_one = New System.Windows.Forms.Panel()
        Me.pan_oneimg = New System.Windows.Forms.Panel()
        Me.pan_onelbl = New System.Windows.Forms.Label()
        Me.pan_four = New System.Windows.Forms.Panel()
        Me.pan_fourimg = New System.Windows.Forms.Panel()
        Me.pan_fourlbl = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.pan_header.SuspendLayout()
        Me.widget_pan.SuspendLayout()
        Me.pan_about.SuspendLayout()
        Me.pan_help.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.pan_two.SuspendLayout()
        Me.pan_9.SuspendLayout()
        Me.pan_eight.SuspendLayout()
        Me.pan_seven.SuspendLayout()
        Me.pan_five.SuspendLayout()
        Me.pan_six.SuspendLayout()
        Me.pan_three.SuspendLayout()
        Me.pan_one.SuspendLayout()
        Me.pan_four.SuspendLayout()
        Me.SuspendLayout()
        '
        'pan_header
        '
        Me.pan_header.BackColor = System.Drawing.Color.LightBlue
        Me.pan_header.Controls.Add(Me.lblBack)
        Me.pan_header.Controls.Add(Me.min_pan)
        Me.pan_header.Controls.Add(Me.close_pan)
        Me.pan_header.Controls.Add(Me.title_lbl)
        Me.pan_header.Dock = System.Windows.Forms.DockStyle.Top
        Me.pan_header.Location = New System.Drawing.Point(0, 0)
        Me.pan_header.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_header.Name = "pan_header"
        Me.pan_header.Size = New System.Drawing.Size(600, 63)
        Me.pan_header.TabIndex = 0
        '
        'lblBack
        '
        Me.lblBack.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBack.Location = New System.Drawing.Point(0, 0)
        Me.lblBack.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblBack.Name = "lblBack"
        Me.lblBack.Size = New System.Drawing.Size(55, 64)
        Me.lblBack.TabIndex = 2
        Me.lblBack.Text = "<-"
        Me.lblBack.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblBack.Visible = False
        '
        'min_pan
        '
        Me.min_pan.BackColor = System.Drawing.Color.Transparent
        Me.min_pan.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.curcubitor
        Me.min_pan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.min_pan.Location = New System.Drawing.Point(494, 6)
        Me.min_pan.Margin = New System.Windows.Forms.Padding(2)
        Me.min_pan.Name = "min_pan"
        Me.min_pan.Size = New System.Drawing.Size(50, 53)
        Me.min_pan.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.min_pan, "Minimize")
        '
        'close_pan
        '
        Me.close_pan.BackColor = System.Drawing.Color.Transparent
        Me.close_pan.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.silver_close_button_png_15
        Me.close_pan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.close_pan.ForeColor = System.Drawing.Color.White
        Me.close_pan.Location = New System.Drawing.Point(548, 6)
        Me.close_pan.Margin = New System.Windows.Forms.Padding(2)
        Me.close_pan.Name = "close_pan"
        Me.close_pan.Size = New System.Drawing.Size(50, 53)
        Me.close_pan.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.close_pan, "Close")
        '
        'title_lbl
        '
        Me.title_lbl.Font = New System.Drawing.Font("Arial Rounded MT Bold", 22.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.title_lbl.Location = New System.Drawing.Point(55, 0)
        Me.title_lbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.title_lbl.Name = "title_lbl"
        Me.title_lbl.Size = New System.Drawing.Size(435, 64)
        Me.title_lbl.TabIndex = 0
        Me.title_lbl.Text = "DOHelper - Tools "
        Me.title_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'widget_pan
        '
        Me.widget_pan.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.widget_pan.Controls.Add(Me.doPanel)
        Me.widget_pan.Controls.Add(Me.panBug)
        Me.widget_pan.Controls.Add(Me.pan_about)
        Me.widget_pan.Controls.Add(Me.pan_help)
        Me.widget_pan.Controls.Add(Me.calc_pan)
        Me.widget_pan.Controls.Add(Me.info_pan)
        Me.widget_pan.Controls.Add(Me.tools_pan)
        Me.widget_pan.Dock = System.Windows.Forms.DockStyle.Left
        Me.widget_pan.Location = New System.Drawing.Point(0, 63)
        Me.widget_pan.Margin = New System.Windows.Forms.Padding(2)
        Me.widget_pan.Name = "widget_pan"
        Me.widget_pan.Size = New System.Drawing.Size(55, 425)
        Me.widget_pan.TabIndex = 1
        '
        'doPanel
        '
        Me.doPanel.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.hades
        Me.doPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.doPanel.Location = New System.Drawing.Point(2, 188)
        Me.doPanel.Name = "doPanel"
        Me.doPanel.Size = New System.Drawing.Size(50, 50)
        Me.doPanel.TabIndex = 5
        Me.ToolTip1.SetToolTip(Me.doPanel, "Jump To Darkorbit")
        '
        'panBug
        '
        Me.panBug.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.Bugs
        Me.panBug.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.panBug.Location = New System.Drawing.Point(4, 253)
        Me.panBug.Margin = New System.Windows.Forms.Padding(2)
        Me.panBug.Name = "panBug"
        Me.panBug.Size = New System.Drawing.Size(48, 56)
        Me.panBug.TabIndex = 4
        Me.ToolTip1.SetToolTip(Me.panBug, "Report Bugs")
        '
        'pan_about
        '
        Me.pan_about.BackColor = System.Drawing.Color.Transparent
        Me.pan_about.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pan_about.Controls.Add(Me.lblInformation)
        Me.pan_about.Location = New System.Drawing.Point(4, 311)
        Me.pan_about.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_about.Name = "pan_about"
        Me.pan_about.Size = New System.Drawing.Size(50, 53)
        Me.pan_about.TabIndex = 3
        '
        'lblInformation
        '
        Me.lblInformation.BackColor = System.Drawing.Color.Transparent
        Me.lblInformation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblInformation.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInformation.Location = New System.Drawing.Point(0, 0)
        Me.lblInformation.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblInformation.Name = "lblInformation"
        Me.lblInformation.Size = New System.Drawing.Size(50, 53)
        Me.lblInformation.TabIndex = 1
        Me.lblInformation.Text = "i"
        Me.lblInformation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ToolTip1.SetToolTip(Me.lblInformation, "About")
        '
        'pan_help
        '
        Me.pan_help.BackColor = System.Drawing.Color.Transparent
        Me.pan_help.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pan_help.Controls.Add(Me.pan_helplbl)
        Me.pan_help.Location = New System.Drawing.Point(4, 369)
        Me.pan_help.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_help.Name = "pan_help"
        Me.pan_help.Size = New System.Drawing.Size(50, 53)
        Me.pan_help.TabIndex = 2
        '
        'pan_helplbl
        '
        Me.pan_helplbl.AutoSize = True
        Me.pan_helplbl.BackColor = System.Drawing.Color.Transparent
        Me.pan_helplbl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pan_helplbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pan_helplbl.Location = New System.Drawing.Point(0, 0)
        Me.pan_helplbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.pan_helplbl.Name = "pan_helplbl"
        Me.pan_helplbl.Size = New System.Drawing.Size(52, 55)
        Me.pan_helplbl.TabIndex = 0
        Me.pan_helplbl.Text = "?"
        Me.ToolTip1.SetToolTip(Me.pan_helplbl, "Help")
        '
        'calc_pan
        '
        Me.calc_pan.BackColor = System.Drawing.Color.Transparent
        Me.calc_pan.BackgroundImage = CType(resources.GetObject("calc_pan.BackgroundImage"), System.Drawing.Image)
        Me.calc_pan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.calc_pan.Location = New System.Drawing.Point(3, 118)
        Me.calc_pan.Margin = New System.Windows.Forms.Padding(2)
        Me.calc_pan.Name = "calc_pan"
        Me.calc_pan.Size = New System.Drawing.Size(50, 53)
        Me.calc_pan.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.calc_pan, "Calculators")
        '
        'info_pan
        '
        Me.info_pan.BackColor = System.Drawing.Color.Transparent
        Me.info_pan.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.information_icon_clip_art_at_clker_com_vector_clip_art_online__32
        Me.info_pan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.info_pan.Location = New System.Drawing.Point(3, 60)
        Me.info_pan.Margin = New System.Windows.Forms.Padding(2)
        Me.info_pan.Name = "info_pan"
        Me.info_pan.Size = New System.Drawing.Size(50, 53)
        Me.info_pan.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.info_pan, "Information")
        '
        'tools_pan
        '
        Me.tools_pan.BackColor = System.Drawing.Color.Transparent
        Me.tools_pan.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.Home_Improvement_512
        Me.tools_pan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tools_pan.Location = New System.Drawing.Point(3, 2)
        Me.tools_pan.Margin = New System.Windows.Forms.Padding(2)
        Me.tools_pan.Name = "tools_pan"
        Me.tools_pan.Size = New System.Drawing.Size(50, 53)
        Me.tools_pan.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.tools_pan, "Tools")
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.TableLayoutPanel1)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(55, 63)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(545, 425)
        Me.Panel1.TabIndex = 2
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.AutoSize = True
        Me.TableLayoutPanel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.Controls.Add(Me.pan_two, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.pan_9, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.pan_eight, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.pan_seven, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.pan_five, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.pan_six, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.pan_three, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.pan_one, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.pan_four, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(2)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(545, 425)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'pan_two
        '
        Me.pan_two.Controls.Add(Me.pan_twoimg)
        Me.pan_two.Controls.Add(Me.pan_twolbl)
        Me.pan_two.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pan_two.Location = New System.Drawing.Point(184, 3)
        Me.pan_two.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_two.Name = "pan_two"
        Me.pan_two.Size = New System.Drawing.Size(176, 136)
        Me.pan_two.TabIndex = 2
        '
        'pan_twoimg
        '
        Me.pan_twoimg.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.bk
        Me.pan_twoimg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pan_twoimg.Location = New System.Drawing.Point(43, 4)
        Me.pan_twoimg.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_twoimg.Name = "pan_twoimg"
        Me.pan_twoimg.Size = New System.Drawing.Size(94, 102)
        Me.pan_twoimg.TabIndex = 2
        '
        'pan_twolbl
        '
        Me.pan_twolbl.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pan_twolbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pan_twolbl.Location = New System.Drawing.Point(0, 106)
        Me.pan_twolbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.pan_twolbl.Name = "pan_twolbl"
        Me.pan_twolbl.Size = New System.Drawing.Size(176, 30)
        Me.pan_twolbl.TabIndex = 3
        Me.pan_twolbl.Text = "Alien"
        Me.pan_twolbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pan_9
        '
        Me.pan_9.Controls.Add(Me.pan_nineimg)
        Me.pan_9.Controls.Add(Me.pan_ninelbl)
        Me.pan_9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pan_9.Location = New System.Drawing.Point(365, 285)
        Me.pan_9.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_9.Name = "pan_9"
        Me.pan_9.Size = New System.Drawing.Size(177, 137)
        Me.pan_9.TabIndex = 2
        '
        'pan_nineimg
        '
        Me.pan_nineimg.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.bk
        Me.pan_nineimg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pan_nineimg.Location = New System.Drawing.Point(43, 4)
        Me.pan_nineimg.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_nineimg.Name = "pan_nineimg"
        Me.pan_nineimg.Size = New System.Drawing.Size(94, 102)
        Me.pan_nineimg.TabIndex = 4
        '
        'pan_ninelbl
        '
        Me.pan_ninelbl.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pan_ninelbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pan_ninelbl.Location = New System.Drawing.Point(0, 107)
        Me.pan_ninelbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.pan_ninelbl.Name = "pan_ninelbl"
        Me.pan_ninelbl.Size = New System.Drawing.Size(177, 30)
        Me.pan_ninelbl.TabIndex = 5
        Me.pan_ninelbl.Text = "Alien"
        Me.pan_ninelbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pan_eight
        '
        Me.pan_eight.Controls.Add(Me.pan_eightimg)
        Me.pan_eight.Controls.Add(Me.pan_eightlbl)
        Me.pan_eight.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pan_eight.Location = New System.Drawing.Point(184, 285)
        Me.pan_eight.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_eight.Name = "pan_eight"
        Me.pan_eight.Size = New System.Drawing.Size(176, 137)
        Me.pan_eight.TabIndex = 2
        '
        'pan_eightimg
        '
        Me.pan_eightimg.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.bk
        Me.pan_eightimg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pan_eightimg.Location = New System.Drawing.Point(43, 4)
        Me.pan_eightimg.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_eightimg.Name = "pan_eightimg"
        Me.pan_eightimg.Size = New System.Drawing.Size(94, 102)
        Me.pan_eightimg.TabIndex = 4
        '
        'pan_eightlbl
        '
        Me.pan_eightlbl.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pan_eightlbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pan_eightlbl.Location = New System.Drawing.Point(0, 107)
        Me.pan_eightlbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.pan_eightlbl.Name = "pan_eightlbl"
        Me.pan_eightlbl.Size = New System.Drawing.Size(176, 30)
        Me.pan_eightlbl.TabIndex = 5
        Me.pan_eightlbl.Text = "Alien"
        Me.pan_eightlbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pan_seven
        '
        Me.pan_seven.Controls.Add(Me.pan_sevenimg)
        Me.pan_seven.Controls.Add(Me.pan_sevenlbl)
        Me.pan_seven.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pan_seven.Location = New System.Drawing.Point(3, 285)
        Me.pan_seven.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_seven.Name = "pan_seven"
        Me.pan_seven.Size = New System.Drawing.Size(176, 137)
        Me.pan_seven.TabIndex = 2
        '
        'pan_sevenimg
        '
        Me.pan_sevenimg.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.bk
        Me.pan_sevenimg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pan_sevenimg.Location = New System.Drawing.Point(43, 4)
        Me.pan_sevenimg.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_sevenimg.Name = "pan_sevenimg"
        Me.pan_sevenimg.Size = New System.Drawing.Size(94, 102)
        Me.pan_sevenimg.TabIndex = 4
        '
        'pan_sevenlbl
        '
        Me.pan_sevenlbl.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pan_sevenlbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pan_sevenlbl.Location = New System.Drawing.Point(0, 107)
        Me.pan_sevenlbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.pan_sevenlbl.Name = "pan_sevenlbl"
        Me.pan_sevenlbl.Size = New System.Drawing.Size(176, 30)
        Me.pan_sevenlbl.TabIndex = 5
        Me.pan_sevenlbl.Text = "Alien"
        Me.pan_sevenlbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pan_five
        '
        Me.pan_five.Controls.Add(Me.WebBrowser1)
        Me.pan_five.Controls.Add(Me.pan_fiveimg)
        Me.pan_five.Controls.Add(Me.pan_fivelbl)
        Me.pan_five.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pan_five.Location = New System.Drawing.Point(184, 144)
        Me.pan_five.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_five.Name = "pan_five"
        Me.pan_five.Size = New System.Drawing.Size(176, 136)
        Me.pan_five.TabIndex = 2
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Location = New System.Drawing.Point(373, 310)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.TabIndex = 6
        Me.WebBrowser1.Visible = False
        '
        'pan_fiveimg
        '
        Me.pan_fiveimg.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.bk
        Me.pan_fiveimg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pan_fiveimg.Location = New System.Drawing.Point(43, 4)
        Me.pan_fiveimg.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_fiveimg.Name = "pan_fiveimg"
        Me.pan_fiveimg.Size = New System.Drawing.Size(94, 102)
        Me.pan_fiveimg.TabIndex = 4
        '
        'pan_fivelbl
        '
        Me.pan_fivelbl.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pan_fivelbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pan_fivelbl.Location = New System.Drawing.Point(0, 106)
        Me.pan_fivelbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.pan_fivelbl.Name = "pan_fivelbl"
        Me.pan_fivelbl.Size = New System.Drawing.Size(176, 30)
        Me.pan_fivelbl.TabIndex = 5
        Me.pan_fivelbl.Text = "Alien"
        Me.pan_fivelbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pan_six
        '
        Me.pan_six.Controls.Add(Me.pan_siximg)
        Me.pan_six.Controls.Add(Me.pan_sixlbl)
        Me.pan_six.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pan_six.Location = New System.Drawing.Point(365, 144)
        Me.pan_six.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_six.Name = "pan_six"
        Me.pan_six.Size = New System.Drawing.Size(177, 136)
        Me.pan_six.TabIndex = 2
        '
        'pan_siximg
        '
        Me.pan_siximg.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.bk
        Me.pan_siximg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pan_siximg.Location = New System.Drawing.Point(43, 4)
        Me.pan_siximg.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_siximg.Name = "pan_siximg"
        Me.pan_siximg.Size = New System.Drawing.Size(94, 102)
        Me.pan_siximg.TabIndex = 4
        '
        'pan_sixlbl
        '
        Me.pan_sixlbl.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pan_sixlbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pan_sixlbl.Location = New System.Drawing.Point(0, 106)
        Me.pan_sixlbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.pan_sixlbl.Name = "pan_sixlbl"
        Me.pan_sixlbl.Size = New System.Drawing.Size(177, 30)
        Me.pan_sixlbl.TabIndex = 5
        Me.pan_sixlbl.Text = "Alien"
        Me.pan_sixlbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pan_three
        '
        Me.pan_three.Controls.Add(Me.pan_threeimg)
        Me.pan_three.Controls.Add(Me.pan_threelbl)
        Me.pan_three.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pan_three.Location = New System.Drawing.Point(365, 3)
        Me.pan_three.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_three.Name = "pan_three"
        Me.pan_three.Size = New System.Drawing.Size(177, 136)
        Me.pan_three.TabIndex = 2
        '
        'pan_threeimg
        '
        Me.pan_threeimg.BackColor = System.Drawing.Color.Transparent
        Me.pan_threeimg.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.bk
        Me.pan_threeimg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pan_threeimg.Location = New System.Drawing.Point(43, 4)
        Me.pan_threeimg.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_threeimg.Name = "pan_threeimg"
        Me.pan_threeimg.Size = New System.Drawing.Size(94, 102)
        Me.pan_threeimg.TabIndex = 4
        '
        'pan_threelbl
        '
        Me.pan_threelbl.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pan_threelbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pan_threelbl.Location = New System.Drawing.Point(0, 106)
        Me.pan_threelbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.pan_threelbl.Name = "pan_threelbl"
        Me.pan_threelbl.Size = New System.Drawing.Size(177, 30)
        Me.pan_threelbl.TabIndex = 5
        Me.pan_threelbl.Text = "Alien"
        Me.pan_threelbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pan_one
        '
        Me.pan_one.Controls.Add(Me.pan_oneimg)
        Me.pan_one.Controls.Add(Me.pan_onelbl)
        Me.pan_one.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pan_one.Location = New System.Drawing.Point(3, 3)
        Me.pan_one.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_one.Name = "pan_one"
        Me.pan_one.Size = New System.Drawing.Size(176, 136)
        Me.pan_one.TabIndex = 0
        '
        'pan_oneimg
        '
        Me.pan_oneimg.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.bk
        Me.pan_oneimg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pan_oneimg.Location = New System.Drawing.Point(43, 4)
        Me.pan_oneimg.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_oneimg.Name = "pan_oneimg"
        Me.pan_oneimg.Size = New System.Drawing.Size(94, 102)
        Me.pan_oneimg.TabIndex = 0
        '
        'pan_onelbl
        '
        Me.pan_onelbl.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pan_onelbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pan_onelbl.Location = New System.Drawing.Point(0, 106)
        Me.pan_onelbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.pan_onelbl.Name = "pan_onelbl"
        Me.pan_onelbl.Size = New System.Drawing.Size(176, 30)
        Me.pan_onelbl.TabIndex = 1
        Me.pan_onelbl.Text = "Alien"
        Me.pan_onelbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pan_four
        '
        Me.pan_four.Controls.Add(Me.pan_fourimg)
        Me.pan_four.Controls.Add(Me.pan_fourlbl)
        Me.pan_four.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pan_four.Location = New System.Drawing.Point(3, 144)
        Me.pan_four.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_four.Name = "pan_four"
        Me.pan_four.Size = New System.Drawing.Size(176, 136)
        Me.pan_four.TabIndex = 1
        '
        'pan_fourimg
        '
        Me.pan_fourimg.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.bk
        Me.pan_fourimg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pan_fourimg.Location = New System.Drawing.Point(43, 4)
        Me.pan_fourimg.Margin = New System.Windows.Forms.Padding(2)
        Me.pan_fourimg.Name = "pan_fourimg"
        Me.pan_fourimg.Size = New System.Drawing.Size(94, 102)
        Me.pan_fourimg.TabIndex = 4
        '
        'pan_fourlbl
        '
        Me.pan_fourlbl.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pan_fourlbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pan_fourlbl.Location = New System.Drawing.Point(0, 106)
        Me.pan_fourlbl.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.pan_fourlbl.Name = "pan_fourlbl"
        Me.pan_fourlbl.Size = New System.Drawing.Size(176, 30)
        Me.pan_fourlbl.TabIndex = 5
        Me.pan_fourlbl.Text = "Alien"
        Me.pan_fourlbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel2
        '
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(545, 425)
        Me.Panel2.TabIndex = 1
        Me.Panel2.Visible = False
        '
        'ToolTip1
        '
        Me.ToolTip1.ForeColor = System.Drawing.Color.Transparent
        '
        'Gui_frm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(600, 488)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.widget_pan)
        Me.Controls.Add(Me.pan_header)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Gui_frm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DOProject"
        Me.pan_header.ResumeLayout(False)
        Me.widget_pan.ResumeLayout(False)
        Me.pan_about.ResumeLayout(False)
        Me.pan_help.ResumeLayout(False)
        Me.pan_help.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.pan_two.ResumeLayout(False)
        Me.pan_9.ResumeLayout(False)
        Me.pan_eight.ResumeLayout(False)
        Me.pan_seven.ResumeLayout(False)
        Me.pan_five.ResumeLayout(False)
        Me.pan_six.ResumeLayout(False)
        Me.pan_three.ResumeLayout(False)
        Me.pan_one.ResumeLayout(False)
        Me.pan_four.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pan_header As System.Windows.Forms.Panel
    Friend WithEvents min_pan As System.Windows.Forms.Panel
    Friend WithEvents close_pan As System.Windows.Forms.Panel
    Friend WithEvents title_lbl As System.Windows.Forms.Label
    Friend WithEvents widget_pan As System.Windows.Forms.Panel
    Friend WithEvents calc_pan As System.Windows.Forms.Panel
    Friend WithEvents info_pan As System.Windows.Forms.Panel
    Friend WithEvents tools_pan As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pan_two As System.Windows.Forms.Panel
    Friend WithEvents pan_9 As System.Windows.Forms.Panel
    Friend WithEvents pan_eight As System.Windows.Forms.Panel
    Friend WithEvents pan_seven As System.Windows.Forms.Panel
    Friend WithEvents pan_five As System.Windows.Forms.Panel
    Friend WithEvents pan_six As System.Windows.Forms.Panel
    Friend WithEvents pan_three As System.Windows.Forms.Panel
    Friend WithEvents pan_one As System.Windows.Forms.Panel
    Friend WithEvents pan_four As System.Windows.Forms.Panel
    Friend WithEvents pan_onelbl As System.Windows.Forms.Label
    Friend WithEvents pan_oneimg As System.Windows.Forms.Panel
    Friend WithEvents pan_twolbl As System.Windows.Forms.Label
    Friend WithEvents pan_twoimg As System.Windows.Forms.Panel
    Friend WithEvents pan_ninelbl As System.Windows.Forms.Label
    Friend WithEvents pan_nineimg As System.Windows.Forms.Panel
    Friend WithEvents pan_eightlbl As System.Windows.Forms.Label
    Friend WithEvents pan_eightimg As System.Windows.Forms.Panel
    Friend WithEvents pan_sevenlbl As System.Windows.Forms.Label
    Friend WithEvents pan_sevenimg As System.Windows.Forms.Panel
    Friend WithEvents pan_fivelbl As System.Windows.Forms.Label
    Friend WithEvents pan_fiveimg As System.Windows.Forms.Panel
    Friend WithEvents pan_sixlbl As System.Windows.Forms.Label
    Friend WithEvents pan_siximg As System.Windows.Forms.Panel
    Friend WithEvents pan_threelbl As System.Windows.Forms.Label
    Friend WithEvents pan_threeimg As System.Windows.Forms.Panel
    Friend WithEvents pan_fourlbl As System.Windows.Forms.Label
    Friend WithEvents pan_fourimg As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents pan_about As System.Windows.Forms.Panel
    Friend WithEvents pan_help As System.Windows.Forms.Panel
    Friend WithEvents pan_helplbl As System.Windows.Forms.Label
    Friend WithEvents lblBack As System.Windows.Forms.Label
    Friend WithEvents lblInformation As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents panBug As System.Windows.Forms.Panel
    Friend WithEvents doPanel As System.Windows.Forms.Panel
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
End Class
