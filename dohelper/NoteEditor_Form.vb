﻿Public Class NoteEditor_Form

    'Buttons
    Private Sub Save_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Save_Button.Click
        If RichTextBox1.Text = "" Then
            MsgBox("Nothing to save!", MsgBoxStyle.OkOnly, "Error!")
            GoTo End1
        End If
        Dim SaveFileDialog1 As New SaveFileDialog
        SaveFileDialog1.Filter = "RTF|*.rtf"
        If SaveFileDialog1.ShowDialog() <> Windows.Forms.DialogResult.Cancel Then
            RichTextBox1.SaveFile(SaveFileDialog1.FileName)
        ElseIf Windows.Forms.DialogResult.Cancel Then
        End If
end1:
    End Sub
    Private Sub CancelClose_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CancelClose_Button.Click
        RichTextBox1.Text = ""
        Me.Hide()
    End Sub
    Private Sub Clear_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clear_Button.Click
        If MsgBox("Are you sure you want to clear the note?", vbYesNo, "DOHelper") = vbYes Then
            RichTextBox1.Text = ""
        End If
    End Sub
End Class