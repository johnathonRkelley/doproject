﻿Public Class Gui_frm
    'Initialized Variables
    Dim tools, calcs, info As Boolean
    Dim title As String = "DOHelper - "
    Dim drag As Boolean
    Dim mousex As Integer
    Dim mousey As Integer
    Dim db As Database
    Dim con As String = "Under Construction"

    'Loading of the Form
    Private Sub Gui_frm_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Main form contains the main db so it needs to be ran first to send out the db to each form
        Main_Form.Show()
        Main_Form.Hide()

        tools = False
        calcs = False
        info = True

        title_lbl.Text = title & "Information"
        Me.Text = title & "Information"

        panelChange()
    End Sub

    'Database pass through
    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    'Panel Change Creates changes the pictures for the form
    Private Sub panelChange()
        If tools = True Then
            'Panel One 
            pan_oneimg.BackgroundImage = My.Resources.camera
            pan_onelbl.Text = "Screen Capture"

            'Panel two
            pan_twoimg.BackgroundImage = My.Resources.notepad
            pan_twolbl.Text = "Note Editor"

            'Panel 3
            pan_threeimg.BackgroundImage = My.Resources.clock
            pan_threelbl.Text = "Auction Reminder"

            'Panel 4
            pan_fourimg.BackgroundImage = My.Resources.drone
            pan_fourlbl.Text = "Drone Information"

            'Panel 5
            pan_fiveimg.BackgroundImage = My.Resources.uc
            pan_fivelbl.Text = con

            'Panel 6
            pan_siximg.BackgroundImage = My.Resources.uc
            pan_sixlbl.Text = con

            'Panel 7
            pan_sevenimg.BackgroundImage = My.Resources.uc
            pan_sevenlbl.Text = con

            'Panel 8
            pan_eightimg.BackgroundImage = My.Resources.uc
            pan_eightlbl.Text = con

            'Panel 9
            pan_nineimg.BackgroundImage = My.Resources.uc
            pan_ninelbl.Text = con

        ElseIf calcs = True Then
            'Panel One 
            pan_oneimg.BackgroundImage = My.Resources.LF3
            pan_onelbl.Text = "Laser Damage"

            'Panel two
            pan_twoimg.BackgroundImage = My.Resources.Log_disk
            pan_twolbl.Text = "Log Disk Cost"

            'Panel 3
            pan_threeimg.BackgroundImage = My.Resources.Levels
            pan_threelbl.Text = "Rank"

            'Panel 4
            pan_fourimg.BackgroundImage = My.Resources.Levels
            pan_fourlbl.Text = "Kill"

            'Panel 5
            pan_fiveimg.BackgroundImage = My.Resources.hourglass
            pan_fivelbl.Text = "Kill Efficiency"

            'Panel 6
            pan_siximg.BackgroundImage = My.Resources.uc
            pan_sixlbl.Text = con

            'Panel 7
            pan_sevenimg.BackgroundImage = My.Resources.uc
            pan_sevenlbl.Text = con

            'Panel 8
            pan_eightimg.BackgroundImage = My.Resources.uc
            pan_eightlbl.Text = con

            'Panel 9
            pan_nineimg.BackgroundImage = My.Resources.uc
            pan_ninelbl.Text = con
        ElseIf info = True Then
            'Panel One 
            pan_oneimg.BackgroundImage = My.Resources.bk
            pan_onelbl.Text = "Aliens"

            'Panel two
            pan_twoimg.BackgroundImage = My.Resources.Galaxy_Gate_Zeta
            pan_twolbl.Text = "Galaxy Gates"

            'Panel 3
            pan_threeimg.BackgroundImage = My.Resources.Levels
            pan_threelbl.Text = "Levels"

            'Panel 4
            pan_fourimg.BackgroundImage = My.Resources.Links
            pan_fourlbl.Text = "Links"

            'Panel 5
            pan_fiveimg.BackgroundImage = My.Resources.Pilot_Bio
            pan_fivelbl.Text = "Pilot Bio"

            'Panel 6
            pan_siximg.BackgroundImage = My.Resources._19
            pan_sixlbl.Text = "Ranks"

            'Panel 7
            pan_sevenimg.BackgroundImage = My.Resources.ships
            pan_sevenlbl.Text = "Ships"

            'Panel 8
            pan_eightimg.BackgroundImage = My.Resources.Seprom
            pan_eightlbl.Text = "Skylab"

            'Panel 9
            pan_nineimg.BackgroundImage = My.Resources._25
            pan_ninelbl.Text = "Star System"
        End If
    End Sub

    'Menu Buttons Close, Tools, etc
    Private Sub close_pan_MouseClick(sender As Object, e As MouseEventArgs) Handles close_pan.MouseClick
        Environment.Exit(0)
    End Sub
    Private Sub min_pan_MouseClick(sender As Object, e As MouseEventArgs) Handles min_pan.MouseClick
        Me.WindowState = FormWindowState.Minimized
    End Sub
    Private Sub tools_pan_MouseClick(sender As Object, e As MouseEventArgs) Handles tools_pan.MouseClick
        Dim a As String = title & "Tools"
        Me.Text = a
        title_lbl.Text = a

        tools = True
        calcs = False
        info = False

        panelChange()
    End Sub
    Private Sub info_pan_MouseClick(sender As Object, e As MouseEventArgs) Handles info_pan.MouseClick
        Dim a As String = title & "Information"
        Me.Text = a
        title_lbl.Text = a

        tools = False
        calcs = False
        info = True

        panelChange()
    End Sub
    Private Sub calc_pan_MouseClick(sender As Object, e As MouseEventArgs) Handles calc_pan.MouseClick
        Dim a As String = title & "Calculators"
        Me.Text = a
        title_lbl.Text = a

        tools = False
        calcs = True
        info = False

        panelChange()
    End Sub
    Private Sub pan_helplbl_Click(sender As Object, e As EventArgs) Handles pan_helplbl.Click
        Help_Form.Show()
    End Sub
    Private Sub pan_about_Click(sender As Object, e As EventArgs) Handles pan_about.Click, lblInformation.Click
        About_Form.Show()
    End Sub
    Private Sub panBug_MouseClick(sender As Object, e As MouseEventArgs) Handles panBug.MouseClick
        bugPage.Show()
    End Sub
    Private Sub doPanel_MouseClick(sender As Object, e As MouseEventArgs) Handles doPanel.MouseClick
        Process.Start("www.darkorbit.com")
    End Sub

    'Menu Buttons Hover and leave methods
    Private Sub close_pan_MouseHover(sender As Object, e As EventArgs) Handles close_pan.MouseHover
        close_pan.BackColor = Color.Gray
    End Sub
    Private Sub close_pan_MouseLeave(sender As Object, e As EventArgs) Handles close_pan.MouseLeave
        close_pan.BackColor = Color.Transparent
    End Sub
    Private Sub tools_pan_MouseHover(sender As Object, e As EventArgs) Handles tools_pan.MouseHover
        tools_pan.BackColor = Color.Gray
    End Sub
    Private Sub tools_pan_MouseLeave(sender As Object, e As EventArgs) Handles tools_pan.MouseLeave
        tools_pan.BackColor = Color.Transparent
    End Sub
    Private Sub calc_pan_MouseHover(sender As Object, e As EventArgs) Handles calc_pan.MouseHover
        calc_pan.BackColor = Color.Gray
    End Sub
    Private Sub calc_pan_MouseLeave(sender As Object, e As EventArgs) Handles calc_pan.MouseLeave
        calc_pan.BackColor = Color.Transparent
    End Sub
    Private Sub info_pan_MouseHover(sender As Object, e As EventArgs) Handles info_pan.MouseHover
        info_pan.BackColor = Color.Gray
    End Sub
    Private Sub info_pan_MouseLeave(sender As Object, e As EventArgs) Handles info_pan.MouseLeave
        info_pan.BackColor = Color.Transparent
    End Sub
    Private Sub min_pan_MouseHover(sender As Object, e As EventArgs) Handles min_pan.MouseHover
        min_pan.BackColor = Color.Gray
    End Sub
    Private Sub min_pan_MouseLeave(sender As Object, e As EventArgs) Handles min_pan.MouseLeave
        min_pan.BackColor = Color.Transparent
    End Sub
    Private Sub pan_help_MouseHover(sender As Object, e As EventArgs) Handles pan_help.MouseHover, pan_helplbl.MouseHover
        pan_help.BackColor = Color.Gray
    End Sub
    Private Sub pan_help_MouseLeave(sender As Object, e As EventArgs) Handles pan_help.MouseLeave, pan_helplbl.MouseLeave
        pan_help.BackColor = Color.Transparent
    End Sub
    Private Sub pan_about_MouseHover(sender As Object, e As EventArgs) Handles pan_about.MouseHover, lblInformation.MouseHover
        pan_about.BackColor = Color.Gray
    End Sub
    Private Sub pan_about_MouseLeave(sender As Object, e As EventArgs) Handles pan_about.MouseLeave, lblInformation.MouseLeave
        pan_about.BackColor = Color.Transparent
    End Sub
    Private Sub panBug_MouseHover(sender As Object, e As EventArgs) Handles panBug.MouseHover
        panBug.BackColor = Color.Gray
    End Sub
    Private Sub panBug_MouseLeave(sender As Object, e As EventArgs) Handles panBug.MouseLeave
        panBug.BackColor = Color.Transparent
    End Sub
    Private Sub doPanel_MouseHover(sender As Object, e As EventArgs) Handles doPanel.MouseHover
        doPanel.BackColor = Color.Gray
    End Sub
    Private Sub doPanel_MouseLeave(sender As Object, e As EventArgs) Handles doPanel.MouseLeave
        doPanel.BackColor = Color.Transparent
    End Sub

    'List of the panels
    Private Sub pan_one_MouseClick(sender As Object, e As MouseEventArgs) Handles pan_one.MouseClick, pan_onelbl.MouseClick, pan_oneimg.MouseClick
        If tools = True Then
            ScreenCapture_Form.Show()
        ElseIf calcs = True Then
            Laser_Damage.passThrough(db)
            Laser_Damage.Show()
        ElseIf info = True Then
            Aliens_Form.passThrough(db)
            Aliens_Form.Show()
        End If
    End Sub
    Private Sub pan_two_MouseClick(sender As Object, e As MouseEventArgs) Handles pan_two.MouseClick, pan_twolbl.MouseClick, pan_twoimg.MouseClick
        If tools = True Then
            NoteEditor_Form.Show()
        ElseIf calcs = True Then
            Log_Disks.passThrough(db)
            Log_Disks.Show()
        ElseIf info = True Then
            GalaxyGatefrm.passThrough(db)
            GalaxyGatefrm.Show()
        End If
    End Sub
    Private Sub pan_three_MouseClick(sender As Object, e As MouseEventArgs) Handles pan_three.MouseClick, pan_threelbl.MouseClick, pan_threeimg.MouseClick
        If tools = True Then
            AuctionFrm.passThrough(db)
            AuctionFrm.Show()
        ElseIf calcs = True Then
            Main_Form.passThrough(db)
            Main_Form.Show()
        ElseIf info = True Then
            Level_Form.Show()
        End If
    End Sub
    Private Sub pan_four_MouseClick(sender As Object, e As MouseEventArgs) Handles pan_four.MouseClick, pan_fourlbl.MouseClick, pan_fourimg.MouseClick
        If tools = True Then
            DroneFormations.passThrough(db)
            DroneFormations.Show()
        ElseIf calcs = True Then
            Kill_Calculator.passThrough(db)
            Kill_Calculator.Show()
        ElseIf info = True Then
            Links.Show()
        End If
    End Sub
    Private Sub pan_five_MouseClick(sender As Object, e As MouseEventArgs) Handles pan_five.MouseClick, pan_fivelbl.MouseClick, pan_fiveimg.MouseClick
        If tools = True Then
            
        ElseIf calcs = True Then
            EfficientKilling.passThrough(db)
            EfficientKilling.Show()
        ElseIf info = True Then
            Pilot_Bio.passThrough(db)
            Pilot_Bio.Show()
        End If
    End Sub
    Private Sub pan_six_MouseClick(sender As Object, e As MouseEventArgs) Handles pan_six.MouseClick, pan_sixlbl.MouseClick, pan_siximg.MouseClick
        If tools = True Then

        ElseIf calcs = True Then

        ElseIf info = True Then
            Rank_Form.Show()
        End If
    End Sub
    Private Sub pan_seven_MouseClick(sender As Object, e As MouseEventArgs) Handles pan_seven.MouseClick, pan_sevenlbl.MouseClick, pan_sevenimg.MouseClick
        If tools = True Then

        ElseIf calcs = True Then

        ElseIf info = True Then
            Ships_Form.passThrough(db)
            Ships_Form.Show()
        End If
    End Sub
    Private Sub pan_eight_MouseClick(sender As Object, e As MouseEventArgs) Handles pan_eight.MouseClick, pan_eightlbl.MouseClick, pan_eightimg.MouseClick
        If tools = True Then

        ElseIf calcs = True Then

        ElseIf info = True Then
            Skylab.passThrough(db)
            Skylab.Show()
        End If
    End Sub
    Private Sub pan_9_MouseClick(sender As Object, e As MouseEventArgs) Handles pan_9.MouseClick, pan_ninelbl.MouseClick, pan_nineimg.MouseClick
        If tools = True Then

        ElseIf calcs = True Then

        ElseIf info = True Then
            Star_System.passThrough(db)
            Star_System.Show()
        End If
    End Sub

    'Methods to move the GUI
    Private Sub pan_header_MouseDown(sender As Object, e As MouseEventArgs) Handles pan_header.MouseDown, title_lbl.MouseDown
        If e.Button = Windows.Forms.MouseButtons.Left Then
            drag = True
            mousex = Windows.Forms.Cursor.Position.X - Me.Left
            mousey = Windows.Forms.Cursor.Position.Y - Me.Top
        End If
    End Sub
    Private Sub pan_header_MouseMove(sender As Object, e As MouseEventArgs) Handles pan_header.MouseMove, title_lbl.MouseMove
        If drag Then
            Me.Top = Windows.Forms.Cursor.Position.Y - mousey
            Me.Left = Windows.Forms.Cursor.Position.X - mousex
        End If
    End Sub
    Private Sub pan_header_MouseUp(sender As Object, e As MouseEventArgs) Handles pan_header.MouseUp, title_lbl.MouseUp
        drag = False
    End Sub
End Class