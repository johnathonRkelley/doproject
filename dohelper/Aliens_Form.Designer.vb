﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Aliens_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Aliens_Form))
        Me.Alien_Combo = New System.Windows.Forms.ComboBox()
        Me.lblAlienName = New System.Windows.Forms.Label()
        Me.hpLbl = New System.Windows.Forms.Label()
        Me.shieldsLbl = New System.Windows.Forms.Label()
        Me.epLbl = New System.Windows.Forms.Label()
        Me.lblHonor = New System.Windows.Forms.Label()
        Me.lblCredits = New System.Windows.Forms.Label()
        Me.lblUri = New System.Windows.Forms.Label()
        Me.lblMaxDamage = New System.Windows.Forms.Label()
        Me.lblSpeed = New System.Windows.Forms.Label()
        Me.alienHP = New System.Windows.Forms.Label()
        Me.alienShields = New System.Windows.Forms.Label()
        Me.alienEP = New System.Windows.Forms.Label()
        Me.alienHonor = New System.Windows.Forms.Label()
        Me.alienCredits = New System.Windows.Forms.Label()
        Me.alienUridium = New System.Windows.Forms.Label()
        Me.alienMaxDamage = New System.Windows.Forms.Label()
        Me.alienSpeed = New System.Windows.Forms.Label()
        Me.shipCollection = New System.Windows.Forms.ImageList(Me.components)
        Me.picturePanel = New System.Windows.Forms.Panel()
        Me.lblLocation = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Alien_Combo
        '
        Me.Alien_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        resources.ApplyResources(Me.Alien_Combo, "Alien_Combo")
        Me.Alien_Combo.FormattingEnabled = True
        Me.Alien_Combo.Items.AddRange(New Object() {resources.GetString("Alien_Combo.Items"), resources.GetString("Alien_Combo.Items1"), resources.GetString("Alien_Combo.Items2"), resources.GetString("Alien_Combo.Items3"), resources.GetString("Alien_Combo.Items4"), resources.GetString("Alien_Combo.Items5"), resources.GetString("Alien_Combo.Items6"), resources.GetString("Alien_Combo.Items7"), resources.GetString("Alien_Combo.Items8"), resources.GetString("Alien_Combo.Items9"), resources.GetString("Alien_Combo.Items10"), resources.GetString("Alien_Combo.Items11"), resources.GetString("Alien_Combo.Items12"), resources.GetString("Alien_Combo.Items13"), resources.GetString("Alien_Combo.Items14"), resources.GetString("Alien_Combo.Items15"), resources.GetString("Alien_Combo.Items16"), resources.GetString("Alien_Combo.Items17"), resources.GetString("Alien_Combo.Items18"), resources.GetString("Alien_Combo.Items19"), resources.GetString("Alien_Combo.Items20"), resources.GetString("Alien_Combo.Items21"), resources.GetString("Alien_Combo.Items22"), resources.GetString("Alien_Combo.Items23"), resources.GetString("Alien_Combo.Items24"), resources.GetString("Alien_Combo.Items25"), resources.GetString("Alien_Combo.Items26"), resources.GetString("Alien_Combo.Items27"), resources.GetString("Alien_Combo.Items28"), resources.GetString("Alien_Combo.Items29"), resources.GetString("Alien_Combo.Items30"), resources.GetString("Alien_Combo.Items31"), resources.GetString("Alien_Combo.Items32"), resources.GetString("Alien_Combo.Items33"), resources.GetString("Alien_Combo.Items34"), resources.GetString("Alien_Combo.Items35"), resources.GetString("Alien_Combo.Items36"), resources.GetString("Alien_Combo.Items37"), resources.GetString("Alien_Combo.Items38"), resources.GetString("Alien_Combo.Items39"), resources.GetString("Alien_Combo.Items40"), resources.GetString("Alien_Combo.Items41"), resources.GetString("Alien_Combo.Items42"), resources.GetString("Alien_Combo.Items43"), resources.GetString("Alien_Combo.Items44"), resources.GetString("Alien_Combo.Items45"), resources.GetString("Alien_Combo.Items46"), resources.GetString("Alien_Combo.Items47"), resources.GetString("Alien_Combo.Items48"), resources.GetString("Alien_Combo.Items49"), resources.GetString("Alien_Combo.Items50"), resources.GetString("Alien_Combo.Items51"), resources.GetString("Alien_Combo.Items52"), resources.GetString("Alien_Combo.Items53"), resources.GetString("Alien_Combo.Items54"), resources.GetString("Alien_Combo.Items55"), resources.GetString("Alien_Combo.Items56"), resources.GetString("Alien_Combo.Items57"), resources.GetString("Alien_Combo.Items58"), resources.GetString("Alien_Combo.Items59"), resources.GetString("Alien_Combo.Items60"), resources.GetString("Alien_Combo.Items61"), resources.GetString("Alien_Combo.Items62"), resources.GetString("Alien_Combo.Items63")})
        Me.Alien_Combo.Name = "Alien_Combo"
        Me.Alien_Combo.Sorted = True
        '
        'lblAlienName
        '
        resources.ApplyResources(Me.lblAlienName, "lblAlienName")
        Me.lblAlienName.Name = "lblAlienName"
        '
        'hpLbl
        '
        resources.ApplyResources(Me.hpLbl, "hpLbl")
        Me.hpLbl.Name = "hpLbl"
        '
        'shieldsLbl
        '
        resources.ApplyResources(Me.shieldsLbl, "shieldsLbl")
        Me.shieldsLbl.Name = "shieldsLbl"
        '
        'epLbl
        '
        resources.ApplyResources(Me.epLbl, "epLbl")
        Me.epLbl.Name = "epLbl"
        '
        'lblHonor
        '
        resources.ApplyResources(Me.lblHonor, "lblHonor")
        Me.lblHonor.Name = "lblHonor"
        '
        'lblCredits
        '
        resources.ApplyResources(Me.lblCredits, "lblCredits")
        Me.lblCredits.Name = "lblCredits"
        '
        'lblUri
        '
        resources.ApplyResources(Me.lblUri, "lblUri")
        Me.lblUri.Name = "lblUri"
        '
        'lblMaxDamage
        '
        resources.ApplyResources(Me.lblMaxDamage, "lblMaxDamage")
        Me.lblMaxDamage.Name = "lblMaxDamage"
        '
        'lblSpeed
        '
        resources.ApplyResources(Me.lblSpeed, "lblSpeed")
        Me.lblSpeed.Name = "lblSpeed"
        '
        'alienHP
        '
        resources.ApplyResources(Me.alienHP, "alienHP")
        Me.alienHP.Name = "alienHP"
        '
        'alienShields
        '
        resources.ApplyResources(Me.alienShields, "alienShields")
        Me.alienShields.Name = "alienShields"
        '
        'alienEP
        '
        resources.ApplyResources(Me.alienEP, "alienEP")
        Me.alienEP.Name = "alienEP"
        '
        'alienHonor
        '
        resources.ApplyResources(Me.alienHonor, "alienHonor")
        Me.alienHonor.Name = "alienHonor"
        '
        'alienCredits
        '
        resources.ApplyResources(Me.alienCredits, "alienCredits")
        Me.alienCredits.Name = "alienCredits"
        '
        'alienUridium
        '
        resources.ApplyResources(Me.alienUridium, "alienUridium")
        Me.alienUridium.Name = "alienUridium"
        '
        'alienMaxDamage
        '
        resources.ApplyResources(Me.alienMaxDamage, "alienMaxDamage")
        Me.alienMaxDamage.Name = "alienMaxDamage"
        '
        'alienSpeed
        '
        resources.ApplyResources(Me.alienSpeed, "alienSpeed")
        Me.alienSpeed.Name = "alienSpeed"
        '
        'shipCollection
        '
        Me.shipCollection.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        resources.ApplyResources(Me.shipCollection, "shipCollection")
        Me.shipCollection.TransparentColor = System.Drawing.Color.Transparent
        '
        'picturePanel
        '
        Me.picturePanel.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.picturePanel.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.small_streuner
        resources.ApplyResources(Me.picturePanel, "picturePanel")
        Me.picturePanel.Name = "picturePanel"
        '
        'lblLocation
        '
        resources.ApplyResources(Me.lblLocation, "lblLocation")
        Me.lblLocation.Name = "lblLocation"
        '
        'Aliens_Form
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblLocation)
        Me.Controls.Add(Me.picturePanel)
        Me.Controls.Add(Me.alienSpeed)
        Me.Controls.Add(Me.alienMaxDamage)
        Me.Controls.Add(Me.alienUridium)
        Me.Controls.Add(Me.alienCredits)
        Me.Controls.Add(Me.alienHonor)
        Me.Controls.Add(Me.alienEP)
        Me.Controls.Add(Me.alienShields)
        Me.Controls.Add(Me.alienHP)
        Me.Controls.Add(Me.lblSpeed)
        Me.Controls.Add(Me.lblMaxDamage)
        Me.Controls.Add(Me.lblUri)
        Me.Controls.Add(Me.lblCredits)
        Me.Controls.Add(Me.lblHonor)
        Me.Controls.Add(Me.epLbl)
        Me.Controls.Add(Me.shieldsLbl)
        Me.Controls.Add(Me.hpLbl)
        Me.Controls.Add(Me.lblAlienName)
        Me.Controls.Add(Me.Alien_Combo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Aliens_Form"
        Me.ShowIcon = False
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Alien_Combo As System.Windows.Forms.ComboBox
    Friend WithEvents lblAlienName As System.Windows.Forms.Label
    Friend WithEvents hpLbl As System.Windows.Forms.Label
    Friend WithEvents shieldsLbl As System.Windows.Forms.Label
    Friend WithEvents epLbl As System.Windows.Forms.Label
    Friend WithEvents lblHonor As System.Windows.Forms.Label
    Friend WithEvents lblCredits As System.Windows.Forms.Label
    Friend WithEvents lblUri As System.Windows.Forms.Label
    Friend WithEvents lblMaxDamage As System.Windows.Forms.Label
    Friend WithEvents lblSpeed As System.Windows.Forms.Label
    Friend WithEvents alienHP As System.Windows.Forms.Label
    Friend WithEvents alienShields As System.Windows.Forms.Label
    Friend WithEvents alienEP As System.Windows.Forms.Label
    Friend WithEvents alienHonor As System.Windows.Forms.Label
    Friend WithEvents alienCredits As System.Windows.Forms.Label
    Friend WithEvents alienUridium As System.Windows.Forms.Label
    Friend WithEvents alienMaxDamage As System.Windows.Forms.Label
    Friend WithEvents alienSpeed As System.Windows.Forms.Label
    Friend WithEvents picturePanel As System.Windows.Forms.Panel
    Friend WithEvents shipCollection As System.Windows.Forms.ImageList
    Friend WithEvents lblLocation As System.Windows.Forms.Label
End Class
