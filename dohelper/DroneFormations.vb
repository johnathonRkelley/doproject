﻿Imports System.Data.OleDb

Public Class DroneFormations

    'Database variable
    Dim db As Database

    'Load 
    Private Sub Pilot_Bio_Load(sender As Object, e As EventArgs) Handles Me.Load
        cbSkills.SelectedIndex = 0

        change()
    End Sub

    'Selected Index Changes
    Private Sub cbSkills_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSkills.SelectedIndexChanged
        If db Is Nothing Then

        Else
            change()
        End If
    End Sub

    'Passes the database to the current form.
    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    'Changes the data.
    Public Sub change()
        'Local Variables
        Dim fileLocation As String = Application.StartupPath & "\images\DRONES\"

        Dim picFile As String
        Dim pictureFile As String

        'Dataset being initalized
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds.Tables.Add(dt)

        'Name of the Skill
        Dim name As String
        name = cbSkills.SelectedItem.ToString()

        Label1.Text = name

        'The CMD Line query
        Dim cmd As String = "SELECT * FROM Formations where Formation = '" & name & "'"

        'Query
        Dim da As New OleDbDataAdapter
        da = db.getOleDbAdapter(cmd)
        'da = New OleDbDataAdapter("SELECT * FROM npcs where name = '" & name & "'", con)

        'Fills data
        da.Fill(dt)

        'For the database work the columns are as follows
        'ID | Skill | Type | Max Level | CurLevel | Description | Effect | Bonus | Filename

        'Sets the text fields for each of them
        txtDescription.Text = dt.Rows(0).Item(4)

        'Use this for the Picture of the skills
        picFile = dt.Rows(0).Item(5).ToString
        pictureFile = fileLocation & picFile
        picturePan.BackgroundImage = System.Drawing.Image.FromFile(pictureFile)
    End Sub
End Class