﻿Public Class ScreenCapture_Form
    'Buttons
    Private Sub Capture_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Capture_Button.Click
        Me.Hide()
        Dim bounds As Rectangle
        Dim screenshot As System.Drawing.Bitmap
        Dim graph As Graphics
        bounds = Screen.PrimaryScreen.Bounds
        screenshot = New System.Drawing.Bitmap(bounds.Width, bounds.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
        graph = Graphics.FromImage(screenshot)
        graph.CopyFromScreen(bounds.X, bounds.Y, 0, 0, bounds.Size, CopyPixelOperation.SourceCopy)
        PictureBox1.Image = screenshot
        Me.Show()

        Dim SaveFileDialog1 As New SaveFileDialog()
        SaveFileDialog1.Filter = "PNG|*.png|GIF|*.gif|JPG|*.jpg"
        Dim pic As Image
        pic = PictureBox1.Image
        If SaveFileDialog1.ShowDialog() <> Windows.Forms.DialogResult.Cancel Then
            pic.Save(SaveFileDialog1.FileName)
        ElseIf Windows.Forms.DialogResult.Cancel Then
        End If
        PictureBox1.Image = Nothing
    End Sub
    Private Sub Close_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Close_Button.Click
        Me.Close()
    End Sub
End Class