﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Log_Disks
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Log_Disks))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtDesiredPP = New System.Windows.Forms.TextBox()
        Me.txtCurrentLD = New System.Windows.Forms.TextBox()
        Me.txtBeginningPP = New System.Windows.Forms.TextBox()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lbTotalCost = New System.Windows.Forms.Label()
        Me.lblCostRebate = New System.Windows.Forms.Label()
        Me.lblCostPremium = New System.Windows.Forms.Label()
        Me.lblBoth = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 9)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(159, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Beginning PIlot Point :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 34)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(185, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Current Log Disk Amount :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 57)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(153, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Desired Pilot Points :"
        '
        'txtDesiredPP
        '
        Me.txtDesiredPP.Location = New System.Drawing.Point(220, 56)
        Me.txtDesiredPP.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDesiredPP.MaxLength = 2
        Me.txtDesiredPP.Name = "txtDesiredPP"
        Me.txtDesiredPP.Size = New System.Drawing.Size(48, 20)
        Me.txtDesiredPP.TabIndex = 3
        '
        'txtCurrentLD
        '
        Me.txtCurrentLD.Location = New System.Drawing.Point(220, 33)
        Me.txtCurrentLD.Margin = New System.Windows.Forms.Padding(2)
        Me.txtCurrentLD.Name = "txtCurrentLD"
        Me.txtCurrentLD.Size = New System.Drawing.Size(48, 20)
        Me.txtCurrentLD.TabIndex = 2
        '
        'txtBeginningPP
        '
        Me.txtBeginningPP.Location = New System.Drawing.Point(220, 8)
        Me.txtBeginningPP.Margin = New System.Windows.Forms.Padding(2)
        Me.txtBeginningPP.MaxLength = 2
        Me.txtBeginningPP.Name = "txtBeginningPP"
        Me.txtBeginningPP.Size = New System.Drawing.Size(48, 20)
        Me.txtBeginningPP.TabIndex = 1
        '
        'btnCalculate
        '
        Me.btnCalculate.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnCalculate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCalculate.Location = New System.Drawing.Point(21, 90)
        Me.btnCalculate.Margin = New System.Windows.Forms.Padding(2)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(65, 19)
        Me.btnCalculate.TabIndex = 4
        Me.btnCalculate.Text = "Analyze" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(13, 116)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(73, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Total Cost :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(13, 142)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(137, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Total Cost w/ Rebate :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(13, 167)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(143, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Total Cost w/ Premium :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(13, 193)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(97, 13)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Cost with Both :"
        '
        'lbTotalCost
        '
        Me.lbTotalCost.AutoSize = True
        Me.lbTotalCost.Location = New System.Drawing.Point(195, 116)
        Me.lbTotalCost.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbTotalCost.Name = "lbTotalCost"
        Me.lbTotalCost.Size = New System.Drawing.Size(32, 13)
        Me.lbTotalCost.TabIndex = 15
        Me.lbTotalCost.Text = "Cost"
        '
        'lblCostRebate
        '
        Me.lblCostRebate.AutoSize = True
        Me.lblCostRebate.Location = New System.Drawing.Point(195, 142)
        Me.lblCostRebate.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblCostRebate.Name = "lblCostRebate"
        Me.lblCostRebate.Size = New System.Drawing.Size(92, 13)
        Me.lblCostRebate.TabIndex = 16
        Me.lblCostRebate.Text = "Cost w/Rebate"
        '
        'lblCostPremium
        '
        Me.lblCostPremium.AutoSize = True
        Me.lblCostPremium.Location = New System.Drawing.Point(195, 167)
        Me.lblCostPremium.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblCostPremium.Name = "lblCostPremium"
        Me.lblCostPremium.Size = New System.Drawing.Size(98, 13)
        Me.lblCostPremium.TabIndex = 17
        Me.lblCostPremium.Text = "Cost w/Premium"
        '
        'lblBoth
        '
        Me.lblBoth.AutoSize = True
        Me.lblBoth.Location = New System.Drawing.Point(196, 193)
        Me.lblBoth.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblBoth.Name = "lblBoth"
        Me.lblBoth.Size = New System.Drawing.Size(81, 13)
        Me.lblBoth.TabIndex = 18
        Me.lblBoth.Text = "Full Discount"
        '
        'Panel1
        '
        Me.Panel1.BackgroundImage = CType(resources.GetObject("Panel1.BackgroundImage"), System.Drawing.Image)
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Location = New System.Drawing.Point(298, 6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(216, 200)
        Me.Panel1.TabIndex = 19
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(195, 90)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 13)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Uridium"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Log_Disks
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(525, 217)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lblBoth)
        Me.Controls.Add(Me.lblCostPremium)
        Me.Controls.Add(Me.lblCostRebate)
        Me.Controls.Add(Me.lbTotalCost)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btnCalculate)
        Me.Controls.Add(Me.txtBeginningPP)
        Me.Controls.Add(Me.txtCurrentLD)
        Me.Controls.Add(Me.txtDesiredPP)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Log_Disks"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Log Disk Calculator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtDesiredPP As System.Windows.Forms.TextBox
    Friend WithEvents txtCurrentLD As System.Windows.Forms.TextBox
    Friend WithEvents txtBeginningPP As System.Windows.Forms.TextBox
    Friend WithEvents btnCalculate As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lbTotalCost As System.Windows.Forms.Label
    Friend WithEvents lblCostRebate As System.Windows.Forms.Label
    Friend WithEvents lblCostPremium As System.Windows.Forms.Label
    Friend WithEvents lblBoth As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As Label
End Class
