﻿Imports System.Data.OleDb

Public Class Skylab
    'Database variable
    Dim db As Database

    'Load
    Private Sub Skylab_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cb.SelectedIndex = 0

        lblName.Text = cb.SelectedItem.ToString

        change()
    End Sub

    'Passes the database to the current form.
    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    'Changes the data.
    Public Sub change()
        'Dataset being initalized
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds.Tables.Add(dt)

        'Name of the Skill
        Dim name As String
        name = cb.SelectedItem.ToString()

        'The CMD Line query
        Dim cmd As String = "SELECT * FROM Skylab where name = '" & name & "'"

        'Query
        Dim da As New OleDbDataAdapter
        da = db.getOleDbAdapter(cmd)
        'da = New OleDbDataAdapter("SELECT * FROM npcs where name = '" & name & "'", con)

        'Fills data
        da.Fill(dt)

        'For the database work the columns are as follows
        'ID | Name | Description 

        'Sets the text fields for each of them
        lblName.Text = dt.Rows(0).Item(1)
        txtInfo.Text = dt.Rows(0).Item(2)

        'Use this for the Picture of the skills
        'picFile = dt.Rows(0).Item(10).ToString
        'pictureFile = fileLocation & picFile
        'picturePan.BackgroundImage = System.Drawing.Image.FromFile(pictureFile)
        'picturePan.BackColor = Color.AliceBlue
    End Sub

    Private Sub cb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb.SelectedIndexChanged
        If db Is Nothing Then

        Else
            change()
        End If
    End Sub
End Class