﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AuctionFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AuctionFrm))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.timeLabel = New System.Windows.Forms.Label()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MinuteWarningOnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MinuteWarningOnToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MinuteWarningOnToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'Label1
        '
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(0, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(331, 46)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Your friendly auction reminder wants to remind you that there is "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'timeLabel
        '
        Me.timeLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.timeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.timeLabel.Location = New System.Drawing.Point(0, 65)
        Me.timeLabel.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.timeLabel.Name = "timeLabel"
        Me.timeLabel.Size = New System.Drawing.Size(331, 46)
        Me.timeLabel.TabIndex = 1
        Me.timeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Text = "Auction Reminder"
        Me.NotifyIcon1.Visible = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SettingsToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(331, 24)
        Me.MenuStrip1.TabIndex = 2
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.AutoToolTip = True
        Me.SettingsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MinuteWarningOnToolStripMenuItem, Me.MinuteWarningOnToolStripMenuItem1, Me.MinuteWarningOnToolStripMenuItem2})
        Me.SettingsToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.ShortcutKeyDisplayString = ""
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(65, 20)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'MinuteWarningOnToolStripMenuItem
        '
        Me.MinuteWarningOnToolStripMenuItem.Name = "MinuteWarningOnToolStripMenuItem"
        Me.MinuteWarningOnToolStripMenuItem.Size = New System.Drawing.Size(208, 22)
        Me.MinuteWarningOnToolStripMenuItem.Text = "1 Minute Warning (On)"
        '
        'MinuteWarningOnToolStripMenuItem1
        '
        Me.MinuteWarningOnToolStripMenuItem1.Name = "MinuteWarningOnToolStripMenuItem1"
        Me.MinuteWarningOnToolStripMenuItem1.Size = New System.Drawing.Size(208, 22)
        Me.MinuteWarningOnToolStripMenuItem1.Text = "5 Minute Warning (On)"
        '
        'MinuteWarningOnToolStripMenuItem2
        '
        Me.MinuteWarningOnToolStripMenuItem2.Name = "MinuteWarningOnToolStripMenuItem2"
        Me.MinuteWarningOnToolStripMenuItem2.Size = New System.Drawing.Size(208, 22)
        Me.MinuteWarningOnToolStripMenuItem2.Text = "10 Minute Warning (On)"
        '
        'AuctionFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 111)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.timeLabel)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AuctionFrm"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Auction Reminder"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents timeLabel As System.Windows.Forms.Label
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents SettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MinuteWarningOnToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MinuteWarningOnToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MinuteWarningOnToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
End Class
