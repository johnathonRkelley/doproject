﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Laser_Damage
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Laser_Damage))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.labelDamage = New System.Windows.Forms.Label()
        Me.lblMax = New System.Windows.Forms.Label()
        Me.btnCalc = New System.Windows.Forms.Button()
        Me.laserCb = New System.Windows.Forms.ComboBox()
        Me.rockCb = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtShipBonus = New System.Windows.Forms.TextBox()
        Me.txtHavocs = New System.Windows.Forms.TextBox()
        Me.droneCb = New System.Windows.Forms.ComboBox()
        Me.txtDrones = New System.Windows.Forms.TextBox()
        Me.txtLaser = New System.Windows.Forms.TextBox()
        Me.txtRock = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblBoosters = New System.Windows.Forms.Label()
        Me.boosterCb = New System.Windows.Forms.ComboBox()
        Me.cbLaser = New System.Windows.Forms.ComboBox()
        Me.cbShipBonus = New System.Windows.Forms.ComboBox()
        Me.txtLaserUp = New System.Windows.Forms.TextBox()
        Me.txtBoosters = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtNumber = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cbDesign = New System.Windows.Forms.ComboBox()
        Me.txtDrone = New System.Windows.Forms.TextBox()
        Me.cbHavocs = New System.Windows.Forms.ComboBox()
        Me.cbLevel6 = New System.Windows.Forms.ComboBox()
        Me.cbAmmo = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Laser Type : "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 71)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Rocks?"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(14, 98)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(121, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Ship Design Bonus?"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 122)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "On Drones? "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 148)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Level 6 Drones?"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(14, 228)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Level 16 Laser?"
        '
        'labelDamage
        '
        Me.labelDamage.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelDamage.Location = New System.Drawing.Point(215, 219)
        Me.labelDamage.Name = "labelDamage"
        Me.labelDamage.Size = New System.Drawing.Size(299, 132)
        Me.labelDamage.TabIndex = 8
        Me.labelDamage.Text = "Damage  :"
        Me.labelDamage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblMax
        '
        Me.lblMax.AutoSize = True
        Me.lblMax.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMax.Location = New System.Drawing.Point(513, 268)
        Me.lblMax.Name = "lblMax"
        Me.lblMax.Size = New System.Drawing.Size(0, 24)
        Me.lblMax.TabIndex = 9
        '
        'btnCalc
        '
        Me.btnCalc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnCalc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCalc.Location = New System.Drawing.Point(82, 330)
        Me.btnCalc.Name = "btnCalc"
        Me.btnCalc.Size = New System.Drawing.Size(87, 21)
        Me.btnCalc.TabIndex = 10
        Me.btnCalc.Text = "Calculate"
        Me.btnCalc.UseVisualStyleBackColor = True
        '
        'laserCb
        '
        Me.laserCb.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.laserCb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.laserCb.FormattingEnabled = True
        Me.laserCb.Items.AddRange(New Object() {"LF-1", "MP-1", "LF-2", "LF-3", "SL-01", "SLL-01", "SLL-02", "LF-4"})
        Me.laserCb.Location = New System.Drawing.Point(135, 12)
        Me.laserCb.Name = "laserCb"
        Me.laserCb.Size = New System.Drawing.Size(59, 21)
        Me.laserCb.TabIndex = 11
        '
        'rockCb
        '
        Me.rockCb.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.rockCb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rockCb.FormattingEnabled = True
        Me.rockCb.Items.AddRange(New Object() {"None", "Prometid", "Promer.", "Seprom"})
        Me.rockCb.Location = New System.Drawing.Point(135, 68)
        Me.rockCb.Name = "rockCb"
        Me.rockCb.Size = New System.Drawing.Size(72, 21)
        Me.rockCb.TabIndex = 12
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(15, 171)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(58, 13)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "Havocs :"
        '
        'txtShipBonus
        '
        Me.txtShipBonus.Enabled = False
        Me.txtShipBonus.Location = New System.Drawing.Point(215, 94)
        Me.txtShipBonus.Name = "txtShipBonus"
        Me.txtShipBonus.Size = New System.Drawing.Size(30, 20)
        Me.txtShipBonus.TabIndex = 14
        Me.txtShipBonus.Text = ".05"
        Me.txtShipBonus.Visible = False
        '
        'txtHavocs
        '
        Me.txtHavocs.Enabled = False
        Me.txtHavocs.Location = New System.Drawing.Point(-14, 157)
        Me.txtHavocs.Name = "txtHavocs"
        Me.txtHavocs.Size = New System.Drawing.Size(24, 20)
        Me.txtHavocs.TabIndex = 15
        Me.txtHavocs.Text = ".1"
        Me.txtHavocs.Visible = False
        '
        'droneCb
        '
        Me.droneCb.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.droneCb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.droneCb.FormattingEnabled = True
        Me.droneCb.Items.AddRange(New Object() {"Yes", "No"})
        Me.droneCb.Location = New System.Drawing.Point(135, 119)
        Me.droneCb.Name = "droneCb"
        Me.droneCb.Size = New System.Drawing.Size(72, 21)
        Me.droneCb.TabIndex = 16
        '
        'txtDrones
        '
        Me.txtDrones.Enabled = False
        Me.txtDrones.Location = New System.Drawing.Point(-13, 134)
        Me.txtDrones.Name = "txtDrones"
        Me.txtDrones.Size = New System.Drawing.Size(25, 20)
        Me.txtDrones.TabIndex = 17
        Me.txtDrones.Text = ".10"
        Me.txtDrones.Visible = False
        '
        'txtLaser
        '
        Me.txtLaser.Enabled = False
        Me.txtLaser.Location = New System.Drawing.Point(202, 12)
        Me.txtLaser.Name = "txtLaser"
        Me.txtLaser.Size = New System.Drawing.Size(42, 20)
        Me.txtLaser.TabIndex = 18
        Me.txtLaser.Text = "150"
        Me.txtLaser.Visible = False
        '
        'txtRock
        '
        Me.txtRock.Enabled = False
        Me.txtRock.Location = New System.Drawing.Point(215, 68)
        Me.txtRock.Name = "txtRock"
        Me.txtRock.Size = New System.Drawing.Size(30, 20)
        Me.txtRock.TabIndex = 19
        Me.txtRock.Text = ".15"
        Me.txtRock.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Silver
        Me.Panel1.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.LF3
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.txtDrones)
        Me.Panel1.Controls.Add(Me.txtHavocs)
        Me.Panel1.Location = New System.Drawing.Point(251, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(263, 197)
        Me.Panel1.TabIndex = 20
        '
        'lblBoosters
        '
        Me.lblBoosters.AutoSize = True
        Me.lblBoosters.Location = New System.Drawing.Point(14, 199)
        Me.lblBoosters.Name = "lblBoosters"
        Me.lblBoosters.Size = New System.Drawing.Size(67, 13)
        Me.lblBoosters.TabIndex = 21
        Me.lblBoosters.Text = "Boosters? "
        '
        'boosterCb
        '
        Me.boosterCb.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.boosterCb.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.boosterCb.FormattingEnabled = True
        Me.boosterCb.Items.AddRange(New Object() {"None", "Single", "Double"})
        Me.boosterCb.Location = New System.Drawing.Point(135, 196)
        Me.boosterCb.Name = "boosterCb"
        Me.boosterCb.Size = New System.Drawing.Size(72, 21)
        Me.boosterCb.TabIndex = 22
        Me.boosterCb.Text = "None"
        '
        'cbLaser
        '
        Me.cbLaser.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbLaser.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbLaser.FormattingEnabled = True
        Me.cbLaser.Items.AddRange(New Object() {"Yes", "No"})
        Me.cbLaser.Location = New System.Drawing.Point(135, 223)
        Me.cbLaser.Name = "cbLaser"
        Me.cbLaser.Size = New System.Drawing.Size(72, 21)
        Me.cbLaser.TabIndex = 23
        '
        'cbShipBonus
        '
        Me.cbShipBonus.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbShipBonus.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbShipBonus.FormattingEnabled = True
        Me.cbShipBonus.Items.AddRange(New Object() {"Yes", "No"})
        Me.cbShipBonus.Location = New System.Drawing.Point(135, 95)
        Me.cbShipBonus.Name = "cbShipBonus"
        Me.cbShipBonus.Size = New System.Drawing.Size(72, 21)
        Me.cbShipBonus.TabIndex = 24
        '
        'txtLaserUp
        '
        Me.txtLaserUp.Enabled = False
        Me.txtLaserUp.Location = New System.Drawing.Point(215, 224)
        Me.txtLaserUp.Name = "txtLaserUp"
        Me.txtLaserUp.Size = New System.Drawing.Size(30, 20)
        Me.txtLaserUp.TabIndex = 25
        Me.txtLaserUp.Text = ".06"
        Me.txtLaserUp.Visible = False
        '
        'txtBoosters
        '
        Me.txtBoosters.Location = New System.Drawing.Point(215, 196)
        Me.txtBoosters.Name = "txtBoosters"
        Me.txtBoosters.Size = New System.Drawing.Size(30, 20)
        Me.txtBoosters.TabIndex = 26
        Me.txtBoosters.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(15, 42)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(81, 13)
        Me.Label7.TabIndex = 27
        Me.Label7.Text = "# Of Lasers :"
        '
        'txtNumber
        '
        Me.txtNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumber.Location = New System.Drawing.Point(135, 42)
        Me.txtNumber.MaxLength = 2
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.Size = New System.Drawing.Size(25, 20)
        Me.txtNumber.TabIndex = 28
        Me.txtNumber.Text = "1"
        Me.txtNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(14, 252)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(92, 13)
        Me.Label8.TabIndex = 29
        Me.Label8.Text = "Drone Design :"
        '
        'cbDesign
        '
        Me.cbDesign.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbDesign.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDesign.FormattingEnabled = True
        Me.cbDesign.Items.AddRange(New Object() {"None", "Arrow", "Barrage", "Bat", "Drill", "Heart", "Pincer", "Ring", "Turtle", "Veteran", "Wheel", "X"})
        Me.cbDesign.Location = New System.Drawing.Point(135, 249)
        Me.cbDesign.Name = "cbDesign"
        Me.cbDesign.Size = New System.Drawing.Size(72, 21)
        Me.cbDesign.TabIndex = 30
        Me.cbDesign.Text = "None"
        '
        'txtDrone
        '
        Me.txtDrone.Location = New System.Drawing.Point(215, 250)
        Me.txtDrone.Name = "txtDrone"
        Me.txtDrone.Size = New System.Drawing.Size(30, 20)
        Me.txtDrone.TabIndex = 31
        Me.txtDrone.Visible = False
        '
        'cbHavocs
        '
        Me.cbHavocs.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbHavocs.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbHavocs.FormattingEnabled = True
        Me.cbHavocs.Items.AddRange(New Object() {"Yes", "No"})
        Me.cbHavocs.Location = New System.Drawing.Point(135, 168)
        Me.cbHavocs.Name = "cbHavocs"
        Me.cbHavocs.Size = New System.Drawing.Size(58, 21)
        Me.cbHavocs.TabIndex = 32
        '
        'cbLevel6
        '
        Me.cbLevel6.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbLevel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbLevel6.FormattingEnabled = True
        Me.cbLevel6.Items.AddRange(New Object() {"Yes", "No"})
        Me.cbLevel6.Location = New System.Drawing.Point(135, 144)
        Me.cbLevel6.Name = "cbLevel6"
        Me.cbLevel6.Size = New System.Drawing.Size(59, 21)
        Me.cbLevel6.TabIndex = 33
        '
        'cbAmmo
        '
        Me.cbAmmo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cbAmmo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAmmo.FormattingEnabled = True
        Me.cbAmmo.Items.AddRange(New Object() {"x1", "x2", "x3", "x4", "RSB-75"})
        Me.cbAmmo.Location = New System.Drawing.Point(135, 276)
        Me.cbAmmo.Name = "cbAmmo"
        Me.cbAmmo.Size = New System.Drawing.Size(72, 21)
        Me.cbAmmo.TabIndex = 34
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(15, 279)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(80, 13)
        Me.Label9.TabIndex = 35
        Me.Label9.Text = "Ammo Type :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(15, 306)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(61, 13)
        Me.Label10.TabIndex = 36
        Me.Label10.Text = "Pilot Bio?"
        '
        'ComboBox1
        '
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"None", "Bounty Hunter I", "Bounty Hunter I & II", "Alien Hunter"})
        Me.ComboBox1.Location = New System.Drawing.Point(82, 303)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(125, 21)
        Me.ComboBox1.TabIndex = 37
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(215, 303)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(30, 20)
        Me.TextBox1.TabIndex = 38
        Me.TextBox1.Visible = False
        '
        'Laser_Damage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(527, 365)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.cbAmmo)
        Me.Controls.Add(Me.cbLevel6)
        Me.Controls.Add(Me.cbHavocs)
        Me.Controls.Add(Me.cbDesign)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtNumber)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cbShipBonus)
        Me.Controls.Add(Me.cbLaser)
        Me.Controls.Add(Me.boosterCb)
        Me.Controls.Add(Me.lblBoosters)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.droneCb)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.rockCb)
        Me.Controls.Add(Me.laserCb)
        Me.Controls.Add(Me.btnCalc)
        Me.Controls.Add(Me.lblMax)
        Me.Controls.Add(Me.labelDamage)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtDrone)
        Me.Controls.Add(Me.txtBoosters)
        Me.Controls.Add(Me.txtLaserUp)
        Me.Controls.Add(Me.txtRock)
        Me.Controls.Add(Me.txtLaser)
        Me.Controls.Add(Me.txtShipBonus)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Laser_Damage"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Laser Damage Calculator"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents labelDamage As System.Windows.Forms.Label
    Friend WithEvents lblMax As System.Windows.Forms.Label
    Friend WithEvents btnCalc As System.Windows.Forms.Button
    Friend WithEvents laserCb As System.Windows.Forms.ComboBox
    Friend WithEvents rockCb As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtShipBonus As System.Windows.Forms.TextBox
    Friend WithEvents txtHavocs As System.Windows.Forms.TextBox
    Friend WithEvents droneCb As System.Windows.Forms.ComboBox
    Friend WithEvents txtDrones As System.Windows.Forms.TextBox
    Friend WithEvents txtLaser As System.Windows.Forms.TextBox
    Friend WithEvents txtRock As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblBoosters As System.Windows.Forms.Label
    Friend WithEvents boosterCb As System.Windows.Forms.ComboBox
    Friend WithEvents cbLaser As System.Windows.Forms.ComboBox
    Friend WithEvents cbShipBonus As System.Windows.Forms.ComboBox
    Friend WithEvents txtLaserUp As System.Windows.Forms.TextBox
    Friend WithEvents txtBoosters As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cbDesign As System.Windows.Forms.ComboBox
    Friend WithEvents txtDrone As System.Windows.Forms.TextBox
    Friend WithEvents cbHavocs As System.Windows.Forms.ComboBox
    Friend WithEvents cbLevel6 As System.Windows.Forms.ComboBox
    Friend WithEvents cbAmmo As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
End Class
