﻿Imports System.Data.OleDb

Public Class Laser_Damage
    'Intialized Global Variables
    Dim db As Database
    Dim ammoType As Integer

    'Database is passed through
    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    'Load method
    Private Sub Laser_Damage_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        laserCb.SelectedItem = "LF-3"
        rockCb.SelectedIndex = 0
        droneCb.SelectedIndex = 1
        cbLaser.SelectedIndex = 0
        cbShipBonus.SelectedIndex = 0
        ammoType = 1
        cbAmmo.SelectedIndex = 0
        labelDamage.Text = Nothing

    End Sub

    'Buttons
    Private Sub btnCalc_Click(sender As Object, e As EventArgs) Handles btnCalc.Click
        'Gets start value
        Dim total As Double
        Dim s As String = txtLaser.Text

        Dim runningTot As Double = s * txtNumber.Text

        'Checks for rock increase
        If rockCb.SelectedIndex = 0 Then
            'Do Nothing
        Else
            runningTot = runningTot * (1 + txtRock.Text)
        End If

        'Checks for Ship Bonus
        If cbShipBonus.SelectedIndex = 0 Then
            runningTot = runningTot * (1 + txtShipBonus.Text)
        End If

        'Checks to see if the laser is on the drones
        Dim onDrones As Boolean
        If droneCb.SelectedIndex = 0 Then
            onDrones = True
        Else
            onDrones = False
        End If


        'Does calculations according to if they are on the drones or not
        If onDrones = True Then
            'Level 6 Drones
            If cbLevel6.SelectedIndex = 0 Then
                runningTot = runningTot * (1 + txtDrones.Text)
            End If

            If cbHavocs.SelectedIndex = 0 Then
                runningTot = runningTot * (1 + txtHavocs.Text)
            End If
            'total = runningTot
        Else
            'total = runningTot
        End If

        'Boosters
        If boosterCb.SelectedIndex = 1 Then
            runningTot = runningTot * (1 + txtBoosters.Text)
        ElseIf boosterCb.SelectedIndex = 2 Then
            runningTot = runningTot * (1 + txtBoosters.Text)
        End If

        'Level 16 Laser
        If cbLaser.SelectedIndex = 0 Then
            runningTot = runningTot * (1 + txtLaserUp.Text)
        End If

        total = runningTot

        'Drone Design
        Dim maxLength As Integer
        maxLength = txtDrone.TextLength

        If txtDrone.Text = Nothing Then
        ElseIf txtDrone.Text.Substring(0, 1) = "-" Then
            Dim neg As Double = txtDrone.Text.Substring(1, maxLength - 1)
            runningTot = runningTot - (neg * runningTot)
        Else
            runningTot = runningTot * (1 + txtDrone.Text)
        End If

        total = runningTot

        If TextBox1.Text = Nothing Then
            'Do Nothing
        Else
            total = total * (1 + TextBox1.Text)
        End If


        'Times the total by the 
        total = total * ammoType

        'Gets the minimum and max damage
        Dim min, max As Integer
        min = total * 0.75
        max = total

        'Puts the min and max in the label
        labelDamage.Text = "Damage : " & min & " - " & max
    End Sub

    'Combo Box Selected Index Changes
    Private Sub laserCb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles laserCb.SelectedIndexChanged
        If db Is Nothing Then

        Else
            change()
        End If
    End Sub
    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rockCb.SelectedIndexChanged
        If rockCb.SelectedIndex = 1 Then
            txtRock.Text = 0.15
        ElseIf rockCb.SelectedIndex = 2 Then
            txtRock.Text = 0.3
        ElseIf rockCb.SelectedIndex = 3 Then
            txtRock.Text = 0.6
        Else
            txtRock.Text = 0
        End If
    End Sub
    Private Sub ComboBox3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles droneCb.SelectedIndexChanged
        If droneCb.SelectedIndex = 0 Then
            cbLevel6.Enabled = True
            cbHavocs.Enabled = True
        Else
            cbLevel6.Enabled = False
            cbHavocs.Enabled = False
        End If
    End Sub
    Private Sub cbShipBonus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbShipBonus.SelectedIndexChanged
        If cbShipBonus.SelectedIndex = 0 Then
            txtShipBonus.Text = 0.05
        Else
            txtShipBonus.Text = 0
        End If
    End Sub
    Private Sub boosterCb_SelectedIndexChanged(sender As Object, e As EventArgs) Handles boosterCb.SelectedIndexChanged
        If boosterCb.SelectedIndex = 0 Then
            txtBoosters.Text = 0
        ElseIf boosterCb.SelectedIndex = 1 Then
            txtBoosters.Text = 0.1
        Else
            txtBoosters.Text = 0.2
        End If
    End Sub
    Private Sub cbLaser_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbLaser.SelectedIndexChanged
        If cbLaser.SelectedIndex = 0 Then
            txtLaserUp.Text = 0.06
        Else
            txtLaserUp.Text = 0
        End If
    End Sub
    Private Sub cbDesign_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbDesign.SelectedIndexChanged
        If cbDesign.SelectedIndex = 0 Then
            txtDrone.Text = Nothing
        Else
            'Creates a DataTable
            Dim ds As New DataSet
            Dim dt As New DataTable
            ds.Tables.Add(dt)

            'Gets the name from the selectedItem
            Dim name As String
            name = cbDesign.SelectedItem.ToString()
            Console.WriteLine(name)
            'CMD String to be used for query
            Dim cmd As String = "Select Lasers from formations where Formation ='" & name & "'"

            'Query
            Dim da As New OleDbDataAdapter
            da = db.getOleDbAdapter(cmd)
            'da = New OleDbDataAdapter("SELECT * FROM npcs where name = '" & name & "'", con)

            'Fills data
            da.Fill(dt)

            ' ID | Laser | Max
            'Get the txtLaser Data.
            txtDrone.Text = dt.Rows(0).Item(0)
        End If
    End Sub
    Private Sub cbAmmo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbAmmo.SelectedIndexChanged
        Dim index As Integer

        index = cbAmmo.SelectedIndex

        If index = 0 Then
            ammoType = 1
        ElseIf index = 1 Then
            ammoType = 2
        ElseIf index = 2 Then
            ammoType = 3
        ElseIf index = 3 Then
            ammoType = 4
        ElseIf index = 4 Then
            ammoType = 6
        End If
    End Sub

    'Method to be used for the Combo Box being selected.
    Public Sub change()
        Dim fileLocation As String
        Dim picFile As String
        Dim pictureFile As String

        'Establishes the File Location for the images
        fileLocation = Application.StartupPath & "\images\LASERS\"

        'Creates a DataTable
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds.Tables.Add(dt)

        'Gets the name from the selectedItem
        Dim name As String
        name = laserCb.SelectedItem.ToString()

        'CMD String to be used for query
        Dim cmd As String = "SELECT * FROM Laser where Laser = '" & name & "'"

        'Query
        Dim da As New OleDbDataAdapter
        da = db.getOleDbAdapter(cmd)
        'da = New OleDbDataAdapter("SELECT * FROM npcs where name = '" & name & "'", con)

        'Fills data
        da.Fill(dt)

        ' ID | Laser | Max
        'Get the txtLaser Data.
        txtLaser.Text = dt.Rows(0).Item(2)
        picFile = dt.Rows(0).Item(3)

        'Gets the picture file
        pictureFile = fileLocation & picFile

        'Plugs in the Picture File
        Panel1.BackgroundImage = System.Drawing.Image.FromFile(pictureFile)
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.SelectedIndex = 0 Then
            TextBox1.Text = Nothing
        ElseIf ComboBox1.SelectedIndex = 1 Then
            TextBox1.Text = 0.04
        ElseIf ComboBox1.SelectedIndex = 2 Then
            TextBox1.Text = 0.12
        ElseIf ComboBox1.SelectedIndex = 3 Then
            TextBox1.Text = 0.12
        End If
    End Sub
End Class