﻿Imports System.Data.OleDb
Public Class Database
    'Access
    Dim con As New OleDbConnection

    Public Sub print()
        Console.WriteLine("Loaded")
    End Sub

    Public Sub close()
        con.Close()
    End Sub

    Public Function getOleDbAdapter(ByVal info As String) As OleDbDataAdapter
        Return New OleDbDataAdapter(info, con)
    End Function

    Public Sub loadDatabase()
        Dim databaseLoc As String
        Dim fileName As String
        Dim conString As String

        'Sets name of Database
        fileName = "DOHelperDB.mdb"

        'Sets Database Location and image folder
        databaseLoc = Application.StartupPath & "\database\" & fileName

        'Connection String
        conString = "provider=microsoft.jet.oledb.4.0; data source = " & databaseLoc & ";Jet OLEDB:Database Password=DiwKq6ABhDWq;"

        'Creates a connection string
        con.ConnectionString = conString
        con.Open()
    End Sub

    Public Function getConnection() As OleDbConnection
        Return con
    End Function

    'Checks for DBNulls that were present in the Database
    Public Function DbNullChecker(ByRef a As Object) As String
        Dim ohNo As String = "UC"
        Dim final As String = ""

        If Not IsDBNull(a) Then
            final = a
        Else
            final = ohNo
        End If

        Return final
    End Function

End Class