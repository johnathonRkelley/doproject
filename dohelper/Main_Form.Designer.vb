﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main_Form))
        Me.AS_Combo = New System.Windows.Forms.ComboBox()
        Me.Calculate_Button = New System.Windows.Forms.Button()
        Me.YourRank_Text = New System.Windows.Forms.TextBox()
        Me.NextRank_Text = New System.Windows.Forms.TextBox()
        Me.KillType_Label = New System.Windows.Forms.Label()
        Me.Calculation_Label = New System.Windows.Forms.Label()
        Me.YouNeed_Label = New System.Windows.Forms.Label()
        Me.YourRank_Label = New System.Windows.Forms.Label()
        Me.NextRank_Label = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'AS_Combo
        '
        Me.AS_Combo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.AS_Combo.FormattingEnabled = True
        Me.AS_Combo.Items.AddRange(New Object() {"Streuner", "Boss Streuner", "Uber Streuner", "Lordakia", "Boss Lordakia", "Uber Lordakia", "Mordon", "Boss Mordon", "Uber Mordon", "Saimon", "Boss Saimon", "Uber Saimon", "Devolarium", "Boss Devolarium", "Uber Devolarium", "Sibelon", "Boss Sibelon", "Uber Sibelon", "Sibelonit", "Boss Sibelonit", "Uber Sibelonit", "Lordakium", "Boss Lordakium", "Uber Lordakium", "Kristallin", "Boss Kristallin", "Uber Kristallin", "Kristallon", "Boss Kristallon", "Uber Kristallon", "Streune[R]", "Boss Streune[R]", "Uber Streune[R]", "Protegit", "Cubikon", "Kucurbium", "Phoenix", "Yamato", "Leonov", "Defcom", "Liberator", "Piranha", "Nostromo", "BigBoy", "Vengeance", "Goliath", "Aegis", "Citadel", "Spearhead", "Tartarus"})
        Me.AS_Combo.Location = New System.Drawing.Point(12, 12)
        Me.AS_Combo.Name = "AS_Combo"
        Me.AS_Combo.Size = New System.Drawing.Size(203, 21)
        Me.AS_Combo.TabIndex = 0
        '
        'Calculate_Button
        '
        Me.Calculate_Button.Location = New System.Drawing.Point(12, 91)
        Me.Calculate_Button.Name = "Calculate_Button"
        Me.Calculate_Button.Size = New System.Drawing.Size(203, 21)
        Me.Calculate_Button.TabIndex = 3
        Me.Calculate_Button.Text = "Calculate"
        Me.Calculate_Button.UseVisualStyleBackColor = True
        '
        'YourRank_Text
        '
        Me.YourRank_Text.Location = New System.Drawing.Point(153, 39)
        Me.YourRank_Text.MaxLength = 30
        Me.YourRank_Text.Name = "YourRank_Text"
        Me.YourRank_Text.Size = New System.Drawing.Size(191, 20)
        Me.YourRank_Text.TabIndex = 1
        '
        'NextRank_Text
        '
        Me.NextRank_Text.Location = New System.Drawing.Point(153, 65)
        Me.NextRank_Text.MaxLength = 30
        Me.NextRank_Text.Name = "NextRank_Text"
        Me.NextRank_Text.Size = New System.Drawing.Size(191, 20)
        Me.NextRank_Text.TabIndex = 2
        '
        'KillType_Label
        '
        Me.KillType_Label.AutoSize = True
        Me.KillType_Label.Location = New System.Drawing.Point(366, 72)
        Me.KillType_Label.Name = "KillType_Label"
        Me.KillType_Label.Size = New System.Drawing.Size(31, 13)
        Me.KillType_Label.TabIndex = 4
        Me.KillType_Label.Text = "Type"
        '
        'Calculation_Label
        '
        Me.Calculation_Label.AutoSize = True
        Me.Calculation_Label.Location = New System.Drawing.Point(369, 39)
        Me.Calculation_Label.Name = "Calculation_Label"
        Me.Calculation_Label.Size = New System.Drawing.Size(60, 17)
        Me.Calculation_Label.TabIndex = 5
        Me.Calculation_Label.Text = "Calculation"
        Me.Calculation_Label.UseCompatibleTextRendering = True
        '
        'YouNeed_Label
        '
        Me.YouNeed_Label.AutoSize = True
        Me.YouNeed_Label.Location = New System.Drawing.Point(221, 15)
        Me.YouNeed_Label.Name = "YouNeed_Label"
        Me.YouNeed_Label.Size = New System.Drawing.Size(89, 13)
        Me.YouNeed_Label.TabIndex = 6
        Me.YouNeed_Label.Text = "You need to kill..."
        '
        'YourRank_Label
        '
        Me.YourRank_Label.AutoSize = True
        Me.YourRank_Label.Location = New System.Drawing.Point(11, 41)
        Me.YourRank_Label.Name = "YourRank_Label"
        Me.YourRank_Label.Size = New System.Drawing.Size(136, 13)
        Me.YourRank_Label.TabIndex = 8
        Me.YourRank_Label.Text = "Your Current Rank Points ="
        '
        'NextRank_Label
        '
        Me.NextRank_Label.AutoSize = True
        Me.NextRank_Label.Location = New System.Drawing.Point(48, 68)
        Me.NextRank_Label.Name = "NextRank_Label"
        Me.NextRank_Label.Size = New System.Drawing.Size(99, 13)
        Me.NextRank_Label.TabIndex = 9
        Me.NextRank_Label.Text = "Next Rank Points ="
        '
        'Main_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(442, 119)
        Me.Controls.Add(Me.NextRank_Label)
        Me.Controls.Add(Me.YourRank_Label)
        Me.Controls.Add(Me.YouNeed_Label)
        Me.Controls.Add(Me.Calculation_Label)
        Me.Controls.Add(Me.KillType_Label)
        Me.Controls.Add(Me.NextRank_Text)
        Me.Controls.Add(Me.YourRank_Text)
        Me.Controls.Add(Me.Calculate_Button)
        Me.Controls.Add(Me.AS_Combo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Main_Form"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DOHelper - Rank Calculator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents AS_Combo As System.Windows.Forms.ComboBox
    Friend WithEvents Calculate_Button As System.Windows.Forms.Button
    Friend WithEvents YourRank_Text As System.Windows.Forms.TextBox
    Friend WithEvents NextRank_Text As System.Windows.Forms.TextBox
    Friend WithEvents KillType_Label As System.Windows.Forms.Label
    Friend WithEvents Calculation_Label As System.Windows.Forms.Label
    Friend WithEvents YouNeed_Label As System.Windows.Forms.Label
    Friend WithEvents YourRank_Label As System.Windows.Forms.Label
    Friend WithEvents NextRank_Label As System.Windows.Forms.Label

End Class
