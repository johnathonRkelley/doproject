﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Ships_Form
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Ships_Form))
        Me.ShipCombo = New System.Windows.Forms.ComboBox()
        Me.lblShipName = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.baseHP = New System.Windows.Forms.Label()
        Me.lblSpeed = New System.Windows.Forms.Label()
        Me.lblCargo = New System.Windows.Forms.Label()
        Me.lblLasers = New System.Windows.Forms.Label()
        Me.lblExtras = New System.Windows.Forms.Label()
        Me.lblGenerators = New System.Windows.Forms.Label()
        Me.txtSpecial = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblSpecial = New System.Windows.Forms.Label()
        Me.shipCollection = New System.Windows.Forms.ImageList(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ShipCombo
        '
        Me.ShipCombo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ShipCombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ShipCombo.FormattingEnabled = True
        Me.ShipCombo.Location = New System.Drawing.Point(12, 11)
        Me.ShipCombo.Name = "ShipCombo"
        Me.ShipCombo.Size = New System.Drawing.Size(305, 28)
        Me.ShipCombo.Sorted = True
        Me.ShipCombo.TabIndex = 0
        Me.ShipCombo.Text = "Aegis"
        '
        'lblShipName
        '
        Me.lblShipName.BackColor = System.Drawing.SystemColors.Control
        Me.lblShipName.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblShipName.Location = New System.Drawing.Point(334, 12)
        Me.lblShipName.Name = "lblShipName"
        Me.lblShipName.Size = New System.Drawing.Size(240, 58)
        Me.lblShipName.TabIndex = 3
        Me.lblShipName.Text = "Ship Name"
        Me.lblShipName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(324, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Base HP :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(450, 133)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(77, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Generators :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(324, 133)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 13)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Cargo :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(450, 79)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(52, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Lasers :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(450, 107)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Extras :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(324, 107)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(51, 13)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Speed :"
        '
        'baseHP
        '
        Me.baseHP.AutoSize = True
        Me.baseHP.Location = New System.Drawing.Point(391, 79)
        Me.baseHP.Name = "baseHP"
        Me.baseHP.Size = New System.Drawing.Size(24, 13)
        Me.baseHP.TabIndex = 11
        Me.baseHP.Text = "HP"
        '
        'lblSpeed
        '
        Me.lblSpeed.AutoSize = True
        Me.lblSpeed.Location = New System.Drawing.Point(387, 107)
        Me.lblSpeed.Name = "lblSpeed"
        Me.lblSpeed.Size = New System.Drawing.Size(41, 13)
        Me.lblSpeed.TabIndex = 12
        Me.lblSpeed.Text = "speed"
        '
        'lblCargo
        '
        Me.lblCargo.AutoSize = True
        Me.lblCargo.Location = New System.Drawing.Point(387, 132)
        Me.lblCargo.Name = "lblCargo"
        Me.lblCargo.Size = New System.Drawing.Size(39, 13)
        Me.lblCargo.TabIndex = 13
        Me.lblCargo.Text = "cargo"
        '
        'lblLasers
        '
        Me.lblLasers.AutoSize = True
        Me.lblLasers.Location = New System.Drawing.Point(512, 80)
        Me.lblLasers.Name = "lblLasers"
        Me.lblLasers.Size = New System.Drawing.Size(44, 13)
        Me.lblLasers.TabIndex = 14
        Me.lblLasers.Text = "Lasers"
        '
        'lblExtras
        '
        Me.lblExtras.AutoSize = True
        Me.lblExtras.Location = New System.Drawing.Point(512, 107)
        Me.lblExtras.Name = "lblExtras"
        Me.lblExtras.Size = New System.Drawing.Size(36, 13)
        Me.lblExtras.TabIndex = 15
        Me.lblExtras.Text = "Xtras"
        '
        'lblGenerators
        '
        Me.lblGenerators.AutoSize = True
        Me.lblGenerators.Location = New System.Drawing.Point(537, 133)
        Me.lblGenerators.Name = "lblGenerators"
        Me.lblGenerators.Size = New System.Drawing.Size(36, 13)
        Me.lblGenerators.TabIndex = 16
        Me.lblGenerators.Text = "Gens"
        '
        'txtSpecial
        '
        Me.txtSpecial.Location = New System.Drawing.Point(327, 186)
        Me.txtSpecial.Multiline = True
        Me.txtSpecial.Name = "txtSpecial"
        Me.txtSpecial.ReadOnly = True
        Me.txtSpecial.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSpecial.Size = New System.Drawing.Size(247, 67)
        Me.txtSpecial.TabIndex = 17
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(324, 169)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(94, 13)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Special Ability?"
        '
        'lblSpecial
        '
        Me.lblSpecial.AutoSize = True
        Me.lblSpecial.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSpecial.Location = New System.Drawing.Point(436, 169)
        Me.lblSpecial.Name = "lblSpecial"
        Me.lblSpecial.Size = New System.Drawing.Size(30, 13)
        Me.lblSpecial.TabIndex = 19
        Me.lblSpecial.Text = "spec"
        '
        'shipCollection
        '
        Me.shipCollection.ImageStream = CType(resources.GetObject("shipCollection.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.shipCollection.TransparentColor = System.Drawing.Color.Transparent
        Me.shipCollection.Images.SetKeyName(0, "basicAegis.png")
        Me.shipCollection.Images.SetKeyName(1, "eliteAegis.png")
        Me.shipCollection.Images.SetKeyName(2, "veteranAegis.png")
        Me.shipCollection.Images.SetKeyName(3, "solemnBigboy.png")
        Me.shipCollection.Images.SetKeyName(4, "bigboy.png")
        Me.shipCollection.Images.SetKeyName(5, "citadel.png")
        Me.shipCollection.Images.SetKeyName(6, "eliteCitadel.png")
        Me.shipCollection.Images.SetKeyName(7, "veteranCitadel.png")
        Me.shipCollection.Images.SetKeyName(8, "defcom.png")
        Me.shipCollection.Images.SetKeyName(9, "goliath.png")
        Me.shipCollection.Images.SetKeyName(10, "bastionGoliath.png")
        Me.shipCollection.Images.SetKeyName(11, "diminisherGoliath.png")
        Me.shipCollection.Images.SetKeyName(12, "enforcerGoliath.png")
        Me.shipCollection.Images.SetKeyName(13, "exaltedGoliath.png")
        Me.shipCollection.Images.SetKeyName(14, "sentinelGoliath.png")
        Me.shipCollection.Images.SetKeyName(15, "solaceGoliath.png")
        Me.shipCollection.Images.SetKeyName(16, "spectrumGoliath.png")
        Me.shipCollection.Images.SetKeyName(17, "venomGoliath.png")
        Me.shipCollection.Images.SetKeyName(18, "veteranGoliath.png")
        Me.shipCollection.Images.SetKeyName(19, "leonov.png")
        Me.shipCollection.Images.SetKeyName(20, "leonov.png")
        Me.shipCollection.Images.SetKeyName(21, "liberator.png")
        Me.shipCollection.Images.SetKeyName(22, "nostrome.png")
        Me.shipCollection.Images.SetKeyName(23, "phoenix.png")
        Me.shipCollection.Images.SetKeyName(24, "piranha.png")
        Me.shipCollection.Images.SetKeyName(25, "spearhead.png")
        Me.shipCollection.Images.SetKeyName(26, "eliteSpearhead.png")
        Me.shipCollection.Images.SetKeyName(27, "veteranSpearhead.png")
        Me.shipCollection.Images.SetKeyName(28, "tarturus.jpg")
        Me.shipCollection.Images.SetKeyName(29, "avengerVengance.png")
        Me.shipCollection.Images.SetKeyName(30, "vengance.png")
        Me.shipCollection.Images.SetKeyName(31, "adeptVengance.png")
        Me.shipCollection.Images.SetKeyName(32, "corsairVengance.png")
        Me.shipCollection.Images.SetKeyName(33, "lightningVengance.png")
        Me.shipCollection.Images.SetKeyName(34, "pusatVengance.png")
        Me.shipCollection.Images.SetKeyName(35, "revengeVengance.png")
        Me.shipCollection.Images.SetKeyName(36, "yamato.png")
        Me.shipCollection.Images.SetKeyName(37, "adeptVengance.png")
        Me.shipCollection.Images.SetKeyName(38, "aegis.png")
        Me.shipCollection.Images.SetKeyName(39, "Ambassador Nostromo.png")
        Me.shipCollection.Images.SetKeyName(40, "avengerVengance.png")
        Me.shipCollection.Images.SetKeyName(41, "bastionGoliath.png")
        Me.shipCollection.Images.SetKeyName(42, "bigboy.png")
        Me.shipCollection.Images.SetKeyName(43, "biY2mw8.png")
        Me.shipCollection.Images.SetKeyName(44, "centaurGoliath.png")
        Me.shipCollection.Images.SetKeyName(45, "championGoliath.PNG")
        Me.shipCollection.Images.SetKeyName(46, "citadel.png")
        Me.shipCollection.Images.SetKeyName(47, "corsairVengance.png")
        Me.shipCollection.Images.SetKeyName(48, "Cyborg.png")
        Me.shipCollection.Images.SetKeyName(49, "defcom.png")
        Me.shipCollection.Images.SetKeyName(50, "diminisherGoliath.png")
        Me.shipCollection.Images.SetKeyName(51, "Diplomat Nostromo.png")
        Me.shipCollection.Images.SetKeyName(52, "D-Raven.png")
        Me.shipCollection.Images.SetKeyName(53, "eliteAegis.png")
        Me.shipCollection.Images.SetKeyName(54, "eliteCitadel.png")
        Me.shipCollection.Images.SetKeyName(55, "eliteSpearhead.png")
        Me.shipCollection.Images.SetKeyName(56, "enforcerGoliath.png")
        Me.shipCollection.Images.SetKeyName(57, "Envoy Nostromo.png")
        Me.shipCollection.Images.SetKeyName(58, "exaltedGoliath.png")
        Me.shipCollection.Images.SetKeyName(59, "fNeq7ix.png")
        Me.shipCollection.Images.SetKeyName(60, "G-Champion.png")
        Me.shipCollection.Images.SetKeyName(61, "goalGoliath.png")
        Me.shipCollection.Images.SetKeyName(62, "goliath.png")
        Me.shipCollection.Images.SetKeyName(63, "Goliath_Ignite.png")
        Me.shipCollection.Images.SetKeyName(64, "Hammerclaw.png")
        Me.shipCollection.Images.SetKeyName(65, "kickGoliath.png")
        Me.shipCollection.Images.SetKeyName(66, "leonov.png")
        Me.shipCollection.Images.SetKeyName(67, "liberator.png")
        Me.shipCollection.Images.SetKeyName(68, "lightningVengance.png")
        Me.shipCollection.Images.SetKeyName(69, "Mimesis.png")
        Me.shipCollection.Images.SetKeyName(70, "nostromo.png")
        Me.shipCollection.Images.SetKeyName(71, "P9YSsoG.png")
        Me.shipCollection.Images.SetKeyName(72, "peacemakerGoliath.png")
        Me.shipCollection.Images.SetKeyName(73, "phoenix.png")
        Me.shipCollection.Images.SetKeyName(74, "piranha.png")
        Me.shipCollection.Images.SetKeyName(75, "Pusat.png")
        Me.shipCollection.Images.SetKeyName(76, "pusatVengance.png")
        Me.shipCollection.Images.SetKeyName(77, "refereeGoliath.png")
        Me.shipCollection.Images.SetKeyName(78, "revengeVengance.png")
        Me.shipCollection.Images.SetKeyName(79, "S Elite.png")
        Me.shipCollection.Images.SetKeyName(80, "S Veteran.png")
        Me.shipCollection.Images.SetKeyName(81, "saturnGoliath.png")
        Me.shipCollection.Images.SetKeyName(82, "sentinelGoliath.png")
        Me.shipCollection.Images.SetKeyName(83, "solaceGoliath.png")
        Me.shipCollection.Images.SetKeyName(84, "solemnBigboy.png")
        Me.shipCollection.Images.SetKeyName(85, "sovereignGoliath.png")
        Me.shipCollection.Images.SetKeyName(86, "spearhead.png")
        Me.shipCollection.Images.SetKeyName(87, "spectrumGoliath.png")
        Me.shipCollection.Images.SetKeyName(88, "surgeonGoliath.png")
        Me.shipCollection.Images.SetKeyName(89, "tarturus.jpg")
        Me.shipCollection.Images.SetKeyName(90, "tarturus.png")
        Me.shipCollection.Images.SetKeyName(91, "tarturus_old.jpg")
        Me.shipCollection.Images.SetKeyName(92, "vanquisherGoliath.png")
        Me.shipCollection.Images.SetKeyName(93, "vengance.png")
        Me.shipCollection.Images.SetKeyName(94, "venomGoliath.png")
        Me.shipCollection.Images.SetKeyName(95, "veteranAegis.png")
        Me.shipCollection.Images.SetKeyName(96, "veteranCitadel.png")
        Me.shipCollection.Images.SetKeyName(97, "veteranGoliath.png")
        Me.shipCollection.Images.SetKeyName(98, "veteranSpearhead.png")
        Me.shipCollection.Images.SetKeyName(99, "yamato.png")
        Me.shipCollection.Images.SetKeyName(100, "Y-Ronin.png")
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.Window
        Me.PictureBox1.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.annhilator
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox1.Location = New System.Drawing.Point(12, 44)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(305, 210)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 20
        Me.PictureBox1.TabStop = False
        '
        'Ships_Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(586, 266)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblSpecial)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtSpecial)
        Me.Controls.Add(Me.lblGenerators)
        Me.Controls.Add(Me.lblExtras)
        Me.Controls.Add(Me.lblLasers)
        Me.Controls.Add(Me.lblCargo)
        Me.Controls.Add(Me.lblSpeed)
        Me.Controls.Add(Me.baseHP)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblShipName)
        Me.Controls.Add(Me.ShipCombo)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Ships_Form"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ship Information"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ShipCombo As System.Windows.Forms.ComboBox
    Friend WithEvents lblShipName As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents baseHP As System.Windows.Forms.Label
    Friend WithEvents lblSpeed As System.Windows.Forms.Label
    Friend WithEvents lblCargo As System.Windows.Forms.Label
    Friend WithEvents lblLasers As System.Windows.Forms.Label
    Friend WithEvents lblExtras As System.Windows.Forms.Label
    Friend WithEvents lblGenerators As System.Windows.Forms.Label
    Friend WithEvents txtSpecial As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblSpecial As System.Windows.Forms.Label
    Friend WithEvents shipCollection As System.Windows.Forms.ImageList
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
End Class
