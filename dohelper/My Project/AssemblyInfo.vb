﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("DO Project")> 
<Assembly: AssemblyDescription("This program is created by the Dream Team to be sent to Big Point.")>
<Assembly: AssemblyCompany("The Dream Team")>
<Assembly: AssemblyProduct("DO Project Helper")>
<Assembly: AssemblyCopyright("Copyright ©  Dream Team 2017")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("b7b74e12-695e-4083-9304-e411e3b15c1f")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("10.0.0.10")> 
<Assembly: AssemblyFileVersion("10.0.0.10")> 

<Assembly: NeutralResourcesLanguageAttribute("en")> 