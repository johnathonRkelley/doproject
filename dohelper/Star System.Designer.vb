﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Star_System
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Star_System))
        Me.lblSwitch = New System.Windows.Forms.Label()
        Me.lblCurrentSystem = New System.Windows.Forms.Label()
        Me.btn11 = New System.Windows.Forms.Button()
        Me.btn12 = New System.Windows.Forms.Button()
        Me.btn13 = New System.Windows.Forms.Button()
        Me.btn23 = New System.Windows.Forms.Button()
        Me.btn22 = New System.Windows.Forms.Button()
        Me.btn21 = New System.Windows.Forms.Button()
        Me.btn14 = New System.Windows.Forms.Button()
        Me.btn41 = New System.Windows.Forms.Button()
        Me.btn42 = New System.Windows.Forms.Button()
        Me.btn24 = New System.Windows.Forms.Button()
        Me.btn34 = New System.Windows.Forms.Button()
        Me.btn43 = New System.Windows.Forms.Button()
        Me.btn33 = New System.Windows.Forms.Button()
        Me.btn32 = New System.Windows.Forms.Button()
        Me.btn31 = New System.Windows.Forms.Button()
        Me.btn18 = New System.Windows.Forms.Button()
        Me.btn16 = New System.Windows.Forms.Button()
        Me.btn17 = New System.Windows.Forms.Button()
        Me.btn53 = New System.Windows.Forms.Button()
        Me.btn52 = New System.Windows.Forms.Button()
        Me.btn15 = New System.Windows.Forms.Button()
        Me.btn51 = New System.Windows.Forms.Button()
        Me.btn26 = New System.Windows.Forms.Button()
        Me.btn28 = New System.Windows.Forms.Button()
        Me.btn27 = New System.Windows.Forms.Button()
        Me.btn25 = New System.Windows.Forms.Button()
        Me.btn37 = New System.Windows.Forms.Button()
        Me.btn35 = New System.Windows.Forms.Button()
        Me.btn36 = New System.Windows.Forms.Button()
        Me.btn38 = New System.Windows.Forms.Button()
        Me.btn45 = New System.Windows.Forms.Button()
        Me.btn44 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblSwitch
        '
        Me.lblSwitch.AutoSize = True
        Me.lblSwitch.BackColor = System.Drawing.Color.Transparent
        Me.lblSwitch.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSwitch.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblSwitch.Location = New System.Drawing.Point(13, 30)
        Me.lblSwitch.Name = "lblSwitch"
        Me.lblSwitch.Size = New System.Drawing.Size(110, 20)
        Me.lblSwitch.TabIndex = 0
        Me.lblSwitch.Text = "Switch Maps"
        '
        'lblCurrentSystem
        '
        Me.lblCurrentSystem.AutoSize = True
        Me.lblCurrentSystem.BackColor = System.Drawing.Color.Transparent
        Me.lblCurrentSystem.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrentSystem.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblCurrentSystem.Location = New System.Drawing.Point(224, 30)
        Me.lblCurrentSystem.Name = "lblCurrentSystem"
        Me.lblCurrentSystem.Size = New System.Drawing.Size(143, 20)
        Me.lblCurrentSystem.TabIndex = 1
        Me.lblCurrentSystem.Text = "Current System :"
        '
        'btn11
        '
        Me.btn11.BackColor = System.Drawing.Color.Transparent
        Me.btn11.FlatAppearance.BorderSize = 0
        Me.btn11.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn11.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn11.ForeColor = System.Drawing.Color.DimGray
        Me.btn11.Location = New System.Drawing.Point(56, 213)
        Me.btn11.Margin = New System.Windows.Forms.Padding(2)
        Me.btn11.Name = "btn11"
        Me.btn11.Size = New System.Drawing.Size(73, 50)
        Me.btn11.TabIndex = 2
        Me.btn11.Text = "1-1"
        Me.btn11.UseVisualStyleBackColor = False
        '
        'btn12
        '
        Me.btn12.BackColor = System.Drawing.Color.Transparent
        Me.btn12.FlatAppearance.BorderSize = 0
        Me.btn12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn12.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn12.ForeColor = System.Drawing.Color.DimGray
        Me.btn12.Location = New System.Drawing.Point(153, 211)
        Me.btn12.Margin = New System.Windows.Forms.Padding(2)
        Me.btn12.Name = "btn12"
        Me.btn12.Size = New System.Drawing.Size(76, 51)
        Me.btn12.TabIndex = 3
        Me.btn12.Text = "1-2"
        Me.btn12.UseVisualStyleBackColor = False
        '
        'btn13
        '
        Me.btn13.BackColor = System.Drawing.Color.Transparent
        Me.btn13.FlatAppearance.BorderSize = 0
        Me.btn13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn13.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn13.ForeColor = System.Drawing.Color.DimGray
        Me.btn13.Location = New System.Drawing.Point(247, 167)
        Me.btn13.Margin = New System.Windows.Forms.Padding(2)
        Me.btn13.Name = "btn13"
        Me.btn13.Size = New System.Drawing.Size(72, 50)
        Me.btn13.TabIndex = 4
        Me.btn13.Text = "1-3"
        Me.btn13.UseVisualStyleBackColor = False
        '
        'btn23
        '
        Me.btn23.BackColor = System.Drawing.Color.Transparent
        Me.btn23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn23.FlatAppearance.BorderSize = 0
        Me.btn23.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn23.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn23.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn23.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn23.ForeColor = System.Drawing.Color.DimGray
        Me.btn23.Location = New System.Drawing.Point(338, 117)
        Me.btn23.Margin = New System.Windows.Forms.Padding(2)
        Me.btn23.Name = "btn23"
        Me.btn23.Size = New System.Drawing.Size(73, 49)
        Me.btn23.TabIndex = 5
        Me.btn23.Text = "2-3"
        Me.btn23.UseVisualStyleBackColor = False
        '
        'btn22
        '
        Me.btn22.BackColor = System.Drawing.Color.Transparent
        Me.btn22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn22.FlatAppearance.BorderSize = 0
        Me.btn22.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn22.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn22.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn22.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn22.ForeColor = System.Drawing.Color.DimGray
        Me.btn22.Location = New System.Drawing.Point(436, 72)
        Me.btn22.Margin = New System.Windows.Forms.Padding(2)
        Me.btn22.Name = "btn22"
        Me.btn22.Size = New System.Drawing.Size(73, 46)
        Me.btn22.TabIndex = 6
        Me.btn22.Text = "2-2"
        Me.btn22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn22.UseVisualStyleBackColor = False
        '
        'btn21
        '
        Me.btn21.BackColor = System.Drawing.Color.Transparent
        Me.btn21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn21.FlatAppearance.BorderSize = 0
        Me.btn21.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn21.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn21.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn21.ForeColor = System.Drawing.Color.DimGray
        Me.btn21.Location = New System.Drawing.Point(526, 34)
        Me.btn21.Margin = New System.Windows.Forms.Padding(2)
        Me.btn21.Name = "btn21"
        Me.btn21.Size = New System.Drawing.Size(76, 48)
        Me.btn21.TabIndex = 7
        Me.btn21.Text = "2-1"
        Me.btn21.UseVisualStyleBackColor = False
        '
        'btn14
        '
        Me.btn14.BackColor = System.Drawing.Color.Transparent
        Me.btn14.FlatAppearance.BorderSize = 0
        Me.btn14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn14.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn14.ForeColor = System.Drawing.Color.DimGray
        Me.btn14.Location = New System.Drawing.Point(248, 262)
        Me.btn14.Margin = New System.Windows.Forms.Padding(2)
        Me.btn14.Name = "btn14"
        Me.btn14.Size = New System.Drawing.Size(70, 50)
        Me.btn14.TabIndex = 8
        Me.btn14.Text = "1-4"
        Me.btn14.UseVisualStyleBackColor = False
        '
        'btn41
        '
        Me.btn41.BackColor = System.Drawing.Color.Transparent
        Me.btn41.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn41.FlatAppearance.BorderSize = 0
        Me.btn41.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn41.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn41.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn41.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn41.ForeColor = System.Drawing.Color.DimGray
        Me.btn41.Location = New System.Drawing.Point(336, 211)
        Me.btn41.Margin = New System.Windows.Forms.Padding(2)
        Me.btn41.Name = "btn41"
        Me.btn41.Size = New System.Drawing.Size(75, 50)
        Me.btn41.TabIndex = 9
        Me.btn41.Text = "4-1"
        Me.btn41.UseVisualStyleBackColor = False
        '
        'btn42
        '
        Me.btn42.BackColor = System.Drawing.Color.Transparent
        Me.btn42.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn42.FlatAppearance.BorderSize = 0
        Me.btn42.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn42.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn42.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn42.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn42.ForeColor = System.Drawing.Color.DimGray
        Me.btn42.Location = New System.Drawing.Point(424, 180)
        Me.btn42.Margin = New System.Windows.Forms.Padding(2)
        Me.btn42.Name = "btn42"
        Me.btn42.Size = New System.Drawing.Size(75, 50)
        Me.btn42.TabIndex = 10
        Me.btn42.Text = "4-2"
        Me.btn42.UseVisualStyleBackColor = False
        '
        'btn24
        '
        Me.btn24.BackColor = System.Drawing.Color.Transparent
        Me.btn24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn24.FlatAppearance.BorderSize = 0
        Me.btn24.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn24.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn24.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn24.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn24.ForeColor = System.Drawing.Color.DimGray
        Me.btn24.Location = New System.Drawing.Point(527, 127)
        Me.btn24.Margin = New System.Windows.Forms.Padding(2)
        Me.btn24.Name = "btn24"
        Me.btn24.Size = New System.Drawing.Size(75, 50)
        Me.btn24.TabIndex = 11
        Me.btn24.Text = "2-4"
        Me.btn24.UseVisualStyleBackColor = False
        '
        'btn34
        '
        Me.btn34.BackColor = System.Drawing.Color.Transparent
        Me.btn34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn34.FlatAppearance.BorderSize = 0
        Me.btn34.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn34.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn34.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn34.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn34.ForeColor = System.Drawing.Color.DimGray
        Me.btn34.Location = New System.Drawing.Point(336, 308)
        Me.btn34.Margin = New System.Windows.Forms.Padding(2)
        Me.btn34.Name = "btn34"
        Me.btn34.Size = New System.Drawing.Size(75, 50)
        Me.btn34.TabIndex = 12
        Me.btn34.Text = "3-4"
        Me.btn34.UseVisualStyleBackColor = False
        '
        'btn43
        '
        Me.btn43.BackColor = System.Drawing.Color.Transparent
        Me.btn43.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn43.FlatAppearance.BorderSize = 0
        Me.btn43.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn43.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn43.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn43.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn43.ForeColor = System.Drawing.Color.DimGray
        Me.btn43.Location = New System.Drawing.Point(428, 247)
        Me.btn43.Margin = New System.Windows.Forms.Padding(2)
        Me.btn43.Name = "btn43"
        Me.btn43.Size = New System.Drawing.Size(75, 50)
        Me.btn43.TabIndex = 13
        Me.btn43.Text = "4-3"
        Me.btn43.UseVisualStyleBackColor = False
        '
        'btn33
        '
        Me.btn33.BackColor = System.Drawing.Color.Transparent
        Me.btn33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn33.FlatAppearance.BorderSize = 0
        Me.btn33.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn33.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn33.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn33.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn33.ForeColor = System.Drawing.Color.DimGray
        Me.btn33.Location = New System.Drawing.Point(527, 221)
        Me.btn33.Margin = New System.Windows.Forms.Padding(2)
        Me.btn33.Name = "btn33"
        Me.btn33.Size = New System.Drawing.Size(75, 50)
        Me.btn33.TabIndex = 14
        Me.btn33.Text = "3-3"
        Me.btn33.UseVisualStyleBackColor = False
        '
        'btn32
        '
        Me.btn32.BackColor = System.Drawing.Color.Transparent
        Me.btn32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn32.FlatAppearance.BorderSize = 0
        Me.btn32.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn32.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn32.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn32.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn32.ForeColor = System.Drawing.Color.DimGray
        Me.btn32.Location = New System.Drawing.Point(434, 350)
        Me.btn32.Margin = New System.Windows.Forms.Padding(2)
        Me.btn32.Name = "btn32"
        Me.btn32.Size = New System.Drawing.Size(75, 50)
        Me.btn32.TabIndex = 15
        Me.btn32.Text = "3-2"
        Me.btn32.UseVisualStyleBackColor = False
        '
        'btn31
        '
        Me.btn31.BackColor = System.Drawing.Color.Transparent
        Me.btn31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn31.FlatAppearance.BorderSize = 0
        Me.btn31.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn31.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn31.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn31.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn31.ForeColor = System.Drawing.Color.DimGray
        Me.btn31.Location = New System.Drawing.Point(526, 400)
        Me.btn31.Margin = New System.Windows.Forms.Padding(2)
        Me.btn31.Name = "btn31"
        Me.btn31.Size = New System.Drawing.Size(75, 50)
        Me.btn31.TabIndex = 16
        Me.btn31.Text = "3-1"
        Me.btn31.UseVisualStyleBackColor = False
        '
        'btn18
        '
        Me.btn18.BackColor = System.Drawing.Color.Transparent
        Me.btn18.FlatAppearance.BorderSize = 0
        Me.btn18.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn18.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn18.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn18.ForeColor = System.Drawing.Color.DimGray
        Me.btn18.Location = New System.Drawing.Point(10, 200)
        Me.btn18.Margin = New System.Windows.Forms.Padding(2)
        Me.btn18.Name = "btn18"
        Me.btn18.Size = New System.Drawing.Size(75, 50)
        Me.btn18.TabIndex = 17
        Me.btn18.Text = "1-8"
        Me.btn18.UseVisualStyleBackColor = False
        '
        'btn16
        '
        Me.btn16.BackColor = System.Drawing.Color.Transparent
        Me.btn16.FlatAppearance.BorderSize = 0
        Me.btn16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn16.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn16.ForeColor = System.Drawing.Color.DimGray
        Me.btn16.Location = New System.Drawing.Point(88, 139)
        Me.btn16.Margin = New System.Windows.Forms.Padding(2)
        Me.btn16.Name = "btn16"
        Me.btn16.Size = New System.Drawing.Size(75, 50)
        Me.btn16.TabIndex = 18
        Me.btn16.Text = "1-6"
        Me.btn16.UseVisualStyleBackColor = False
        '
        'btn17
        '
        Me.btn17.BackColor = System.Drawing.Color.Transparent
        Me.btn17.FlatAppearance.BorderSize = 0
        Me.btn17.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn17.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn17.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn17.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn17.ForeColor = System.Drawing.Color.DimGray
        Me.btn17.Location = New System.Drawing.Point(88, 267)
        Me.btn17.Margin = New System.Windows.Forms.Padding(2)
        Me.btn17.Name = "btn17"
        Me.btn17.Size = New System.Drawing.Size(75, 50)
        Me.btn17.TabIndex = 19
        Me.btn17.Text = "1-7"
        Me.btn17.UseVisualStyleBackColor = False
        '
        'btn53
        '
        Me.btn53.BackColor = System.Drawing.Color.Transparent
        Me.btn53.FlatAppearance.BorderSize = 0
        Me.btn53.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn53.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn53.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn53.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn53.ForeColor = System.Drawing.Color.DimGray
        Me.btn53.Location = New System.Drawing.Point(200, 93)
        Me.btn53.Margin = New System.Windows.Forms.Padding(2)
        Me.btn53.Name = "btn53"
        Me.btn53.Size = New System.Drawing.Size(75, 50)
        Me.btn53.TabIndex = 20
        Me.btn53.Text = "5-3"
        Me.btn53.UseVisualStyleBackColor = False
        '
        'btn52
        '
        Me.btn52.BackColor = System.Drawing.Color.Transparent
        Me.btn52.FlatAppearance.BorderSize = 0
        Me.btn52.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn52.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn52.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn52.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn52.ForeColor = System.Drawing.Color.DimGray
        Me.btn52.Location = New System.Drawing.Point(290, 53)
        Me.btn52.Margin = New System.Windows.Forms.Padding(2)
        Me.btn52.Name = "btn52"
        Me.btn52.Size = New System.Drawing.Size(75, 50)
        Me.btn52.TabIndex = 21
        Me.btn52.Text = "5-2"
        Me.btn52.UseVisualStyleBackColor = False
        '
        'btn15
        '
        Me.btn15.BackColor = System.Drawing.Color.Transparent
        Me.btn15.FlatAppearance.BorderSize = 0
        Me.btn15.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn15.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn15.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn15.ForeColor = System.Drawing.Color.DimGray
        Me.btn15.Location = New System.Drawing.Point(164, 199)
        Me.btn15.Margin = New System.Windows.Forms.Padding(2)
        Me.btn15.Name = "btn15"
        Me.btn15.Size = New System.Drawing.Size(75, 50)
        Me.btn15.TabIndex = 22
        Me.btn15.Text = "1-5"
        Me.btn15.UseVisualStyleBackColor = False
        '
        'btn51
        '
        Me.btn51.BackColor = System.Drawing.Color.Transparent
        Me.btn51.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn51.FlatAppearance.BorderSize = 0
        Me.btn51.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn51.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn51.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn51.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn51.ForeColor = System.Drawing.Color.DimGray
        Me.btn51.Location = New System.Drawing.Point(374, 93)
        Me.btn51.Margin = New System.Windows.Forms.Padding(2)
        Me.btn51.Name = "btn51"
        Me.btn51.Size = New System.Drawing.Size(75, 50)
        Me.btn51.TabIndex = 23
        Me.btn51.Text = "5-1"
        Me.btn51.UseVisualStyleBackColor = False
        '
        'btn26
        '
        Me.btn26.BackColor = System.Drawing.Color.Transparent
        Me.btn26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn26.FlatAppearance.BorderSize = 0
        Me.btn26.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn26.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn26.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn26.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn26.ForeColor = System.Drawing.Color.DimGray
        Me.btn26.Location = New System.Drawing.Point(483, 72)
        Me.btn26.Margin = New System.Windows.Forms.Padding(2)
        Me.btn26.Name = "btn26"
        Me.btn26.Size = New System.Drawing.Size(75, 50)
        Me.btn26.TabIndex = 24
        Me.btn26.Text = "2-6"
        Me.btn26.UseVisualStyleBackColor = False
        '
        'btn28
        '
        Me.btn28.BackColor = System.Drawing.Color.Transparent
        Me.btn28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn28.FlatAppearance.BorderSize = 0
        Me.btn28.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn28.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn28.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn28.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn28.ForeColor = System.Drawing.Color.DimGray
        Me.btn28.Location = New System.Drawing.Point(590, 32)
        Me.btn28.Margin = New System.Windows.Forms.Padding(2)
        Me.btn28.Name = "btn28"
        Me.btn28.Size = New System.Drawing.Size(75, 50)
        Me.btn28.TabIndex = 25
        Me.btn28.Text = "2-8"
        Me.btn28.UseVisualStyleBackColor = False
        '
        'btn27
        '
        Me.btn27.BackColor = System.Drawing.Color.Transparent
        Me.btn27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn27.FlatAppearance.BorderSize = 0
        Me.btn27.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn27.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn27.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn27.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn27.ForeColor = System.Drawing.Color.DimGray
        Me.btn27.Location = New System.Drawing.Point(590, 127)
        Me.btn27.Margin = New System.Windows.Forms.Padding(2)
        Me.btn27.Name = "btn27"
        Me.btn27.Size = New System.Drawing.Size(75, 50)
        Me.btn27.TabIndex = 26
        Me.btn27.Text = "2-7"
        Me.btn27.UseVisualStyleBackColor = False
        '
        'btn25
        '
        Me.btn25.BackColor = System.Drawing.Color.Transparent
        Me.btn25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn25.FlatAppearance.BorderSize = 0
        Me.btn25.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn25.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn25.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn25.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn25.ForeColor = System.Drawing.Color.DimGray
        Me.btn25.Location = New System.Drawing.Point(483, 158)
        Me.btn25.Margin = New System.Windows.Forms.Padding(2)
        Me.btn25.Name = "btn25"
        Me.btn25.Size = New System.Drawing.Size(75, 50)
        Me.btn25.TabIndex = 27
        Me.btn25.Text = "2-5"
        Me.btn25.UseVisualStyleBackColor = False
        '
        'btn37
        '
        Me.btn37.BackColor = System.Drawing.Color.Transparent
        Me.btn37.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn37.FlatAppearance.BorderSize = 0
        Me.btn37.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn37.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn37.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn37.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn37.ForeColor = System.Drawing.Color.DimGray
        Me.btn37.Location = New System.Drawing.Point(566, 295)
        Me.btn37.Margin = New System.Windows.Forms.Padding(2)
        Me.btn37.Name = "btn37"
        Me.btn37.Size = New System.Drawing.Size(75, 50)
        Me.btn37.TabIndex = 28
        Me.btn37.Text = "3-7"
        Me.btn37.UseVisualStyleBackColor = False
        '
        'btn35
        '
        Me.btn35.BackColor = System.Drawing.Color.Transparent
        Me.btn35.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn35.FlatAppearance.BorderSize = 0
        Me.btn35.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn35.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn35.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn35.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn35.ForeColor = System.Drawing.Color.DimGray
        Me.btn35.Location = New System.Drawing.Point(467, 295)
        Me.btn35.Margin = New System.Windows.Forms.Padding(2)
        Me.btn35.Name = "btn35"
        Me.btn35.Size = New System.Drawing.Size(75, 50)
        Me.btn35.TabIndex = 29
        Me.btn35.Text = "3-5"
        Me.btn35.UseVisualStyleBackColor = False
        '
        'btn36
        '
        Me.btn36.BackColor = System.Drawing.Color.Transparent
        Me.btn36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn36.FlatAppearance.BorderSize = 0
        Me.btn36.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn36.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn36.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn36.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn36.ForeColor = System.Drawing.Color.DimGray
        Me.btn36.Location = New System.Drawing.Point(496, 389)
        Me.btn36.Margin = New System.Windows.Forms.Padding(2)
        Me.btn36.Name = "btn36"
        Me.btn36.Size = New System.Drawing.Size(75, 50)
        Me.btn36.TabIndex = 30
        Me.btn36.Text = "3-6"
        Me.btn36.UseVisualStyleBackColor = False
        '
        'btn38
        '
        Me.btn38.BackColor = System.Drawing.Color.Transparent
        Me.btn38.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn38.FlatAppearance.BorderSize = 0
        Me.btn38.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn38.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn38.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn38.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn38.ForeColor = System.Drawing.Color.DimGray
        Me.btn38.Location = New System.Drawing.Point(598, 388)
        Me.btn38.Margin = New System.Windows.Forms.Padding(2)
        Me.btn38.Name = "btn38"
        Me.btn38.Size = New System.Drawing.Size(75, 50)
        Me.btn38.TabIndex = 31
        Me.btn38.Text = "3-8"
        Me.btn38.UseVisualStyleBackColor = False
        '
        'btn45
        '
        Me.btn45.BackColor = System.Drawing.Color.Transparent
        Me.btn45.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn45.FlatAppearance.BorderSize = 0
        Me.btn45.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn45.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn45.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn45.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn45.ForeColor = System.Drawing.Color.DimGray
        Me.btn45.Location = New System.Drawing.Point(226, 324)
        Me.btn45.Margin = New System.Windows.Forms.Padding(2)
        Me.btn45.Name = "btn45"
        Me.btn45.Size = New System.Drawing.Size(184, 115)
        Me.btn45.TabIndex = 32
        Me.btn45.Text = "4-5"
        Me.btn45.UseVisualStyleBackColor = False
        '
        'btn44
        '
        Me.btn44.BackColor = System.Drawing.Color.Transparent
        Me.btn44.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btn44.FlatAppearance.BorderSize = 0
        Me.btn44.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent
        Me.btn44.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btn44.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn44.Font = New System.Drawing.Font("Microsoft Sans Serif", 0.000001!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.btn44.ForeColor = System.Drawing.Color.DimGray
        Me.btn44.Location = New System.Drawing.Point(264, 168)
        Me.btn44.Margin = New System.Windows.Forms.Padding(2)
        Me.btn44.Name = "btn44"
        Me.btn44.Size = New System.Drawing.Size(184, 115)
        Me.btn44.TabIndex = 33
        Me.btn44.Text = "4-4"
        Me.btn44.UseVisualStyleBackColor = False
        '
        'Star_System
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.WindowsApplication1.My.Resources.Resources.lowers
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(684, 461)
        Me.Controls.Add(Me.btn31)
        Me.Controls.Add(Me.btn32)
        Me.Controls.Add(Me.btn33)
        Me.Controls.Add(Me.btn43)
        Me.Controls.Add(Me.btn34)
        Me.Controls.Add(Me.btn24)
        Me.Controls.Add(Me.btn42)
        Me.Controls.Add(Me.btn41)
        Me.Controls.Add(Me.btn14)
        Me.Controls.Add(Me.btn21)
        Me.Controls.Add(Me.btn22)
        Me.Controls.Add(Me.btn23)
        Me.Controls.Add(Me.btn13)
        Me.Controls.Add(Me.btn12)
        Me.Controls.Add(Me.btn11)
        Me.Controls.Add(Me.lblCurrentSystem)
        Me.Controls.Add(Me.lblSwitch)
        Me.Controls.Add(Me.btn44)
        Me.Controls.Add(Me.btn45)
        Me.Controls.Add(Me.btn38)
        Me.Controls.Add(Me.btn36)
        Me.Controls.Add(Me.btn35)
        Me.Controls.Add(Me.btn37)
        Me.Controls.Add(Me.btn25)
        Me.Controls.Add(Me.btn27)
        Me.Controls.Add(Me.btn28)
        Me.Controls.Add(Me.btn26)
        Me.Controls.Add(Me.btn51)
        Me.Controls.Add(Me.btn15)
        Me.Controls.Add(Me.btn52)
        Me.Controls.Add(Me.btn53)
        Me.Controls.Add(Me.btn17)
        Me.Controls.Add(Me.btn16)
        Me.Controls.Add(Me.btn18)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Star_System"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Star System Map"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblSwitch As System.Windows.Forms.Label
    Friend WithEvents lblCurrentSystem As System.Windows.Forms.Label
    Friend WithEvents btn11 As System.Windows.Forms.Button
    Friend WithEvents btn12 As System.Windows.Forms.Button
    Friend WithEvents btn13 As System.Windows.Forms.Button
    Friend WithEvents btn23 As System.Windows.Forms.Button
    Friend WithEvents btn22 As System.Windows.Forms.Button
    Friend WithEvents btn21 As System.Windows.Forms.Button
    Friend WithEvents btn14 As System.Windows.Forms.Button
    Friend WithEvents btn41 As System.Windows.Forms.Button
    Friend WithEvents btn42 As System.Windows.Forms.Button
    Friend WithEvents btn24 As System.Windows.Forms.Button
    Friend WithEvents btn34 As System.Windows.Forms.Button
    Friend WithEvents btn43 As System.Windows.Forms.Button
    Friend WithEvents btn33 As System.Windows.Forms.Button
    Friend WithEvents btn32 As System.Windows.Forms.Button
    Friend WithEvents btn31 As System.Windows.Forms.Button
    Friend WithEvents btn18 As System.Windows.Forms.Button
    Friend WithEvents btn16 As System.Windows.Forms.Button
    Friend WithEvents btn17 As System.Windows.Forms.Button
    Friend WithEvents btn53 As System.Windows.Forms.Button
    Friend WithEvents btn52 As System.Windows.Forms.Button
    Friend WithEvents btn15 As System.Windows.Forms.Button
    Friend WithEvents btn51 As System.Windows.Forms.Button
    Friend WithEvents btn26 As System.Windows.Forms.Button
    Friend WithEvents btn28 As System.Windows.Forms.Button
    Friend WithEvents btn27 As System.Windows.Forms.Button
    Friend WithEvents btn25 As System.Windows.Forms.Button
    Friend WithEvents btn37 As System.Windows.Forms.Button
    Friend WithEvents btn35 As System.Windows.Forms.Button
    Friend WithEvents btn36 As System.Windows.Forms.Button
    Friend WithEvents btn38 As System.Windows.Forms.Button
    Friend WithEvents btn45 As System.Windows.Forms.Button
    Friend WithEvents btn44 As System.Windows.Forms.Button
End Class
