﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EfficientKilling
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EfficientKilling))
        Me.tim_second = New System.Windows.Forms.Timer(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Alien_Combo = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblUri = New System.Windows.Forms.Label()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'tim_second
        '
        Me.tim_second.Interval = 1000
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(161, 136)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(87, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Stop"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(14, 136)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(87, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Start"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Alien_Combo
        '
        Me.Alien_Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Alien_Combo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Alien_Combo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Alien_Combo.FormattingEnabled = True
        Me.Alien_Combo.IntegralHeight = False
        Me.Alien_Combo.Items.AddRange(New Object() {"Annihilator", "Barracuda", "Battleray", "Binary Bot", "Boss Devolarium", "Boss Kristallin", "Boss Kristallon", "Boss Lordakia", "Boss Lordakium", "Boss Mordon", "Boss Saimon", "Boss Sibelon", "Boss Sibelonit", "Boss Streune[R]", "Boss Streuner", "Carnival Bot", "Century Falcon", "Convict", "Corsair", "Cubikon", "Devolarium", "Devourer", "Emperor Kristallon", "Emperor Lordakium", "Emperor Sibelon", "Emperor Streuner", "Hooligan", "Ice Meteoroid", "Icy", "Infernal", "Interceptor", "Kristallin", "Kristallon", "Kucurbium", "Lordakia", "Lordakium", "Marauder", "Melter", "Mordon", "Outcast", "Protegit", "Ravager", "Referee Bot", "Saboteur", "Saimon", "Santa Bot", "Scorcher", "Sibelon", "Sibelonit", "Streune[R]", "Streuner", "Super Ice Meteoroid", "Uber Devolarium", "Uber Kristallin", "Uber Kristallon", "Uber Lordakia", "Uber Lordakium", "Uber Mordon", "Uber Saimon", "Uber Sibelon", "Uber Sibelonit", "Uber Streune[R]", "Uber Streuner", "Vagrant"})
        Me.Alien_Combo.Location = New System.Drawing.Point(14, 37)
        Me.Alien_Combo.Name = "Alien_Combo"
        Me.Alien_Combo.Size = New System.Drawing.Size(234, 24)
        Me.Alien_Combo.Sorted = True
        Me.Alien_Combo.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(0, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(258, 34)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Select an NPC :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblUri
        '
        Me.lblUri.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUri.Location = New System.Drawing.Point(14, 75)
        Me.lblUri.Name = "lblUri"
        Me.lblUri.Size = New System.Drawing.Size(230, 51)
        Me.lblUri.TabIndex = 7
        Me.lblUri.Text = "Uridium Per Hour : "
        Me.lblUri.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTime.Location = New System.Drawing.Point(120, 141)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(45, 13)
        Me.lblTime.TabIndex = 8
        Me.lblTime.Text = "Label1"
        '
        'EfficientKilling
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(258, 170)
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.lblUri)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Alien_Combo)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "EfficientKilling"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Uridium Per Hour"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tim_second As System.Windows.Forms.Timer
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Alien_Combo As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblUri As System.Windows.Forms.Label
    Friend WithEvents lblTime As System.Windows.Forms.Label
End Class
