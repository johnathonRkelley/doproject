﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Kill_Calculator
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Kill_Calculator))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.radExp = New System.Windows.Forms.RadioButton()
        Me.radHon = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.NextRank_Label = New System.Windows.Forms.Label()
        Me.YourRank_Label = New System.Windows.Forms.Label()
        Me.YouNeed_Label = New System.Windows.Forms.Label()
        Me.Calculation_Label = New System.Windows.Forms.Label()
        Me.KillType_Label = New System.Windows.Forms.Label()
        Me.NextRank_Text = New System.Windows.Forms.TextBox()
        Me.YourRank_Text = New System.Windows.Forms.TextBox()
        Me.Calculate_Button = New System.Windows.Forms.Button()
        Me.AS_Combo = New System.Windows.Forms.ComboBox()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(543, 47)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Select what you want to calculate"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'radExp
        '
        Me.radExp.AutoSize = True
        Me.radExp.Checked = True
        Me.radExp.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.radExp.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radExp.Location = New System.Drawing.Point(42, 49)
        Me.radExp.Margin = New System.Windows.Forms.Padding(2)
        Me.radExp.Name = "radExp"
        Me.radExp.Size = New System.Drawing.Size(105, 21)
        Me.radExp.TabIndex = 1
        Me.radExp.TabStop = True
        Me.radExp.Text = "Experience"
        Me.radExp.UseVisualStyleBackColor = True
        '
        'radHon
        '
        Me.radHon.AutoSize = True
        Me.radHon.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.radHon.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radHon.Location = New System.Drawing.Point(239, 49)
        Me.radHon.Margin = New System.Windows.Forms.Padding(2)
        Me.radHon.Name = "radHon"
        Me.radHon.Size = New System.Drawing.Size(69, 21)
        Me.radHon.TabIndex = 2
        Me.radHon.Text = "Honor"
        Me.radHon.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.NextRank_Label)
        Me.Panel1.Controls.Add(Me.YourRank_Label)
        Me.Panel1.Controls.Add(Me.YouNeed_Label)
        Me.Panel1.Controls.Add(Me.Calculation_Label)
        Me.Panel1.Controls.Add(Me.KillType_Label)
        Me.Panel1.Controls.Add(Me.NextRank_Text)
        Me.Panel1.Controls.Add(Me.YourRank_Text)
        Me.Panel1.Controls.Add(Me.Calculate_Button)
        Me.Panel1.Controls.Add(Me.AS_Combo)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 74)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(543, 115)
        Me.Panel1.TabIndex = 3
        '
        'NextRank_Label
        '
        Me.NextRank_Label.AutoSize = True
        Me.NextRank_Label.Location = New System.Drawing.Point(55, 68)
        Me.NextRank_Label.Name = "NextRank_Label"
        Me.NextRank_Label.Size = New System.Drawing.Size(111, 13)
        Me.NextRank_Label.TabIndex = 18
        Me.NextRank_Label.Text = "Next Experience ="
        '
        'YourRank_Label
        '
        Me.YourRank_Label.AutoSize = True
        Me.YourRank_Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.YourRank_Label.Location = New System.Drawing.Point(12, 39)
        Me.YourRank_Label.Name = "YourRank_Label"
        Me.YourRank_Label.Size = New System.Drawing.Size(156, 13)
        Me.YourRank_Label.TabIndex = 17
        Me.YourRank_Label.Text = "Your Current Experience ="
        '
        'YouNeed_Label
        '
        Me.YouNeed_Label.AutoSize = True
        Me.YouNeed_Label.Location = New System.Drawing.Point(411, 39)
        Me.YouNeed_Label.Name = "YouNeed_Label"
        Me.YouNeed_Label.Size = New System.Drawing.Size(108, 13)
        Me.YouNeed_Label.TabIndex = 16
        Me.YouNeed_Label.Text = "You need to kill..."
        '
        'Calculation_Label
        '
        Me.Calculation_Label.AutoSize = True
        Me.Calculation_Label.Location = New System.Drawing.Point(414, 68)
        Me.Calculation_Label.Name = "Calculation_Label"
        Me.Calculation_Label.Size = New System.Drawing.Size(63, 17)
        Me.Calculation_Label.TabIndex = 15
        Me.Calculation_Label.Text = "Calculation"
        Me.Calculation_Label.UseCompatibleTextRendering = True
        '
        'KillType_Label
        '
        Me.KillType_Label.AutoSize = True
        Me.KillType_Label.Location = New System.Drawing.Point(255, 95)
        Me.KillType_Label.Name = "KillType_Label"
        Me.KillType_Label.Size = New System.Drawing.Size(35, 13)
        Me.KillType_Label.TabIndex = 14
        Me.KillType_Label.Text = "Type"
        '
        'NextRank_Text
        '
        Me.NextRank_Text.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NextRank_Text.Location = New System.Drawing.Point(176, 65)
        Me.NextRank_Text.MaxLength = 30
        Me.NextRank_Text.Name = "NextRank_Text"
        Me.NextRank_Text.Size = New System.Drawing.Size(232, 20)
        Me.NextRank_Text.TabIndex = 12
        '
        'YourRank_Text
        '
        Me.YourRank_Text.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.YourRank_Text.Location = New System.Drawing.Point(176, 39)
        Me.YourRank_Text.MaxLength = 30
        Me.YourRank_Text.Name = "YourRank_Text"
        Me.YourRank_Text.Size = New System.Drawing.Size(232, 20)
        Me.YourRank_Text.TabIndex = 11
        '
        'Calculate_Button
        '
        Me.Calculate_Button.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Calculate_Button.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Calculate_Button.Location = New System.Drawing.Point(12, 91)
        Me.Calculate_Button.Name = "Calculate_Button"
        Me.Calculate_Button.Size = New System.Drawing.Size(237, 21)
        Me.Calculate_Button.TabIndex = 13
        Me.Calculate_Button.Text = "Calculate"
        Me.Calculate_Button.UseVisualStyleBackColor = True
        '
        'AS_Combo
        '
        Me.AS_Combo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.AS_Combo.FormattingEnabled = True
        Me.AS_Combo.Items.AddRange(New Object() {"Streuner", "Boss Streuner", "Uber Streuner", "Lordakia", "Boss Lordakia", "Uber Lordakia", "Mordon", "Boss Mordon", "Uber Mordon", "Saimon", "Boss Saimon", "Uber Saimon", "Devolarium", "Boss Devolarium", "Uber Devolarium", "Sibelon", "Boss Sibelon", "Uber Sibelon", "Sibelonit", "Boss Sibelonit", "Uber Sibelonit", "Lordakium", "Boss Lordakium", "Uber Lordakium", "Kristallin", "Boss Kristallin", "Uber Kristallin", "Kristallon", "Boss Kristallon", "Uber Kristallon", "Streune[R]", "Boss Streune[R]", "Uber Streune[R]", "Protegit", "Cubikon", "Kucurbium", "Phoenix", "Yamato", "Leonov", "Defcom", "Liberator", "Piranha", "Nostromo", "BigBoy", "Vengeance", "Goliath", "Aegis", "Citadel", "Spearhead", "Tartarus"})
        Me.AS_Combo.Location = New System.Drawing.Point(12, 6)
        Me.AS_Combo.Name = "AS_Combo"
        Me.AS_Combo.Size = New System.Drawing.Size(236, 21)
        Me.AS_Combo.TabIndex = 10
        '
        'Kill_Calculator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(543, 189)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.radHon)
        Me.Controls.Add(Me.radExp)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Kill_Calculator"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Kill Calculator"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents radExp As System.Windows.Forms.RadioButton
    Friend WithEvents radHon As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents NextRank_Label As System.Windows.Forms.Label
    Friend WithEvents YourRank_Label As System.Windows.Forms.Label
    Friend WithEvents YouNeed_Label As System.Windows.Forms.Label
    Friend WithEvents Calculation_Label As System.Windows.Forms.Label
    Friend WithEvents KillType_Label As System.Windows.Forms.Label
    Friend WithEvents NextRank_Text As System.Windows.Forms.TextBox
    Friend WithEvents YourRank_Text As System.Windows.Forms.TextBox
    Friend WithEvents Calculate_Button As System.Windows.Forms.Button
    Friend WithEvents AS_Combo As System.Windows.Forms.ComboBox
End Class
