﻿Public Class GalaxyGates_Form
    Dim db As Database

    Private Sub GateAlpha_Radio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GateAlpha_Radio.CheckedChanged
        GalaxyGates_Label.Text = "Wave 1   - 40 Streuner" & vbNewLine & "Wave 2   - 40 Lordakia" & vbNewLine & "Wave 3   - 40 Mordon" & vbNewLine & "Wave 4   - 80 Saimon" & vbNewLine & "Wave 5   - 20 Devolarium" & vbNewLine & "Wave 6   - 80 Kristallin" & vbNewLine & "Wave 7   - 20 Sibelon" & vbNewLine & "Wave 8   - 80 Sibelonit" & vbNewLine & "Wave 9   - 16 Kristallon" & vbNewLine & "Wave 10 - 30 Protegit"
    End Sub

    Private Sub GateBeta_Radio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GateBeta_Radio.CheckedChanged
        GalaxyGates_Label.Text = "Wave 1   - 40 Streuner" & vbNewLine & "Wave 2   - 40 Lordakia" & vbNewLine & "Wave 3   - 40 Mordon" & vbNewLine & "Wave 4   - 80 Saimon" & vbNewLine & "Wave 5   - 20 Devolarium" & vbNewLine & "Wave 6   - 80 Kristallin" & vbNewLine & "Wave 7   - 20 Sibelon" & vbNewLine & "Wave 8   - 80 Sibelonit" & vbNewLine & "Wave 9   - 16 Kristallon" & vbNewLine & "Wave 10 - 30 Protegit"
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GateGamma_Radio.CheckedChanged
        GalaxyGates_Label.Text = "Wave 1   - 40 Streuner" & vbNewLine & "Wave 2   - 40 Lordakia" & vbNewLine & "Wave 3   - 40 Mordon" & vbNewLine & "Wave 4   - 80 Saimon" & vbNewLine & "Wave 5   - 20 Devolarium" & vbNewLine & "Wave 6   - 80 Kristallin" & vbNewLine & "Wave 7   - 20 Sibelon" & vbNewLine & "Wave 8   - 80 Sibelonit" & vbNewLine & "Wave 9   - 16 Kristallon" & vbNewLine & "Wave 10 - 30 Protegit"
    End Sub

    Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GateDelta_Radio.CheckedChanged
        GalaxyGates_Label.Text = "Wave 1   - 5 Lordakia, 10 Mordon, 15 Saimon" & vbNewLine & "Wave 2   - 11 Streuner, 1 StreuneR" & vbNewLine & "Wave 3   - 5 Mordon, 10 Saimon, 15 Kristallin" & vbNewLine & "Wave 4   - 12 Lordakia, 1 Lordakium" & vbNewLine & "Wave 5   - 10 Boss Lordakia, 6 Boss Samonites, 8 Boss Mordons" & vbNewLine & "Wave 6   - 15 Sibelonit, 1 Sibelon" & vbNewLine & "Wave 7   - 5 Sibelonit, 10 Kristallin, 5 Boss StreuneR" & vbNewLine & "Wave 8   - 10 Kristallin, 1 Kristallon" & vbNewLine & "Wave 9   - 15 Protegits, 3 Boss Lordakium" & vbNewLine & "Wave 10 - 3 SaNeJEwZ"
    End Sub

    Private Sub Close_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Close_Button.Click
        Me.Hide()
        Main_Form.Show()
    End Sub

    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    Private Sub GalaxyGates_Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        db.print()
    End Sub
End Class