﻿Imports System.Data.OleDb

Public Class Space_Map_Form
    Dim myConnectionString As String
    Dim db As Database
    'Connection Variable
    Dim con As New OleDbConnection

    Dim mapName As String
    Dim company As String
    Dim cbs As String
    Dim homeMap As String
    Dim npcs As String
    Dim description As String
    Dim resources As String

    Private Sub Space_Map_Form_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Me.Hide()
        Star_System.Show()

    End Sub

    Private Sub Space_Map_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If db Is Nothing Then

        Else

        End If
    End Sub

    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    Public Sub loadAccess(query As String)
        Dim fileLocation As String
        Dim picFile As String
        Dim pictureFile As String


        'Establishes the File Location for the images
        fileLocation = Application.StartupPath & "\images\BACKGROUNDS\"

        'Creates a DataTable
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds.Tables.Add(dt)
        Dim da As New OleDbDataAdapter

        Dim cmd As String = query

        'Query
        da = db.getOleDbAdapter(cmd)

        'Fills data
        da.Fill(dt)

        Dim mapName As String
        Dim company As String
        Dim cbs As String
        Dim homeMap As String
        Dim npcs As String

        'Sets the text fields for each of them
        mapName = dt.Rows(0).Item(0).ToString
        lblMap.Text = mapName

        company = dt.Rows(0).Item(1).ToString
        lblCompany.Text = company
        cbs = dt.Rows(0).Item(2).ToString
        lblCBS.Text = cbs
        homeMap = dt.Rows(0).Item(3).ToString
        lblHome.Text = homeMap
        npcs = dt.Rows(0).Item(4).ToString
        Dim break As String = separate(npcs)
        txtNPCs.Text = break

        picFile = dt.Rows(0).Item(8).ToString

        pictureFile = fileLocation & picFile
        mapHolder.BackgroundImage = System.Drawing.Image.FromFile(pictureFile)
    End Sub

    'Breaks up the Text
    Public Function separate(ByVal info As String) As String
        Dim line As String = ""
        Dim param As Boolean = False
        For Each c As Char In info
            Dim letter As String = c.ToString

            If letter = "<" Then
                param = True
            ElseIf letter = ">" Then
                param = False
                Continue For
            End If

            If param = True Then
                'Do Nothing and let it go through
            Else
                If letter = "\" Then
                    line = line & Environment.NewLine
                Else
                    line = line & letter
                End If
            End If
        Next
        Return line
    End Function

End Class