﻿Imports System.Data.OleDb

Public Class AuctionFrm
    Private timeLeft As Integer
    Private one, five, ten As Boolean
    Dim db As Database

    'Passes over the database from the GUI
    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    'Loading, Closing
    Protected Overrides Sub OnFormClosing(ByVal e As FormClosingEventArgs)
        MyBase.OnFormClosing(e)
        If Not e.Cancel AndAlso e.CloseReason = CloseReason.UserClosing Then
            e.Cancel = True
            NotifyIcon1.ShowBalloonTip(500, "Auction Reminder", "Auction Reminder is still running, and will give you notifications. If you choose to not have them, disable in settings.", ToolTipIcon.Info)

            Me.Hide()
        End If
    End Sub
    Private Sub AuctionFrm_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Me.WindowState = FormWindowState.Minimized
    End Sub
    Private Sub AuctionFrm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Start()

        NotifyIcon1.Icon = SystemIcons.Information
        NotifyIcon1.BalloonTipTitle = "Bid Warning"
        NotifyIcon1.BalloonTipText = "Balloon Tip Text."
        NotifyIcon1.BalloonTipIcon = ToolTipIcon.Info

        'Checks for Database information
        If db Is Nothing Then
            'Do Nothing
        Else
            one = change("OneMinute")
            five = change("FiveMinute")
            ten = change("TenMinute")
        End If

        'Checks if the One minute warning is True
        If one = True Then
            MinuteWarningOnToolStripMenuItem.Text = "One Minute Warning (On)"
        Else
            MinuteWarningOnToolStripMenuItem.Text = "One Minute Warning (Off)"
        End If

        'Checks if the One minute warning is True
        If five = True Then
            MinuteWarningOnToolStripMenuItem1.Text = "Five Minute Warning (On)"
        Else
            MinuteWarningOnToolStripMenuItem1.Text = "Five Minute Warning (Off)"
        End If

        'Checks if the One minute warning is True
        If ten = True Then
            MinuteWarningOnToolStripMenuItem2.Text = "Ten Minute Warning (On)"
        Else
            MinuteWarningOnToolStripMenuItem2.Text = "Ten Minute Warning (Off)"
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim Hrs As Integer
        Dim Min As Integer
        Dim Sec As Integer

        'Console.WriteLine(Hour(Now) & ":" & Minute(Now) & ":" & Second(Now))

        'Seconds'
        Sec = timeLeft Mod 60

        'Minutes'
        Min = ((timeLeft - Sec) / 60) Mod 60

        'Hours'
        Hrs = ((timeLeft - (Sec + (Min * 60))) / 3600) Mod 60

        'Checks to Notify
        notify()

        'While the time is greater than 0 keep changing
        If timeLeft > 0 Then
            timeLeft -= 1
            timeLabel.Text = Min & " minutes " & Sec & " seconds"
        Else
            'Starts over
            timeLeft = 3599
        End If
    End Sub

    Public Sub Start()
        ' Start the timer.
        'timeLeft = 3599
        'Seconds'
        Dim Sec As Integer = timeLeft Mod 60

        'Minutes'
        Dim Min As Integer = ((timeLeft - Sec) / 60) Mod 60
        'Gets the current time minus a minute to give around right time
        Dim bid As Integer = 15 * 60
        Dim current As Integer = (60 * 60) - (Minute(Now) * 60) + (60 - Second(Now)) - 60

        timeLeft = current + bid

        'timeLabel.Text = "30 seconds"
        Timer1.Start()
    End Sub

    Private Sub notify()
        Dim oneMinute As Integer = 60
        Dim fiveMinutes As Integer = 60 * 5
        Dim tenMinutes As Integer = 60 * 10

        If timeLeft = oneMinute And one = True Then
            NotifyIcon1.ShowBalloonTip(500, "One Minute Warning", "You have one minute left! Be sure to bid!", ToolTipIcon.Info)
        ElseIf timeLeft = fiveMinutes And five = True Then
            NotifyIcon1.ShowBalloonTip(500, "Five Minute Warning", "You have five minutes left! Be sure to bid!", ToolTipIcon.Info)
        ElseIf timeLeft = tenMinutes And ten = True Then
            NotifyIcon1.ShowBalloonTip(500, "Ten Minute Warning", "You have ten minutes left! Be sure to bid!", ToolTipIcon.Info)
        End If

    End Sub

    Private Sub timeLabel_MouseClick(sender As Object, e As MouseEventArgs) Handles timeLabel.MouseClick
        NotifyIcon1.Visible = True
    End Sub

    'Minute Warnings Menu Bar
    Private Sub MinuteWarningOnToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MinuteWarningOnToolStripMenuItem.Click
        Dim name As String = "OneMinute"

        If one = True Then

            Dim cmd As String = "Update Settings Set Active = False Where Setting = '" & name & "'"

            Dim da As New OleDbDataAdapter
            Dim connection As OleDbConnection

            da = db.getOleDbAdapter(cmd)
            connection = db.getConnection
            da.UpdateCommand = connection.CreateCommand
            da.UpdateCommand.CommandText = cmd
            da.UpdateCommand.ExecuteNonQuery()


            MinuteWarningOnToolStripMenuItem.Text = "One Minute Warning (Off)"
            one = False
        Else
            MinuteWarningOnToolStripMenuItem.Text = "One Minute Warning (On)"

            Dim cmd As String = "Update Settings Set Active = True Where Setting = '" & name & "'"

            Dim da As New OleDbDataAdapter
            Dim connection As OleDbConnection

            da = db.getOleDbAdapter(cmd)
            connection = db.getConnection
            da.UpdateCommand = connection.CreateCommand
            da.UpdateCommand.CommandText = cmd
            da.UpdateCommand.ExecuteNonQuery()


            one = True
        End If
    End Sub
    Private Sub MinuteWarningOnToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles MinuteWarningOnToolStripMenuItem1.Click
        Dim name As String = "FiveMinute"

        If five = True Then
            Dim cmd As String = "Update Settings Set Active = False Where Setting = '" & name & "'"

            Dim da As New OleDbDataAdapter
            Dim connection As OleDbConnection

            da = db.getOleDbAdapter(cmd)
            connection = db.getConnection
            da.UpdateCommand = connection.CreateCommand
            da.UpdateCommand.CommandText = cmd
            da.UpdateCommand.ExecuteNonQuery()

            MinuteWarningOnToolStripMenuItem1.Text = "Five Minute Warning (Off)"
            five = False
        Else

            Dim cmd As String = "Update Settings Set Active = True Where Setting = '" & name & "'"

            Dim da As New OleDbDataAdapter
            Dim connection As OleDbConnection

            da = db.getOleDbAdapter(cmd)
            connection = db.getConnection
            da.UpdateCommand = connection.CreateCommand
            da.UpdateCommand.CommandText = cmd
            da.UpdateCommand.ExecuteNonQuery()

            MinuteWarningOnToolStripMenuItem1.Text = "Five Minute Warning (On)"
            five = True
        End If
    End Sub
    Private Sub MinuteWarningOnToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles MinuteWarningOnToolStripMenuItem2.Click
        Dim name As String = "TenMinute"

        If ten = True Then

            Dim cmd As String = "Update Settings Set Active = False Where Setting = '" & name & "'"

            Dim da As New OleDbDataAdapter
            Dim connection As OleDbConnection

            da = db.getOleDbAdapter(cmd)
            connection = db.getConnection
            da.UpdateCommand = connection.CreateCommand
            da.UpdateCommand.CommandText = cmd
            da.UpdateCommand.ExecuteNonQuery()

            MinuteWarningOnToolStripMenuItem2.Text = "Ten Minute Warning (Off)"
            ten = False
        Else

            Dim cmd As String = "Update Settings Set Active = True Where Setting = '" & name & "'"

            Dim da As New OleDbDataAdapter
            Dim connection As OleDbConnection

            da = db.getOleDbAdapter(cmd)
            connection = db.getConnection
            da.UpdateCommand = connection.CreateCommand
            da.UpdateCommand.CommandText = cmd
            da.UpdateCommand.ExecuteNonQuery()

            MinuteWarningOnToolStripMenuItem2.Text = "Ten Minute Warning (On)"
            ten = True
        End If
    End Sub
    Public Function change(ByVal name As String) As Boolean
        'Gets data ready to be put into a DataTable
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds.Tables.Add(dt)

        'This is the CMD string
        Dim cmd As String = "Select Active from Settings Where Setting = '" & name & "'"

        'Query
        Dim da As New OleDbDataAdapter
        da = db.getOleDbAdapter(cmd)

        'Fills the data
        da.Fill(dt)

        'Sets the text fields for each of them
        Dim bool As Boolean = dt.Rows(0).Item(0)
        Return bool
    End Function
End Class