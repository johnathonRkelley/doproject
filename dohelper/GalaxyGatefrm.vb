﻿Imports System.Data.OleDb

Public Class GalaxyGatefrm
    Dim db As Database
    Dim gateName As String

    'Passes the database to the Form to be used.
    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    'Form Load
    Private Sub GalaxyGatefrm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        gateName = "Alpha"

        ComboBox1.SelectedIndex() = 0

        getInformation()
    End Sub

    'Form Close
    Private Sub GalaxyGatefrm_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Me.Hide()
    End Sub

    'This method grabs information from the Database
    Public Sub getInformation()
        'Gets inputed values from the user
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim da As New OleDbDataAdapter
        ds.Tables.Add(dt)

        'Gets the name of the Gate.
        gateName = ComboBox1.SelectedItem.ToString


        If gateName = Nothing Then
            gateName = "Alpha"
        End If
        Dim query As String = "SELECT * FROM GalaxyGates where GateName = '" & gateName & "'"

        da = db.getOleDbAdapter(query)
        da.Fill(dt)

        Dim waveInfo As String = dt.Rows(0).Item(2).ToString
        Dim information As String = separate(waveInfo)
        Dim reward As String = separate(dt.Rows(0).Item(3))
        txtInformation.Text = information
        txtReward.Text = reward

    End Sub

    'Breaks up the Text
    Public Function separate(ByVal info As String) As String
        Dim line As String = ""
        Dim param As Boolean = False
        For Each c As Char In info
            Dim letter As String = c.ToString

            If letter = "<" Then
                param = True
            ElseIf letter = ">" Then
                param = False
                Continue For
            End If

            If param = True Then
                'Do Nothing and let it go through
            Else
                If letter = "\" Then
                    line = line & Environment.NewLine
                Else
                    line = line & letter
                End If
            End If
            
        Next

        Return line
    End Function

    'Check Boxes
    Private Sub radioReward_CheckedChanged(sender As Object, e As EventArgs) Handles radioReward.CheckedChanged
        txtReward.Visible = True
        txtInformation.Visible = False
    End Sub
    Private Sub radioWave_CheckedChanged(sender As Object, e As EventArgs) Handles radioWave.CheckedChanged
        txtInformation.Visible = True
        txtReward.Visible = False
    End Sub

    'ComboBox Selected Index Change
    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        getInformation()
    End Sub
End Class