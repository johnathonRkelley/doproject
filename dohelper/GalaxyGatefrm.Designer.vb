﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class GalaxyGatefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GalaxyGatefrm))
        Me.txtInformation = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.radioWave = New System.Windows.Forms.RadioButton()
        Me.radioReward = New System.Windows.Forms.RadioButton()
        Me.txtReward = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtInformation
        '
        Me.txtInformation.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInformation.Location = New System.Drawing.Point(9, 76)
        Me.txtInformation.Margin = New System.Windows.Forms.Padding(2)
        Me.txtInformation.Multiline = True
        Me.txtInformation.Name = "txtInformation"
        Me.txtInformation.ReadOnly = True
        Me.txtInformation.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInformation.Size = New System.Drawing.Size(336, 143)
        Me.txtInformation.TabIndex = 0
        '
        'ComboBox1
        '
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"Alpha", "Beta", "Delta", "Epsilon", "Gamma", "Hades", "Kappa", "Kronos", "Kuiper", "Lambda", "Zeta"})
        Me.ComboBox1.Location = New System.Drawing.Point(156, 10)
        Me.ComboBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(189, 28)
        Me.ComboBox1.Sorted = True
        Me.ComboBox1.TabIndex = 1
        '
        'radioWave
        '
        Me.radioWave.AutoSize = True
        Me.radioWave.Checked = True
        Me.radioWave.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.radioWave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radioWave.Location = New System.Drawing.Point(7, 55)
        Me.radioWave.Margin = New System.Windows.Forms.Padding(2)
        Me.radioWave.Name = "radioWave"
        Me.radioWave.Size = New System.Drawing.Size(124, 17)
        Me.radioWave.TabIndex = 2
        Me.radioWave.TabStop = True
        Me.radioWave.Text = "Wave Information"
        Me.radioWave.UseVisualStyleBackColor = True
        '
        'radioReward
        '
        Me.radioReward.AutoSize = True
        Me.radioReward.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.radioReward.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radioReward.Location = New System.Drawing.Point(190, 55)
        Me.radioReward.Margin = New System.Windows.Forms.Padding(2)
        Me.radioReward.Name = "radioReward"
        Me.radioReward.Size = New System.Drawing.Size(134, 17)
        Me.radioReward.TabIndex = 3
        Me.radioReward.Text = "Reward Information"
        Me.radioReward.UseVisualStyleBackColor = True
        '
        'txtReward
        '
        Me.txtReward.Font = New System.Drawing.Font("Microsoft Tai Le", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReward.Location = New System.Drawing.Point(9, 76)
        Me.txtReward.Margin = New System.Windows.Forms.Padding(2)
        Me.txtReward.Multiline = True
        Me.txtReward.Name = "txtReward"
        Me.txtReward.ReadOnly = True
        Me.txtReward.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtReward.Size = New System.Drawing.Size(336, 143)
        Me.txtReward.TabIndex = 4
        Me.txtReward.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(10, 12)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(121, 24)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Galaxy Gate :"
        '
        'GalaxyGatefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(358, 229)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtReward)
        Me.Controls.Add(Me.radioReward)
        Me.Controls.Add(Me.radioWave)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.txtInformation)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "GalaxyGatefrm"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Galaxy Gate Informaiton"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtInformation As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents radioWave As System.Windows.Forms.RadioButton
    Friend WithEvents txtReward As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Protected Friend WithEvents radioReward As RadioButton
End Class
