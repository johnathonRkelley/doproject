﻿Imports System.Data.OleDb

Public Class EfficientKilling
    Dim hour As Integer = 3600
    Dim second As Integer = 0
    Dim alienUri As Integer
    Dim timeLeft As Integer
    Dim stopped As Boolean

    Dim db As Database

    Private Sub tim_second_Tick(sender As Object, e As EventArgs) Handles tim_second.Tick
        Dim Sec, Min As Integer
        'Seconds'
        Sec = second Mod 60

        'Minutes'
        Min = ((second - Sec) / 60) Mod 60


        second += 1
        lblTime.Text = Min & ":" & Sec

        'lblSeconds.Text = second
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        tim_second.Start()
        Button2.Enabled = False
        Button1.Enabled = True
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        tim_second.Stop()
        Button1.Enabled = False
        Button2.Enabled = True
        stopped = True

        Dim calculation As Integer = hour / second

        'lblCalc.Text = calculation

        lblUri.Text = "Uridium Per Hour : " & calculation * alienUri
        second = 0

        lblTime.Text = "0:0"

        'lblSeconds.Text = 0
    End Sub

    Private Sub EfficientKilling_Load(sender As Object, e As EventArgs) Handles Me.Load
        Alien_Combo.SelectedIndex = 0
        lblTime.Text = "0:0"
        If db Is Nothing Then

        Else
            change()
        End If
    End Sub

    'Passes over the database from the GUI
    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    Public Sub change()
        'Gets data ready to be put into a DataTable
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds.Tables.Add(dt)

        'Grabs the name from the Combo Box
        Dim name As String
        name = Alien_Combo.SelectedItem.ToString

        'This is the CMD string
        Dim cmd As String = "SELECT * FROM npcs where name = '" & name & "'"

        'Query
        Dim da As New OleDbDataAdapter
        da = db.getOleDbAdapter(cmd)

        'Fills the data
        da.Fill(dt)

        'Sets the text fields for each of them

        alienUri = dt.Rows(0).Item(9)
    End Sub

    Private Sub Alien_Combo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Alien_Combo.SelectedIndexChanged
        If db Is Nothing Then

        Else
            change()
        End If
    End Sub
End Class