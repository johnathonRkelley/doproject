﻿Imports System.Data.OleDb
Public Class Aliens_Form
    'Variables used 
    Dim alienName As String
    Dim hp As Integer
    Dim shields As Integer
    Dim ep As Integer
    Dim honor As Integer
    Dim credits As Integer
    Dim uridium As Integer
    Dim speed As Integer
    Dim maxDamage As Integer
    Dim alienLocation As String

    'Dim con As New OleDbConnection
    Dim db As Database

    'Loads default data into the NPC Database
    Private Sub Aliens_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Alien_Combo.SelectedIndex = 0

        If db Is Nothing Then

        Else
            change()
        End If
    End Sub

    'Closes
    Private Sub Aliens_Form_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Me.Close()
    End Sub

    'Passes over the database from the GUI
    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    'Loads access files on the change of Alien Combo
    Private Sub Alien_Combo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Alien_Combo.SelectedIndexChanged
        If db Is Nothing Then

        Else
            change()
        End If
    End Sub

    'This changes the data based on the database.
    Public Sub change()
        'Local Variables
        Dim fileLocation As String
        Dim picFile As String
        Dim pictureFile As String

        'Establishes the File Location for the images
        fileLocation = Application.StartupPath & "\images\npcs\"

        'Gets data ready to be put into a DataTable
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds.Tables.Add(dt)

        'Grabs the name from the Combo Box
        Dim name As String
        name = Alien_Combo.SelectedItem.ToString

        'This is the CMD string
        Dim cmd As String = "SELECT * FROM npcs where name = '" & name & "'"

        'Query
        Dim da As New OleDbDataAdapter
        da = db.getOleDbAdapter(cmd)

        'Fills the data
        da.Fill(dt)

        'Sets the text fields for each of them
        alienHP.Text = dt.Rows(0).Item(0)
        alienShields.Text = dt.Rows(0).Item(3)
        alienMaxDamage.Text = dt.Rows(0).Item(2)
        alienSpeed.Text = dt.Rows(0).Item(5)
        alienEP.Text = dt.Rows(0).Item(4)
        alienCredits.Text = dt.Rows(0).Item(6)
        alienHonor.Text = dt.Rows(0).Item(7)
        alienUridium.Text = dt.Rows(0).Item(9)
        lblAlienName.Text = dt.Rows(0).Item(1)
        picFile = dt.Rows(0).Item(10).ToString
        alienLocation = dt.Rows(0).Item(11).ToString

        lblLocation.Text = "Found In : " & alienLocation

        'Gets the picture file
        pictureFile = fileLocation & picFile

        'Plugs in the Picture File
        picturePanel.BackgroundImage = System.Drawing.Image.FromFile(pictureFile)
    End Sub
End Class