﻿Imports System.Data.OleDb

Public Class Kill_Calculator
    'Variables usd
    Dim yourrank, nextrank, neededpoints As Integer
    Dim x As Double
    Dim db As Database

    'Database is being passed through
    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    'Closes
    Private Sub Kill_Calculator_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Me.Hide()
    End Sub

    '------- Interactions --------
    Private Sub Calculate_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Calculate_Button.Click
        x = 1 'Reinitializes the variables

        '----- Checks for empty textboxes 

        If NextRank_Text.Text = "" And YourRank_Text.Text = "" Then
            MsgBox("You must enter your current rank points and the points for your next rank!  ", MsgBoxStyle.OkOnly, "Error!")
            GoTo end1
        End If

        If NextRank_Text.Text = "" Then
            MsgBox("You must enter the points for your next rank!  ", MsgBoxStyle.OkOnly, "Error!")
            GoTo end1
        End If

        If YourRank_Text.Text = "" Then
            MsgBox("You must enter your rank points!", MsgBoxStyle.OkOnly, "Error!")
            GoTo end1
        End If

        '----- Sets variables from data presented

        nextrank = NextRank_Text.Text
        yourrank = YourRank_Text.Text
        neededpoints = nextrank - yourrank

        '----- Checks to see if that the rank is possible and sees if the combobox has been changed

        If nextrank <= yourrank Then
            If nextrank = 0 Then
                MsgBox("This is not possible!  Zero is invalid!", MsgBoxStyle.OkOnly, "Error!")
                GoTo end1
            End If
            MsgBox("This is not possible!", MsgBoxStyle.OkOnly, "Error!")
            GoTo end1
        End If

        If nextrank <= 0 Then
            MsgBox("You must enter your current rank points!  Zero is invalid!", MsgBoxStyle.OkOnly, "Error!")
            GoTo end1
        End If

        If AS_Combo.SelectedItem = "" Then
            MsgBox("Please select an Alien or Ship!", MsgBoxStyle.OkOnly, "Error!")
            GoTo end1
        End If

        'Gets information from the Access Database
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds.Tables.Add(dt)
        Dim name As String = AS_Combo.SelectedItem.ToString

        'Query
        Dim da As New OleDbDataAdapter

        Dim cmd As String

        'Checks to see what to look for
        If radExp.Checked = True Then
            cmd = "SELECT ep FROM Mixed where Ship = '" & name & "'"
        Else
            cmd = "SELECT honor FROM Mixed where ship = '" & name & "'"
        End If
        'Dim cmd As String = "SELECT ep FROM npcs where name = '" & name & "'"
        'Dim cmd As String = "SELECT honor FROM npcs where name = '" & name & "'"

        da = db.getOleDbAdapter(cmd)

        'Fills data
        da.Fill(dt)
        'Sets the text fields for each of them
        Dim point As Double
        Try
            point = dt.Rows(0).Item(0)
        Catch ex As Exception
            Console.WriteLine(name)
        End Try

        x = neededpoints / point
        Calculation_Label.Text = x

        KillType_Label.Text = name & "(s)"
end1:
    End Sub
    Private Sub YourRank_Text_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles YourRank_Text.KeyPress
        If (e.KeyChar < "0" OrElse e.KeyChar > "9") _
    AndAlso e.KeyChar <> ControlChars.Back AndAlso e.KeyChar <> "." Then
            'cancel keys
            e.Handled = True
        End If
        If e.KeyChar = "." Then
            e.Handled = True
        End If
    End Sub
    Private Sub NextRank_Text_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NextRank_Text.KeyPress
        If (e.KeyChar < "0" OrElse e.KeyChar > "9") _
    AndAlso e.KeyChar <> ControlChars.Back AndAlso e.KeyChar <> "." Then
            'cancel keys
            e.Handled = True
        End If
        If e.KeyChar = "." Then
            e.Handled = True
        End If
    End Sub

    Private Sub radExp_CheckedChanged(sender As Object, e As EventArgs) Handles radExp.CheckedChanged
        YourRank_Label.Text = "Your Current Experience = "
        NextRank_Label.Text = "Next Experience = "
    End Sub

    Private Sub YourRank_Text_TextChanged(sender As Object, e As EventArgs) Handles YourRank_Text.TextChanged

    End Sub

    Private Sub radHon_CheckedChanged(sender As Object, e As EventArgs) Handles radHon.CheckedChanged
        YourRank_Label.Text = "Your Current Honor = "
        NextRank_Label.Text = "Next Honor = "
    End Sub

    Private Sub Kill_Calculator_Load(sender As Object, e As EventArgs) Handles Me.Load
        AS_Combo.SelectedIndex = 0
    End Sub
End Class