﻿Public Class Star_System
    'Intialized variables
    Dim maps As Boolean = True
    Dim db As Database

    'Form load and Dispose
    Private Sub Star_System_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        maps = True
        lblCurrentSystem.Text = "Current System : Lower Maps"
        'Enabled Buttons
        btn11.Enabled = True
        btn12.Enabled = True
        btn13.Enabled = True
        btn14.Enabled = True
        btn21.Enabled = True
        btn22.Enabled = True
        btn23.Enabled = True
        btn24.Enabled = True
        btn31.Enabled = True
        btn32.Enabled = True
        btn33.Enabled = True
        btn34.Enabled = True
        btn41.Enabled = True
        btn42.Enabled = True
        btn43.Enabled = True

        'Disabled Buttons
        btn44.Enabled = False
        btn45.Enabled = False
        btn51.Enabled = False
        btn52.Enabled = False
        btn53.Enabled = False
        btn15.Enabled = False
        btn16.Enabled = False
        btn17.Enabled = False
        btn18.Enabled = False
        btn25.Enabled = False
        btn26.Enabled = False
        btn27.Enabled = False
        btn28.Enabled = False
        btn35.Enabled = False
        btn36.Enabled = False
        btn37.Enabled = False
        btn38.Enabled = False

        'Click Handler 
        AddHandler btn11.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn12.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn13.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn14.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn15.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn16.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn17.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn18.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn21.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn22.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn23.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn24.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn25.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn26.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn27.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn28.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn31.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn32.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn33.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn34.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn35.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn36.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn37.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn38.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn53.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn52.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn51.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn41.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn42.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn43.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn44.Click, AddressOf HandleDynamicButtonClick
        AddHandler btn45.Click, AddressOf HandleDynamicButtonClick
    End Sub
    Private Sub Star_System_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Me.Hide()
    End Sub

    'Label Click Method
    Private Sub lblSwitch_Click(sender As Object, e As EventArgs) Handles lblSwitch.Click
        maps = Not maps

        If maps = True Then
            lblCurrentSystem.Text = "Current System : Lower Maps"
            Me.BackgroundImage() = My.Resources.lowers
            btn11.Enabled = True
            btn12.Enabled = True
            btn13.Enabled = True
            btn14.Enabled = True
            btn21.Enabled = True
            btn22.Enabled = True
            btn23.Enabled = True
            btn24.Enabled = True
            btn31.Enabled = True
            btn32.Enabled = True
            btn33.Enabled = True
            btn34.Enabled = True
            btn41.Enabled = True
            btn42.Enabled = True
            btn43.Enabled = True

            'Disabled Buttons
            btn44.Enabled = False
            btn45.Enabled = False
            btn51.Enabled = False
            btn52.Enabled = False
            btn53.Enabled = False
            btn15.Enabled = False
            btn16.Enabled = False
            btn17.Enabled = False
            btn18.Enabled = False
            btn25.Enabled = False
            btn26.Enabled = False
            btn27.Enabled = False
            btn28.Enabled = False
            btn35.Enabled = False
            btn36.Enabled = False
            btn37.Enabled = False
            btn38.Enabled = False
            'maps = False
        ElseIf maps = False Then
            lblCurrentSystem.Text = "Current System : Upper Maps"
            Me.BackgroundImage() = My.Resources.uppers
            btn44.Enabled = True
            btn45.Enabled = True
            btn51.Enabled = True
            btn52.Enabled = True
            btn53.Enabled = True
            btn15.Enabled = True
            btn16.Enabled = True
            btn17.Enabled = True
            btn18.Enabled = True
            btn25.Enabled = True
            btn26.Enabled = True
            btn27.Enabled = True
            btn28.Enabled = True
            btn35.Enabled = True
            btn36.Enabled = True
            btn37.Enabled = True
            btn38.Enabled = True

            'Disabled Buttons
            btn11.Enabled = False
            btn12.Enabled = False
            btn13.Enabled = False
            btn14.Enabled = False
            btn21.Enabled = False
            btn22.Enabled = False
            btn23.Enabled = False
            btn24.Enabled = False
            btn31.Enabled = False
            btn32.Enabled = False
            btn33.Enabled = False
            btn34.Enabled = False
            btn41.Enabled = False
            btn42.Enabled = False
            btn43.Enabled = False
            'maps = True
        End If
    End Sub

    'This is the dynamic button click
    Public Sub HandleDynamicButtonClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim btn As Button = DirectCast(sender, Button)

        Dim button As Button = CType(sender, Button)
        Dim text As String
        text = button.Text
        Dim query As String
        Console.WriteLine(text)
        Console.WriteLine()
        query = "SELECT * FROM maps where MapName = '" & text & "';"

        Space_Map_Form.passThrough(db)
        Call Space_Map_Form.loadAccess(query)
        Space_Map_Form.Show()
        Me.Hide()
    End Sub

    'This passes the database through to the current form
    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub
End Class