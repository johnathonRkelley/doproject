﻿Imports System.Data.OleDb

Public Class Ships_Form
    'Global Variables
    Dim shipName As String
    Dim speedNumber As Integer
    Dim cargoNumber As Integer
    Dim lasersNumber As Integer
    Dim gens As Integer
    Dim hp As Integer
    Dim extrasNumber As Integer
    Dim spec As String
    Dim specText As String

    'This is the database variables
    Dim db As Database

    'Load and close
    Private Sub Ships_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        change()


        'Loads the dropdown box with the ships inside of the Datbase
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds.Tables.Add(dt)

        'The CMD Query
        Dim cmd As String = "SELECT name from Ships"

        'Query
        Dim da As New OleDbDataAdapter
        da = db.getOleDbAdapter(cmd)

        'Fills data
        da.Fill(dt)

        Dim b As Boolean = True
        Dim iter As Integer = 0
        While b = True
            Try
                If Not IsDBNull(dt.Rows(iter).Item(0)) Then 'Adds information from database
                    Dim a As String = dt.Rows(iter).Item(0)

                    ShipCombo.Items.Add(a)

                    iter = iter + 1
                Else
                    b = False
                End If
            Catch ex As IndexOutOfRangeException
                b = False
            End Try  
        End While

    End Sub
    Private Sub Ships_Form_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Me.Hide()
    End Sub

    'Passes database over to this form.
    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    'CB Selected Index Change
    Private Sub ShipCombo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ShipCombo.SelectedIndexChanged
        If db Is Nothing Then

        Else
            change()
        End If
    End Sub

    'This queries the database
    Public Sub change()
        'Local Variables
        Dim fileLocation As String
        Dim picFile As String
        Dim pictureFile As String

        'Gets the fileLocation for the images
        fileLocation = Application.StartupPath & "\images\ships\"

        'This is the dataset
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds.Tables.Add(dt)

        'This gets the name from the Combobox
        Dim name As String

        Try
            name = ShipCombo.SelectedItem.ToString
        Catch ex As NullReferenceException
            name = "Aegis"
        End Try


        'The CMD Query
        Dim cmd As String = "SELECT * FROM ships where name = '" & name & "'"

        'Query
        Dim da As New OleDbDataAdapter
        da = db.getOleDbAdapter(cmd)

        'Fills data
        da.Fill(dt)

        'Checks for DBNulls
        lblShipName.Text = name
        baseHP.Text = db.DbNullChecker(dt.Rows(0).Item(2))
        lblCargo.Text = db.DbNullChecker(dt.Rows(0).Item(3))
        lblSpeed.Text = db.DbNullChecker(dt.Rows(0).Item(4))
        lblGenerators.Text = db.DbNullChecker(dt.Rows(0).Item(5))
        lblExtras.Text = db.DbNullChecker(dt.Rows(0).Item(6))
        lblSpecial.Text = db.DbNullChecker(dt.Rows(0).Item(7))
        txtSpecial.Text = db.DbNullChecker(dt.Rows(0).Item(8))

        'Picture file from database and then gets the picture
        picFile = dt.Rows(0).Item(10).ToString

        If Not IsDBNull(dt.Rows(0).Item(10)) Then
            pictureFile = fileLocation & picFile
            PictureBox1.BackgroundImage = System.Drawing.Image.FromFile(pictureFile)
        Else
            Console.WriteLine("File not Found")
        End If
    End Sub
End Class