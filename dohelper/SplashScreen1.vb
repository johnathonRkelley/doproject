﻿Public NotInheritable Class SplashScreen1
    Dim change As Integer = 5
    Dim start As Integer = 0
    Dim periods As Integer = 0
    Dim wait As Integer = 0

    'Splashscreen on load
    Private Sub SplashScreen1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Timer2.Start() 'Loads the progress bar
        Timer1.Start() 'Loads the page wait
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If start = change Then
            Timer1.Stop()
            Timer2.Stop()
            Timer3.Stop()
            Dim bool As Boolean = CheckForUpdates()

            If bool = True Then
                Gui_frm.Show()
                Me.Hide()
            Else
                Timer4.Start()
            End If
        Else
            start = start + 1
        End If
    End Sub

    'Timer for the Progress Bar
    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        ProgressBar1.PerformStep()

        If ProgressBar1.Value = ProgressBar1.Maximum Then
            Timer3.Start()
        End If
    End Sub

    'Animated Timer for the Checking Updates
    Private Sub Timer3_Tick(sender As Object, e As EventArgs) Handles Timer3.Tick
        periods += 1
        If periods Mod 4 = 0 Then
            Label3.Text = "Checking for updates"
        ElseIf periods Mod 4 = 1 Then
            Label3.Text = "Checking for updates."
        ElseIf periods Mod 4 = 2 Then
            Label3.Text = "Checking for updates.."
        ElseIf periods Mod 4 = 3 Then
            Label3.Text = "Checking for updates..."
        End If
    End Sub

    'Updating method
    Public Function CheckForUpdates() As Boolean
        Dim updated = True
        Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create("https://www.dropbox.com/s/rj52xdjis2mt01d/Version.txt?dl=1")
        Dim response As System.Net.HttpWebResponse = request.GetResponse()
        Dim sr As System.IO.StreamReader = New System.IO.StreamReader(response.GetResponseStream())
        Dim newestversion As String = sr.ReadToEnd()
        Dim currentversion As String = Application.ProductVersion
        Console.WriteLine("Newest : " & newestversion)
        'currentversion = "bad"
        Console.WriteLine("Current : " & currentversion)
        If newestversion.Contains(currentversion) Then
            updated = True
            MsgBox("DOHelper is the most current version!")
        Else
            MsgBox("There is a newer version of DOHelper. We will download it for you now.")

            Dim result As DialogResult = MessageBox.Show("There is a new update. Would you like to update now?", "Update Now?", MessageBoxButtons.YesNo)

            If result = DialogResult.Yes Then

                'https://www.dropbox.com/s/ldsmfizzkizvbld/Setup.exe?dl=0

                WebBrowser1.Navigate("https://www.dropbox.com/s/ldsmfizzkizvbld/Setup.exe?dl=1")
                updated = False


            End If

        End If

        Return updated
    End Function

    Private Sub Timer4_Tick(sender As Object, e As EventArgs) Handles Timer4.Tick
        Try
            Dim p As String = Process.GetCurrentProcess.MainWindowTitle

            If p.Contains("Verify") Then
                Timer4.Stop()
                'MsgBox("We will turn off in the previous DOHelper while you install!", MsgBoxStyle.SystemModal, "Important!")
                Timer5.Start()
            End If

            If p Is Nothing Then

            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub WebBrowser1_FileDownload(sender As Object, e As EventArgs) Handles WebBrowser1.FileDownload
        Timer4.Start()
    End Sub

    Private Sub Timer5_Tick(sender As Object, e As EventArgs) Handles Timer5.Tick
        If wait = 10 Then
            Timer5.Stop()
            If MessageBox.Show("Are you installing your program now? If so, we will close the DOHelper, if not we will give more time.", "Respond", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                Environment.Exit(0)
            Else
                wait = 0
                Timer5.Start()
            End If
        Else
            wait += 1
        End If
    End Sub
End Class
