﻿Imports System.Data.OleDb
Public Class Main_Form
    'Database Object
    Dim db As Database

    'Variables usd
    Dim yourrank, nextrank, neededpoints As Integer
    Dim x As Double

    '-------Load and Close Method---------
    Private Sub Main_Form_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Me.Hide()
    End Sub
    Private Sub Main_Form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        db = New Database()        ' Declare Box2 of type Box
        db.loadDatabase()

        'Passing the data base through
        Aliens_Form.passThrough(db)
        Log_Disks.passThrough(db)
        Ships_Form.passThrough(db)
        Log_Disks.passThrough(db)
        GalaxyGates_Form.passThrough(db)
        GalaxyGatefrm.passThrough(db)
        Pilot_Bio.passThrough(db)
        Gui_frm.passThrough(db)
        Laser_Damage.passThrough(db)

        Gui_frm.Show()

        AS_Combo.SelectedIndex = 0

    End Sub

    '------- Interactions --------
    Private Sub Calculate_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Calculate_Button.Click
        x = 1 'Reinitializes the variables

        '----- Checks for empty textboxes 

        If NextRank_Text.Text = "" And YourRank_Text.Text = "" Then
            MsgBox("You must enter your current rank points and the points for your next rank!  ", MsgBoxStyle.OkOnly, "Error!")
            GoTo end1
        End If

        If NextRank_Text.Text = "" Then
            MsgBox("You must enter the points for your next rank!  ", MsgBoxStyle.OkOnly, "Error!")
            GoTo end1
        End If

        If YourRank_Text.Text = "" Then
            MsgBox("You must enter your rank points!", MsgBoxStyle.OkOnly, "Error!")
            GoTo end1
        End If

        '----- Sets variables from data presented

        nextrank = NextRank_Text.Text
        yourrank = YourRank_Text.Text
        neededpoints = nextrank - yourrank
        Console.WriteLine(neededpoints)

        '----- Checks to see if that the rank is possible and sees if the combobox has been changed

        If nextrank <= yourrank Then
            If nextrank = 0 Then
                MsgBox("This is not possible!  Zero is invalid!", MsgBoxStyle.OkOnly, "Error!")
                GoTo end1
            End If
            MsgBox("This is not possible!", MsgBoxStyle.OkOnly, "Error!")
            GoTo end1
        End If

        If nextrank <= 0 Then
            MsgBox("You must enter your current rank points!  Zero is invalid!", MsgBoxStyle.OkOnly, "Error!")
            GoTo end1
        End If

        If AS_Combo.SelectedItem = "" Then
            MsgBox("Please select an Alien or Ship!", MsgBoxStyle.OkOnly, "Error!")
            GoTo end1
        End If

        'Gets information from the Access Database
        Dim ds As New DataSet
        Dim dt As New DataTable
        ds.Tables.Add(dt)
        Dim name As String = AS_Combo.SelectedItem.ToString

        'Query
        Dim da As New OleDbDataAdapter
        Dim cmd As String = "SELECT * FROM RankPoint where name = '" & name & "'"

        da = db.getOleDbAdapter(cmd)

        'Fills data
        da.Fill(dt)
        'Sets the text fields for each of them
        Dim point As Double = dt.Rows(0).Item(2)

        x = neededpoints / point
        Calculation_Label.Text = x

        KillType_Label.Text = name & "(s)"
end1:
    End Sub
    Private Sub YourRank_Text_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles YourRank_Text.KeyPress
        If (e.KeyChar < "0" OrElse e.KeyChar > "9") _
    AndAlso e.KeyChar <> ControlChars.Back AndAlso e.KeyChar <> "." Then
            'cancel keys
            e.Handled = True
        End If
        If e.KeyChar = "." Then
            e.Handled = True
        End If
    End Sub
    Private Sub NextRank_Text_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles NextRank_Text.KeyPress
        If (e.KeyChar < "0" OrElse e.KeyChar > "9") _
    AndAlso e.KeyChar <> ControlChars.Back AndAlso e.KeyChar <> "." Then
            'cancel keys
            e.Handled = True
        End If
        If e.KeyChar = "." Then
            e.Handled = True
        End If
    End Sub

    '------ Menu Strip Items --------
    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If MsgBox("Are you sure you want to exit DOHelper?", MsgBoxStyle.YesNo, "DOHelper - Exit") = MsgBoxResult.Yes Then
            db.close()
            End
        End If
    End Sub
    Private Sub ScreenCaptureToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Hide()
        ScreenCapture_Form.Show()
    End Sub
    Private Sub NoteEditorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        NoteEditor_Form.Show()
    End Sub
    Private Sub AboutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        About_Form.Show()
    End Sub
    Private Sub HelpToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Help_Form.Show()
    End Sub
    Private Sub GalaxyGatesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        GalaxyGatefrm.Show()

        'GalaxyGates_Form.Show()
        Me.Hide()
    End Sub
    Private Sub LogDisksToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Log_Disks.Show()
        Me.Hide()
    End Sub
    Private Sub AliensToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Aliens_Form.Show()
        Me.Hide()
    End Sub
    Private Sub StarSystemToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Star_System.Show()
        Me.Hide()
    End Sub
    Private Sub ShipsToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Ships_Form.Show()
    End Sub
    Private Sub RanksToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Rank_Form.Show()
    End Sub
    Private Sub LevelsToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Level_Form.Show()
    End Sub
    Private Sub LaserDamageToolStripMenuItem_Click(sender As Object, e As EventArgs)
        Laser_Damage.Show()
    End Sub

    'Database is being passed through
    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    Private Sub AS_Combo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles AS_Combo.SelectedIndexChanged

    End Sub

    Private Sub NextRank_Label_Click(sender As Object, e As EventArgs) Handles NextRank_Label.Click

    End Sub

    Private Sub YourRank_Text_TextChanged(sender As Object, e As EventArgs) Handles YourRank_Text.TextChanged

    End Sub

    Private Sub NextRank_Text_TextChanged(sender As Object, e As EventArgs) Handles NextRank_Text.TextChanged

    End Sub

    Private Sub KillType_Label_Click(sender As Object, e As EventArgs) Handles KillType_Label.Click

    End Sub

    Private Sub Calculation_Label_Click(sender As Object, e As EventArgs) Handles Calculation_Label.Click

    End Sub

    Private Sub YouNeed_Label_Click(sender As Object, e As EventArgs) Handles YouNeed_Label.Click

    End Sub

    Private Sub YourRank_Label_Click(sender As Object, e As EventArgs) Handles YourRank_Label.Click

    End Sub
End Class