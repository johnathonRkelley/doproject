﻿Imports System.Data.OleDb

Public Class Log_Disks
    'Intialized Variables
    Dim beginningPP As Integer
    Dim currentLD As Integer
    Dim desiredPP As Integer
    Dim percentOff As Double
    Dim logDiskCost As Integer
    Dim discountedLD As Integer

    Dim myConnectionString As String

    'Database being used
    Dim db As Database

    'Passes the database through the Log Disk Form.
    Public Sub passThrough(ByRef access As Database)
        db = access
    End Sub

    'Load Form
    Private Sub Pilot_Points_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        logDiskCost = 300

        lblBoth.Text = Nothing
        lblCostPremium.Text = Nothing
        lbTotalCost.Text = Nothing
        lblCostRebate.Text = Nothing
    End Sub

    'Close Form
    Private Sub Pilot_Points_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Me.Hide()
    End Sub

    'Calculate method
    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        'Local Variables
        Dim remainingDisks As Integer
        Dim disksNeeded As Integer

        percentOff = 0 'Re-initializes the percentOff to 0

        'Checks to see if beginningPP is empty
        If txtBeginningPP.TextLength = 0 Then
            MsgBox("Please enter your beginning PP")
            Return
        Else
            beginningPP = txtBeginningPP.Text
        End If

        'checks to see if current log disk is empty
        If txtCurrentLD.TextLength = 0 Then
            currentLD = 0
        Else
            currentLD = txtCurrentLD.Text
        End If

        'Get the values from the desired log disks
        If txtDesiredPP.TextLength = 0 Then
            desiredPP = disksNeeded
        Else
            desiredPP = txtDesiredPP.Text
        End If

        'Gets inputed values from the user
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim da As New OleDbDataAdapter
        ds.Tables.Add(dt)

        'Query
        Dim query As String = "SELECT DISTINCTROW Sum([LogDisks].[LDToGet]) AS [Sum Of LDToGet] FROM LogDisks WHERE (((LogDisks.[PPs])<= " & desiredPP & "And (LogDisks.[PPs])> " & beginningPP & "));"

        'Fills the data into the table
        da = db.getOleDbAdapter(query)
        da.Fill(dt)

        'Gets the disk needed
        disksNeeded = dt.Rows(0).Item(0)
        lbTotalCost.Text = disksNeeded

        'Total Cost first 
        Dim totalCost As Integer
        totalCost = logDiskCost * (disksNeeded - currentLD)
        lbTotalCost.Text = totalCost

        'Cost with Rebate
        totalCost = (logDiskCost * 0.75) * (disksNeeded - currentLD)
        lblCostRebate.Text = totalCost

        'Cost with Premium
        totalCost = (logDiskCost * 0.95) * (disksNeeded - currentLD)
        lblCostPremium.Text = totalCost

        'Cost with Both Premium and Rebate
        totalCost = (logDiskCost * 0.7) * (disksNeeded - currentLD)
        lblBoth.Text = totalCost

        'Calulates the discounted log disk cost'
        discountedLD = logDiskCost * (1 - percentOff)

        remainingDisks = disksNeeded - currentLD
    End Sub

    Private Sub txtDesiredPP_TextChanged(sender As Object, e As EventArgs) Handles txtDesiredPP.TextChanged

    End Sub

    Private Sub txtCurrentLD_TextChanged(sender As Object, e As EventArgs) Handles txtCurrentLD.TextChanged

    End Sub

    Private Sub txtBeginningPP_TextChanged(sender As Object, e As EventArgs) Handles txtBeginningPP.TextChanged

    End Sub
End Class